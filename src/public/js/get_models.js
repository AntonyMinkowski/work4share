$(document).ready(
    $(function() {
        // событие выбора бренда. подгужаем для выбранного бренда досьупные модели
        $( "#brandid" ).change(function () {
            // скрываем список моделей
            $("#model").addClass("d-none");
            var str = "";
            $.ajax({
                method: "GET",
                url: "/car/models",
                data: {
                    brandid: $(this).val()
                },
                success: function(data){
                    var select_list = ["<option disabled selected>Ничего не выбрано</option>"];
                    $.each( data, function( key, val ) {
                        select_list.push(
                            "<option value='"
                            + val.id
                            + "'>"
                            + val.name
                            + "</option>");
                    });
                    $("#model select").html(select_list.join( "" ));
                    // теперь можно показать
                    $("#model").removeClass("d-none");
                }
            });
        });
    })
);
