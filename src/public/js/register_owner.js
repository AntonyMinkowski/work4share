$(document).ready(
    $(function() {
        $(document).on("submit", "#upload_passport", (function(evt) {
            evt.preventDefault();
            request = $.ajax({
                url: this.action,
                type: 'POST',
                data: new FormData(this),
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false
            });

            request.done(function(msg) {
                $("#result").html(msg);

                console.log('Success!');
                $("#upload_passport").addClass("d-none");
                $(".verify").removeClass("d-none");
            });

            request.fail(function(jqXHR, textStatus) {
                $("#result").html("Request failed: " + textStatus);

                console.log('Error!' + textStatus);
            });
        }));
        $(document).on("submit", "#upload_driving_lisense", (function(evt) {
            evt.preventDefault();
            request = $.ajax({
                url: this.action,
                type: 'POST',
                data: new FormData(this),
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false
            });

            request.done(function(msg) {
                $("#result").html(msg);

                console.log('Success!');
                $("#upload_driving_lisense").addClass("d-none");
                $(".verify").removeClass("d-none");
            });

            request.fail(function(jqXHR, textStatus) {
                $("#result").html("Request failed: " + textStatus);

                console.log('Error!' + textStatus);
            });
        }));
        $(document).on("submit", "#upload_selfie", (function(evt) {
            evt.preventDefault();
            request = $.ajax({
                url: this.action,
                type: 'POST',
                data: new FormData(this),
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false
            });

            request.done(function(msg) {
                $("#result").html(msg);

                console.log('Success!');
                $("#upload_selfie").addClass("d-none");
                $(".verify").removeClass("d-none");
            });

            request.fail(function(jqXHR, textStatus) {
                $("#result").html("Request failed: " + textStatus);

                console.log('Error!' + textStatus);
            });
        }));
    })
);
