/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 508);
/******/ })
/************************************************************************/
/******/ ({

/***/ 508:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(509);


/***/ }),

/***/ 509:
/***/ (function(module, exports) {

var modelSelect = $('#model-select');
$('#brandid').change(function () {
    var brandId = $(this).val();
    $.get('/staff/getModels/' + brandId, function (data) {
        console.log(data);
        if (data.length) {
            var models = '<option value="">\u041D\u0438\u0447\u0435\u0433\u043E \u043D\u0435 \u0432\u044B\u0431\u0440\u0430\u043D\u043E</option>';
            data.forEach(function (item) {
                models += '<option\n                                class="form-cars__option" value="' + item['id'] + '">\n                            ' + item['name'] + '\n                        </option>';
            });
            modelSelect.html(models);
            modelSelect.show();
        }
    });
});

$('body').on('submit', '.staff-auto form', function (e) {
    e.preventDefault();
    var form = $(this);
    var loading = $('.loading');
    loading.addClass('active');

    if (form.find('select').val().length == 0) {
        alert('Вы ничего не выбрали');
        loading.removeClass('active');
        return true;
    } else {
        $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            data: form.serialize()
        }).done(function (res) {
            console.log(res);

            setTimeout(function () {
                loading.removeClass('active');
            }, 1000);
        }).fail(function (err) {
            console.log(err);

            alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
            setTimeout(function () {
                loading.removeClass('active');
            }, 1000);
        });
    }
});

/***/ })

/******/ });
//# sourceMappingURL=staff.js.map