<?php

use Illuminate\Database\Seeder;
use App\Models\BookingStatus;

class BookingStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $items = [
            ['name' => 'Ожидает оплаты'],
            ['name' => 'Оплачено'],
            ['name' => 'Забронировано'],
            ['name' => 'Отмена брони'],
            ['name' => 'Бронь отменена'],
            ['name' => 'Подтверждение владельца'],
            ['name' => 'Владелец подтвердил'],
            ['name' => 'Ожидание передачи авто'],
            ['name' => 'Владелец передал авто'],
            ['name' => 'Арендатор получил авто'],
            ['name' => 'Аренда'],
            ['name' => 'Аренда приостановлена'],
            ['name' => 'Аренда возобновлена'],
            ['name' => 'Завершение аренды'],
            ['name' => 'Ожидание передачи владельцу'],
            ['name' => 'Арендатор передал авто'],
            ['name' => 'Владалец получил авто'],
            ['name' => 'Ожидание оплаты за перепробег'],
            ['name' => 'Оплачен перепробег'],
            ['name' => 'Оплачено'],
            ['name' => 'Аренда завершена'],
            ['name' => 'Бронь отменена владельцем'],
        ];

        foreach ($items as $item) {
            BookingStatus::insert([
                'name' => $item['name']
            ]);
        }
    }
}
