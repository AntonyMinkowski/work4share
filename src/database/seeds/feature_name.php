<?php

use Illuminate\Database\Seeder;

class feature_name extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('feature_name')->insert([
            'en_name' => 'air',
            'ru_name' => 'Кондиционер',
        ]);
        DB::table('feature_name')->insert([
            'en_name' => 'bluetooth',
            'ru_name' => 'Bluetooth',
        ]);
        DB::table('feature_name')->insert([
            'en_name' => 'audio_input',
            'ru_name' => 'AUX',
        ]);
        DB::table('feature_name')->insert([
            'en_name' => 'usb_charger',
            'ru_name' => 'USB зарядка',
        ]);
        DB::table('feature_name')->insert([
            'en_name' => 'child_seat',
            'ru_name' => 'Детское сидение',
        ]);
        DB::table('feature_name')->insert([
            'en_name' => 'cruise_control',
            'ru_name' => 'Круиз контроль',
        ]);
        DB::table('feature_name')->insert([
            'en_name' => 'gps',
            'ru_name' => 'GPS навигация',
        ]);
        DB::table('feature_name')->insert([
            'en_name' => 'heated_seats',
            'ru_name' => 'Подогрев сидений',
        ]);
    }
}
