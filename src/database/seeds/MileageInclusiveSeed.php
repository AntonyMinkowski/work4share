<?php

use App\Models\MileageInclusive;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class MileageInclusiveSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tableName = MileageInclusive::newModelInstance()->getTable();
        if (Schema::hasTable($tableName) === false) {
            echo 'MileageInclusiveSeed: таблица ' . $tableName . ' не создана. Выполните миграцию.' . PHP_EOL;
            return;
        }

        if (DB::table($tableName)->count() > 0) {
            echo 'MileageInclusiveSeed: таблица ' . $tableName . ' уже заполнена' . PHP_EOL;
            return;
        }

        $rows     = [];
        $date     = date('Y-m-d H:i:s');
        $nameList = [
            '100',
            '150',
            '200',
            '250',
            '300',
            '400',
            '500',
            'Не ограниченно',
        ];

        foreach ($nameList as $name) {
            $rows[] = ['name' => $name, 'created_at' => $date, 'updated_at' => $date];
        }

        // Записываем в базу
        DB::table($tableName)
          ->insert($rows);

        echo 'MileageInclusiveSeed: количество добавленных записей: ' . count($rows) . PHP_EOL;
    }
}
