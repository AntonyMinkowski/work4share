<?php

use Illuminate\Database\Seeder;

class DepositPrices extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\DepositPrice::insert([
            'car_price' => 0,
            'deposit_price' => 5000,
        ]);

        \App\DepositPrice::insert([
            'car_price' => 2000001,
            'deposit_price' => 10000,
        ]);

        \App\DepositPrice::insert([
            'car_price' => 3000001,
            'deposit_price' => 30000,
        ]);

        \App\DepositPrice::insert([
            'car_price' => 5000001,
            'deposit_price' => 100000,
        ]);

    }
}
