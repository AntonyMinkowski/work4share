<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersRefusalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $items = [
            ['name' => 'Паспорт подтвержден'],
            ['name' => 'Паспорт не читаем'],
            ['name' => 'Паспорт не действителен'],
            ['name' => 'Паспорт отклонен, возраст менее 21 года'],
            ['name' => 'ВУ подтверждено'],
            ['name' => 'ВУ не читаемо'],
            ['name' => 'ВУ не действительно'],
            ['name' => 'ВУ отклонено, стаж менее 2 лет'],
            ['name' => 'Пользователь подтверждён'],
            ['name' => 'Пользователь не подтвержден']
        ];

        foreach ($items as $item) {
            DB::table('users_validate_refusal')->insert($item);
        }
    }
}
