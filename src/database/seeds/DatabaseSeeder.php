<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $classes = [
            City::class,
            ReRunPriceSeeder::class,
            FuelTypeSeeder::class,
            TransmissionSeeder::class,
            ModelSeeder::class,
            BookingStatusSeeder::class,
            BrandCarsSeeder::class,
            CarBansSeeder::class,
            CarsColorSeeder::class,
            CarsRefusalSeeder::class,
            CarTypesSeeder::class,
            CustomerVerifyStatusSeed::class,
            ExtraSeeder::class,
            ExtraTimeSeeder::class,
            feature_name::class,
            MileageInclusiveSeed::class,
            MileageInclusiveSeeder::class,
            RentPlaceSeeder::class,
            RentTimeSeeder::class,
            StaffModule::class,
            StaffRole::class,
            TimeAlertOwenRentSeed::class,
            UserEventPushType::class,
            UsersRefusalSeeder::class
        ];

        foreach ($classes as $class) {
            $this->call($class);
        }
    }
}
