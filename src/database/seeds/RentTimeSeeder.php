<?php

use Illuminate\Database\Seeder;
use App\Models\RentTime;

class RentTimeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 30; $i++) {
            $_plural_days = array('день', 'дня', 'дней'); 
            RentTime::insert([
                'name' => $i . ' ' . $_plural_days[RentTime::plural_type($i)],
                'time' => 86400*$i
            ]);
        }
    }
}
