<?php

use App\Models\TimeAlertOwnRent;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class TimeAlertOwenRentSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tableName = TimeAlertOwnRent::newModelInstance()->getTable();
        if (Schema::hasTable($tableName) === false) {
            echo 'TimeAlertOwenRentSeed: таблица ' . $tableName . ' не создана. Выполните миграцию.' . PHP_EOL;
            return;
        }

        if (DB::table($tableName)->count() > 0) {
            echo 'TimeAlertOwenRentSeed: таблица ' . $tableName . ' уже заполнена' . PHP_EOL;

            return;
        }

        $rows     = [];
        $dataList = [
            ['name' => '1 час', 'time' => '60'],
            ['name' => '12 часов', 'time' => '43200'],
            ['name' => '1 день', 'time' => '86400'],
            ['name' => '2 дня', 'time' => '172800'],
            ['name' => '3 дня', 'time' => '259200'],
        ];

        foreach ($dataList as $data) {
            $rows[] = ['name' => $data['name'], 'time' => $data['time']];
        }

        // Записываем в базу
        DB::table($tableName)
          ->insert($rows);

        echo 'TimeAlertOwnRentSeed: количество добавленных записей: ' . count($rows) . PHP_EOL;
    }
}
