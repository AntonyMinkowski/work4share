<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCarTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('car_types')) {
            Schema::create('car_types', function (Blueprint $table) {
                    $table->increments('id');
                    $table->string('name');
                    $table->timestamps();
            });

            if (Schema::hasTable('cars')) {
                if (Schema::hasColumn('cars', 'car_type')) {
                    Schema::table('cars', function (Blueprint $table) {
                        $table->integer('car_type')->unsigned()->nullable()->change();
                    });
                } else {
                    Schema::table('cars', function (Blueprint $table) {
                        $table->integer('car_type')->unsigned()->nullable();
                    });
                }
                Schema::disableForeignKeyConstraints();
                Schema::table('cars', function (Blueprint $table) {
                    $table->foreign('car_type')->references('id')->on('car_types');
                });
                Schema::enableForeignKeyConstraints();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('cars')) {
            if (Schema::hasColumn('cars', 'car_type')) {
                Schema::table('cars', function (Blueprint $table) {
                    $table->dropForeign(['car_type']);
                    $table->dropColumn('car_type');
                });
            }
        }

        Schema::dropIfExists('car_types');
    }
}
