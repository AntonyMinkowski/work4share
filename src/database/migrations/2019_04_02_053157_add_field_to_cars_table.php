<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * 6. В сущность cars добавить атрибуты:
 * vin (text)
 * time_alert (int)
 * mileage_incusive (int)
 * min_overmile_fee (float)
 * max_overmile_fee (float)
 * need_kasko_and_osago (bool)
 */
class AddFieldToCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table->string('vin')->nullable();
            $table->integer('time_alert')->nullable();
            $table->integer('mileage_inclusive')->nullable();
            $table->float('min_overmile_fee')->nullable();
            $table->float('max_overmile_fee')->nullable();
            $table->boolean('need_kasko_and_osago')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('cars')) {
            $columns = ['vin', 'time_alert', 'mileage_inclusive', 'min_overmile_fee', 'max_overmile_fee', 'need_kasko_and_osago'];
            foreach ($columns as $column) {
                $this->dropColumnIfExist($column);
            }
        }
    }

    /**
     * Проверяем на сущестование таблицы и поля затем удаляем
     *
     * @param string $column
     * @param string $table
     */
    private function dropColumnIfExist(string $column, string $table = 'cars')
    {
        if (Schema::hasColumn($table, $column)) {
            Schema::table($table, function (Blueprint $table) use ($column) {
                $table->dropColumn($column);
            });
        }
    }
}
