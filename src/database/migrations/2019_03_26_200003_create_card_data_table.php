<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardDataTable extends Migration
{
    public function up()
    {
        Schema::create('cars_data', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pts_image_part_one')->nullable();
            $table->string('pts_image_part_two')->nullable();
            $table->string('sts_image_part_one')->nullable();
            $table->string('sts_image_part_two')->nullable();
            $table->string('osago_image')->nullable();
            $table->string('kasko_image')->nullable();
            $table->timestamps();
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars_data');
    }
}



