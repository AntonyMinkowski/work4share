<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixColumnCars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('cars')) {
            if (Schema::hasColumn('cars', 'year_of_issue')) {
                Schema::table('cars', function (Blueprint $table) {
                    $table->integer('year_of_issue')->nullable()->change();
                });
            }

            if (Schema::hasColumn('cars', 'pts_image_id')) {
                Schema::table('cars', function (Blueprint $table) {
                    $table->integer('pts_image_id')->nullable()->change();
                });
            }

            if (Schema::hasColumn('cars', 'sts_image_id')) {
                Schema::table('cars', function (Blueprint $table) {
                    $table->integer('sts_image_id')->nullable()->change();
                });
            }

            if (Schema::hasColumn('cars', 'cost_day')) {
                Schema::table('cars', function (Blueprint $table) {
                    $table->dropColumn('cost_day');
                });
                Schema::table('cars', function (Blueprint $table) {
                    $table->tinyInteger('cost_day')->nullable();
                });
            }

            if (Schema::hasColumn('cars', 'cost_hour')) {
                Schema::table('cars', function (Blueprint $table) {
                    $table->dropColumn('cost_hour');
                });
                Schema::table('cars', function (Blueprint $table) {
                    $table->tinyInteger('cost_hour')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('cars')) {
            if (Schema::hasColumn('cars', 'year_of_issue')) {
                Schema::table('cars', function (Blueprint $table) {
                    $table->integer('year_of_issue')->change();
                });
            }

            if (Schema::hasColumn('cars', 'pts_image_id')) {
                Schema::table('cars', function (Blueprint $table) {
                    $table->integer('pts_image_id')->change();
                });
            }

            if (Schema::hasColumn('cars', 'sts_image_id')) {
                Schema::table('cars', function (Blueprint $table) {
                    $table->integer('sts_image_id')->change();
                });
            }

            if (Schema::hasColumn('cars', 'cost_day')) {
                Schema::table('cars', function (Blueprint $table) {
                    $table->string('cost_day')->change();
                });
            }

            if (Schema::hasColumn('cars', 'sts_image_id')) {
                Schema::table('cars', function (Blueprint $table) {
                    $table->string('cost_hour')->change();
                });
            }
        }
    }
}
