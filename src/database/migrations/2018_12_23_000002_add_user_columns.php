<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('users')) {
            if (!Schema::hasColumn('users', 'passport_images_id')) {
                Schema::table('users', function (Blueprint $table) {
                    $table->integer('passport_images_id')->nullable();
                    $table->tinyInteger('isAdmin')->nullable();
                    $table->tinyInteger('isOwner')->nullable();
                    $table->tinyInteger('isCustomer')->nullable();
                    $table->tinyInteger('validate')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('users')) {
            if (Schema::hasColumn('users', 'passport_images_id')) {
                Schema::table('users', function (Blueprint $table) {
                    $table->dropColumn('passport_images_id');
                });
            }
        }

        if (Schema::hasTable('users')) {
            if (Schema::hasColumn('users', 'isAdmin')) {
                Schema::table('users', function (Blueprint $table) {
                    $table->dropColumn('isAdmin');
                });
            }
        }

        if (Schema::hasTable('users')) {
            if (Schema::hasColumn('users', 'isOwner')) {
                Schema::table('users', function (Blueprint $table) {
                    $table->dropColumn('isOwner');
                });
            }
        }

        if (Schema::hasTable('users')) {
            if (Schema::hasColumn('users', 'isCustomer')) {
                Schema::table('users', function (Blueprint $table) {
                    $table->dropColumn('isCustomer');
                });
            }
        }

        if (Schema::hasTable('users')) {
            if (Schema::hasColumn('users', 'validate')) {
                Schema::table('users', function (Blueprint $table) {
                    $table->dropColumn('validate');
                });
            }
        }
    }
}
