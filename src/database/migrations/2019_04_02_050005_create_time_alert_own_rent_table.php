<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Создаем сузность time_alert_own_rent с полями
 * id (inc)
 * name (text)
 * time (int)
 */
class CreateTimeAlertOwnRentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('time_alert_own_rent') === false) {
            Schema::create('time_alert_own_rent', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name')->nullable(false);
                $table->integer('time')->nullable(false);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('time_alert_own_rent');
    }
}
