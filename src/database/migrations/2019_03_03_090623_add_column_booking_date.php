<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnBookingDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('booking_date')) {
            Schema::table('booking_date', function (Blueprint $table) {
                $table->integer('ownGaveKayBeforeRent')->nullable();
            });

            Schema::table('booking_date', function (Blueprint $table) {
                $table->integer('usrTookKayBeforeRent')->nullable();
            });
            
            Schema::table('booking_date', function (Blueprint $table) {
                $table->integer('usrGaveKayAfterRent')->nullable();
            });            

            Schema::table('booking_date', function (Blueprint $table) {
                $table->integer('ownTookKayBeforeRent')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
