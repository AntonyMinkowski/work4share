<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Создаем сущность mileage_inclusive
 */
class CreateMileageInclusiveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('mileage_inclusive') === false) {
            Schema::create('mileage_inclusive', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name')->nullable(false);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mileage_inclusive');
    }
}
