<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnFeatureTimestamp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('cars_feature')) {
                Schema::table('cars_feature', function (Blueprint $table) {
                    $table->timestamps();
                });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('cars_feature', 'feature_id')) {
            Schema::table('cars_feature', function (Blueprint $table) {
                $table->dropTimestamps();
            });
        }
    }
}
