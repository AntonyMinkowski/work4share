<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Models\BrandsCar::class, function (Faker $faker) {
    return [
        'name' => 'testBrands',
    ];
});
$factory->define(App\Models\ModelsCar::class, function (Faker $faker) {
    return [
        'name' => 'testModels',
        'brand_id' => function () {
            return factory(App\Models\BrandsCar::class)->create(['id' => 1])->id;
        },
    ];
});

$factory->define(App\Models\CarsGear::class, function (Faker $faker) {
    return [
        'name' => 'testGear',
    ];
});

$factory->define(App\Models\CarType::class, function (Faker $faker) {
    return [
        'name' => 'testCarType',
    ];
});

$factory->define(App\Models\Car::class, function ($faker) {
    return [
        'model_id' => function () {
            return factory(App\Models\ModelsCar::class)->create([
                'brand_id' => function () {
                    return factory(App\Models\BrandsCar::class)->create(['id' => 1])->id;
                },
            ])->id;
        },
        'brand_id' => function () {
            return factory(App\Models\BrandsCar::class)->make(['id' => 1])->id;
        },
        'user_id' => function () {
            return factory(App\User::class)->create([
                'name' => 'Test',
                'email' => 'test@test.tu'
            ])->id;
        },
        'car_type' => function () {
            return factory(App\Models\CarType::class)->make(['id' => 1])->id;
        },
    ];
});
