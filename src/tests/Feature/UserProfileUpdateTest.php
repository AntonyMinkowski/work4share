<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class UserProfileUpdateTest extends TestCase
{
    use RefreshDatabase;

    public function setUp()
    {
        parent::setUp();

        factory(\App\User::class)->create([
            'id' => 2,
            'name' => 'Test',
            'email' => 'test@test1.tu',
        ]);

        Storage::fake('hot');
        Storage::fake('ice');
    }

    /**
     * страница профиля
     *
     * @return void
     */
    public function testGetProfile()
    {

        $user = factory(\App\User::class)->make([
            'id' => 2,
            'name' => 'Test',
            'email' => 'test@test1.tu'
        ]);

        $response = $this->actingAs($user)->get(
            route('user.profile')
        );

        $response->assertStatus(200)->assertSeeInOrder([
            'Добавьте фото паспорта со страницами фотографии и прописки',
            'Добавьте фото водительского удостоверения с 2х сторон'
        ]);
    }

    /**
     * добавление фото паспорта
     *
     * @return void
     */
    public function testSetPasport()
    {

        $user = \App\User::find(2);

        $response = $this->actingAs($user)->json(
            'POST',
            route('user.add.pasport'),
            [
                'passport_face' => UploadedFile::fake()->image('passport_face.jpg'),
                'passport_visa' => UploadedFile::fake()->image('passport_visa.jpg')
            ]
        );

        $response->assertStatus(200)->assertJson([
            'result' => true
        ]);
    }

    /**
     * Тест бронирования авто владельцем
     *
     * @return void
     */
    public function testSetDrivingLicence()
    {

        $user = \App\User::find(2);

        $response = $this->actingAs($user)->json(
            'POST',
            route('user.add.driving_licence'),
            [
                'driver_lisense_first' => UploadedFile::fake()->image('driver_lisense_first.jpg'),
                'driver_license_second' => UploadedFile::fake()->image('driver_license_second.jpg')
            ]
        );

        $response->assertStatus(200)->assertJson([
            'result' => true
        ]);
    }

    public function tearDown()
    {
        parent::tearDown();
    }
}
