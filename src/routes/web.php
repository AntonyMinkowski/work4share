<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Auth::routes(['verify' => true]);
Route::post('quick', 'Auth\RegisterController@quick')->name('quick');
// Route::get('/', 'Web\GetCarsController@getFirstCars');
Route::get('logout', 'Auth\LoginController@logout');

Route::view('/calendar', 'calendar');


// Route::get('share', [
//     'middleware' => 'auth',
//     'carslist' => 'UserController@showProfile'
// ]);

// Route::get('carslist', 'GetCarsController@getSelection')->middleware('auth');



Route::group(
    [
        'namespace' => 'Pages',
        'middleware' => 'handle-utm'
    ],
    function() {
        Route::get('/booking', 'BookingPageController@index')->name('booking');
        Route::get('/how-it-works', 'HowPageController@index')->name('how');
        Route::get('/hand-over', 'HandOverPageController@index')->name('handOver');
        Route::get('/contact', 'ContactPageController@index')->name('contact');
        Route::get('/info', 'InfoPageController@index')->name('info');
        
});

//Верификация пользователя
Route::group(['prefix' => 'verify', 'namespace' => 'Web'], function () {
    Route::get('/{id}', 'VerifyController@index')->name('verify.index')->where('id', '[0-9]+');
    Route::post('/passport', 'VerifyController@passport')->name('verify.passport');
    Route::post('/driving', 'VerifyController@driving')->name('verify.driving');
    Route::post('/selfie', 'VerifyController@selfie')->name('verify.selfie');
    Route::post('/email', 'VerifyController@email')->name('verify.email');
    Route::post('/name', 'VerifyController@name')->name('verify.name');
});

Route::group(
    [
        'namespace' => 'Web',
        'middleware' => 'handle-utm'
    ],
    function() {

        Route::get('/', 'HomePageController@index')->name('home');
        Route::get('carslist/{search?}', 'GetCarsController@getSelectionCars')->where('search', '.*')->name('car.search');
        Route::get('carcard/{id}', 'GetCarsController@getCarCard')->name('car.catalog.card');
        Route::get('car/models', 'CarsController@getModels')->where('brandid', '[0-9]+')->name('car.models');
        Route::get('catalog/', 'CatalogController@index')->name('car.catalog.index');
        Route::post('/filter', 'CatalogController@filter')->name('car.catalog.filter');

        Route::get('/car/filter_rent', 'GetCarsController@getFilterRent');
        
        Route::post('goToCheckout/{id}', 'CheckoutController@goToCheckout')->name('checkout.goToCheckout')->where('car_id', '[0-9]+');;
        

        Route::match(['get', 'post'], '/checkout/{id}', 'CheckoutController@index')->name('checkout')
                ->where('car_id', '[0-9]+');

        Route::get('/sendValidateSms', 'CheckoutController@sendValidateSms')->name('sendValidateSms');
        Route::post('/validatePhone', 'CheckoutController@validatePhone')->name('validatePhone');
        Route::post('/booking/{id}', 'CheckoutController@booking')->name('checkout.booking')->where('car_id', '[0-9]+');
        Route::get('/goToRegister', 'CheckoutController@goToRegister')->name('checkout.goToRegister');

        Route::group(
            [
                'middleware' => ['auth', 'verified'],
            ], function () {
                
            Route::post( '/booking', 'GetCarsController@booking');

            Route::get( '/chat', ['uses' => 'DialoguesController@index', 'as' => 'chat']);

            Route::post( '/chat/messages', 'DialoguesController@messages');
            
        });

        // Route::get('/bookingcar', 'BookingController@BookingPage');
        // Route::get('/bookingcar/success', 'BookingController@BookingSuccess');

    });

Route::get('carslist/{search?}', 'Web\GetCarsController@getSelectionCars')->where('search', '.*')->name('car.search');
Route::get('/carcard/{id}', 'Web\GetCarsController@getCarCard');
Route::get('car/models', 'Web\CarsController@getModels')->where('brandid', '[0-9]+')->name('car.models');

Route::get('register_owner', 'Web\UserProfile\ProfileController@registerOwner')
    ->middleware('auth')
    ->name('user.register_owner');
Route::post('set_passport', 'Web\UserController@setPasport')
    ->middleware('auth')
    ->name('user.set_passport');
Route::post('driving_licence', 'Web\UserController@setDrivingLicence')
    ->middleware('auth')
    ->name('user.set_driving_licence');
Route::post('update_selfie', 'Web\UserController@updateSelfie')
    ->middleware('auth')
    ->name('user.update_selfie');

Route::group(
    [
        'middleware' => ['auth', 'verified'],
        'prefix' => 'profile/',
        'namespace' => 'Web',
    ],
    function() {
        Route::get('mycars', 'UserProfile\GetMyCarsController@getMyCars')->name('profile.mycars');
        Route::get('mycars/{id}', 'UserProfile\GetMyCarsController@getMyCar')->where('car_id', '[0-9]+')->name('profile.car');

        Route::get('getMyCarLocation/{id}', 'UserProfile\GetMyCarsController@getMyCarLocation')->where('car_id', '[0-9]+');
        Route::post('getTracking/{id}', 'UserProfile\GetMyCarsController@getTracking')->where('car_id', '[0-9]+');
        Route::post('torentcar', 'UserProfile\GetMyCarsController@toRentCar');
        Route::post('editRentCar/{id}', 'UserProfile\GetMyCarsController@editRentCar')->where('car_id', '[0-9]+')->name('car.editRentCar');
        Route::delete('deleteRentCar/{id}', 'UserProfile\GetMyCarsController@deleteRentCar')->where('car_id', '[0-9]+')->name('car.deleteRentCar');
        Route::post('fromrentcar', 'UserProfile\GetMyCarsController@fromRentCar');
        Route::post('setHeadImageCar', 'UserProfile\GetMyCarsController@setHeadImageCar');
        Route::post('rescission/{id}', 'UserProfile\GetMyCarsController@rescission');
        Route::get('confirm/{id}', 'UserProfile\GetMyCarsController@confirm');
        Route::get('confirm-payment/{id}', 'UserProfile\GetMyCarsController@confirmPayment');
        Route::post('saveCarImages/{car_id}', 'ImageController@saveCarImages')->where('car_id', '[0-9]+');
        Route::post('mycars/{car_id}/feature', 'CarsController@addCarFeature')->where('car_id', '[0-9]+');
        Route::get('my-car/booking/{booking_id}', 'UserProfile\GetMyCarsController@bookingCard')->name('profile.my-car.booking')->where('booking_id', '[0-9]+');
        Route::post('my-car/addCarInterior/{booking_id}', 'UserProfile\GetMyCarsController@addCarInterior')->name('my-car.addCarInterior')->where('booking_id', '[0-9]+');
        Route::post('my-car/addCarFuelResidue/{booking_id}', 'UserProfile\GetMyCarsController@addCarFuelResidue')->name('my-car.addCarFuelResidue')->where('booking_id', '[0-9]+');
        Route::post('my-car/addCarMileagePhoto/{booking_id}', 'UserProfile\GetMyCarsController@addCarMileagePhoto')->name('my-car.addCarMileagePhoto')->where('booking_id', '[0-9]+');
        Route::post('my-car/addCarMileageInput/{booking_id}', 'UserProfile\GetMyCarsController@addCarMileageInput')->name('my-car.addCarMileageInput')->where('booking_id', '[0-9]+');


        Route::get('mycar/{id}', 'UserProfile\EditMyCarController@index')->name('profile.EditMyCarController');
        Route::post('mycar/{id}/osago_image', 'ImageController@osagoImage')->where('car_id', '[0-9]+');
        Route::post('mycar/{id}/kasko_image', 'ImageController@kaskoImage')->where('car_id', '[0-9]+');

        Route::post('mycar/{id}/setHead', 'UserProfile\EditMyCarController@setHeadImageCar')->name('profile.setHead');
        Route::post('mycar/{id}/deleteImage', 'UserProfile\EditMyCarController@deleteImage')->name('profile.deleteImage');
        

        Route::post('updateAuthUserPassword', 'UserProfile\ProfileController@changePassword');
        Route::post('saveNotifications', 'UserProfile\ProfileController@saveNotifications');
        Route::post('uploadAvatar', 'UserProfile\ProfileController@uploadAvatar');
        Route::post('updatePreferredTypeKPP', 'UserProfile\ProfileController@updatePreferredTypeKPP');
        Route::post('saveInfo', 'UserProfile\ProfileController@saveInfo');
        Route::post('updateBirthday', 'UserProfile\ProfileController@updateBirthday');
        Route::post('updateExperience', 'UserProfile\ProfileController@updateExperience');
        Route::post('saveInfo', 'UserProfile\ProfileController@saveInfo');
        Route::get('isActive', 'UserProfile\ProfileController@isActive');

        Route::get('trips', 'UserProfile\MyTripsController@index')->name('profile.trips');
        Route::get('trips/card/{id}', 'UserProfile\MyTripsController@card')->name('profile.trips.card')->where('car_id', '[0-9]+');
        Route::post('trips/card/payment/{id}', 'UserProfile\MyTripsController@paymentFromTrip')->name('profile.trips.payment')->where('booking_id', '[0-9]+');

        Route::get('pdf', 'UserProfile\MyTripsController@pdf')->name('pdf');

        Route::get('', 'UserProfile\ProfileController@show')->name('profile.index');

        Route::post('mycars/{id}/addImages', 'ImageController@addPhotosCar')->name('car.addImages');
        
        Route::post('mycar/{id}/editConditionDelivery', 'UserProfile\EditMyCarController@editConditionDelivery')->name('car.editConditionDelivery');
        Route::post('mycar/{id}/editDeposit', 'UserProfile\EditMyCarController@editDeposit')->name('car.editDeposit');
        Route::post('mycar/{id}/editCarModel', 'UserProfile\EditMyCarController@editCarModel')->name('car.model');
        Route::post('mycar/{id}/editCarBrand', 'UserProfile\EditMyCarController@editCarModel')->name('car.brand');
        Route::post('mycar/{id}/editSeats', 'UserProfile\EditMyCarController@editSeats')->name('car.editSeats');
        Route::post('mycar/{id}/editCity', 'UserProfile\EditMyCarController@editCity')->name('car.editCity');
        Route::post('mycar/description', 'UserProfile\EditMyCarController@description')->name('car.description');
        Route::post('mycar/{id}/editCarType', 'UserProfile\EditMyCarController@editCarType')->where('car_id', '[0-9]+')->name('car.editCarType');
        Route::post('mycar/{id}/editCostDay', 'UserProfile\EditMyCarController@editCostDay')->where('car_id', '[0-9]+')->name('car.editCostDay');
        Route::post('mycar/{id}/countPlace', 'UserProfile\EditMyCarController@countPlace')->where('car_id', '[0-9]+')->name('car.countPlace');
        Route::post('mycar/{id}/editVolume', 'UserProfile\EditMyCarController@editVolume')->where('car_id', '[0-9]+')->name('car.editVolume');
        Route::post('mycar/{id}/editPower', 'UserProfile\EditMyCarController@editPower')->where('car_id', '[0-9]+')->name('car.editPower');
        Route::post('mycar/{id}/editCarPrice', 'UserProfile\EditMyCarController@editAssessedValue')->where('car_id', '[0-9]+')->name('car.editCarPrice');
        Route::post('mycar/{id}/editMileageInclusive', 'UserProfile\EditMyCarController@editMileageInclusive')->where('car_id', '[0-9]+')->name('car.editMileageInclusive');
        Route::post('mycar/{id}/editRentTime', 'UserProfile\EditMyCarController@editRentTime')->where('car_id', '[0-9]+')->name('car.editRentTime');
        Route::post('mycar/{id}/editCoordinates', 'UserProfile\EditMyCarController@editCoordinates')->where('car_id', '[0-9]+')->name('car.editCoordinates');
        Route::post('mycar/extra', 'UserProfile\EditMyCarController@extra')->name('car.extra');
        Route::post('mycar/ban', 'UserProfile\EditMyCarController@ban')->name('car.ban');
        Route::post('mycar/{id}/editLocation', 'UserProfile\EditMyCarController@location')->name('car.location')->where('car_id', '[0-9]+');
        Route::post('mycar/{id}/editGear', 'UserProfile\EditMyCarController@editGear')->name('car.gear')->where('car_id', '[0-9]+');
        Route::post('mycar/{id}/editTransmission', 'UserProfile\EditMyCarController@editTransmission')->name('car.transmission')->where('car_id', '[0-9]+');
        Route::post('mycar/{id}/editFuelType', 'UserProfile\EditMyCarController@editFuelType')->name('car.fuel')->where('car_id', '[0-9]+');
        Route::post('mycar/{id}/editType', 'UserProfile\EditMyCarController@type')->name('car.type')->where('car_id', '[0-9]+');
       
        Route::post('mycar/{id}/editKaskoOsagoDate', 'UserProfile\EditMyCarController@editKaskoOsagoDate')->name('car.editOsagoKaskoDate')->where('car_id', '[0-9]+');
        Route::post('mycar/{id}/editGos', 'UserProfile\EditMyCarController@editGos')->name('car.editGos')->where('car_id', '[0-9]+');

        Route::get('trips/accept/{booking_id}', 'UserProfile\MyTripsController@accept')->name('trips.accept')->where('booking_id', '[0-9]+');
        Route::get('trips/cancel-transfer/{booking_id}', 'UserProfile\MyTripsController@cancelTransfer')->name('trips.cancelTransfer')->where('booking_id', '[0-9]+');
        Route::get('trips/pass-car/{booking_id}', 'UserProfile\MyTripsController@passCar')->name('trips.passCar')->where('booking_id', '[0-9]+');
        Route::get('trips/payment/{booking_id}', 'UserProfile\MyTripsController@payment')->name('trips.payment')->where('booking_id', '[0-9]+');
        Route::post('trips/rescission/{booking_id}', 'UserProfile\MyTripsController@rescission')->name('trips.rescission')->where('booking_id', '[0-9]+');
        Route::post('trips/addCarInterior/{booking_id}', 'UserProfile\MyTripsController@addCarInterior')->name('trips.addCarInterior')->where('booking_id', '[0-9]+');
        Route::post('trips/addCarFuelResidue/{booking_id}', 'UserProfile\MyTripsController@addCarFuelResidue')->name('trips.addCarFuelResidue')->where('booking_id', '[0-9]+');
        Route::post('trips/addCarMileagePhoto/{booking_id}', 'UserProfile\MyTripsController@addCarMileagePhoto')->name('trips.addCarMileagePhoto')->where('booking_id', '[0-9]+');
        Route::post('trips/addCarMileageInput/{booking_id}', 'UserProfile\MyTripsController@addCarMileageInput')->name('trips.addCarMileageInput')->where('booking_id', '[0-9]+');

        // Route::get('mytrips', 'UserProfile\MyTripsController@myTrips');mycars/28
        // Route::get('/', 'UserProfile\ProfileController@show');
    }
);

Route::group(
    [
        'middleware' => 'auth',
        'prefix' => 'car/',
        'namespace' => 'Web'
    ],
    function() {
        Route::get('create', 'CarsController@getCreatePage')->name('car.create.show');
        Route::post('create', 'CarsController@createCar')->name('car.create');
        Route::get('{car_id}', 'CarsController@getPageOwnerCar')->where('car_id', '[0-9]+')->name('car.show');
        Route::post('{car_id}/feature', 'CarsController@addCarFeature')->where('car_id', '[0-9]+')->name('car.update.feature');
        Route::post('{car_id}/unlock_type_cost', 'CarsController@addCarUnlockTypeCost')->where('car_id', '[0-9]+');
        Route::post('{car_id}/update', 'CarsController@updateCar')->where('car_id', '[0-9]+')->name('car.update.params');
        Route::post('{car_id}/sts', 'ImageController@addPhotoDocsCar')->where('car_id', '[0-9]+');
        Route::post('{car_id}/image', 'ImageController@addPhotoCar')->where('car_id', '[0-9]+');
        Route::post('{car_id}/passport_face', 'ImageController@addPhotoPassportFace')->where('car_id', '[0-9]+');
        Route::post('{car_id}/passport_registry', 'ImageController@addPhotoPassportRegistry')->where('car_id', '[0-9]+');
        Route::post('{car_id}/selfie', 'ImageController@addPhotoSelfie')->where('car_id', '[0-9]+');
        Route::post('{car_id}/mileage_inclusive', 'CarsController@addMileageInclusive')->where('car_id', '[0-9]+');
        Route::post('{car_id}/rent_time', 'CarsController@addRentTime')->where('car_id', '[0-9]+');
        Route::post('{car_id}/vin', 'CarsController@vin')->where('car_id', '[0-9]+')->name('vin');


        Route::post('{car_id}/passportData', 'CarsController@passportData')->where('car_id', '[0-9]+')->name('car.passportData');
        Route::post('{car_id}/osago', 'CarsController@osago')->where('car_id', '[0-9]+')->name('car.osago');
        Route::post('{car_id}/kasko', 'CarsController@kasko')->where('car_id', '[0-9]+')->name('car.kasko');

        Route::post('{car_id}/osago_image', 'ImageController@osagoImage')->where('car_id', '[0-9]+');
        Route::post('{car_id}/kasko_image', 'ImageController@kaskoImage')->where('car_id', '[0-9]+');

        Route::post('{car_id}/redirect', 'CarsController@redirect')->where('car_id', '[0-9]+')->name('car.redirect');

    }
);

Route::group(
    [
        'middleware' => 'auth',
        'prefix' => 'message/',
        'namespace' => 'Web'
    ],
    function() {
        Route::get('list', ['uses' => 'MessagesController@list', 'as' => 'message-list']);
    }
);


Route::group(
    [
        'middleware' => 'staff',
        'prefix' => 'staff/',
        'namespace' => 'Staff'
    ],
    function() {


        Route::get('/', 'StartController@start');
        Route::get('/admin', 'AdminController@start');
        Route::match(['get', 'post'],'/checkauto', 'CheckAutoController@start');
        Route::get('/getModels/{brand_id}','CheckAutoController@getModels');
        Route::get('/checkuser', 'CheckUserController@start');
        Route::get('/bookings', 'BookingsController@start');
        Route::get('/validated-auto','ValidatedCarsController@index');
        Route::get('/deposit-amounts','DepositAmountsController@index');
        Route::post('/update-deposit-price','DepositAmountsController@updateDepositPrice');
        Route::get('/delete-deposit-price/{id}','DepositAmountsController@deleteDepositPrice');


        Route::post('/car-refusal/{car_id}', 'CheckAutoController@setCarRefusalId')->where('car_id', '[0-9]+');
        Route::post('/user-refusal/{user_id}', 'CheckUserController@setUserRefusalId')->where('user_id', '[0-9]+');

     });

Route::group(
    [
        'prefix' => 'payment',
        'namespace' => 'Web'
    ],
    function() {

        Route::get('/', 'PaymentController@payment')->name('payment');
        Route::get('/success', 'PaymentController@success')->name('success');;
        Route::get('/result', 'PaymentController@result')->name('result');;
        Route::get('/fail', 'PaymentController@fail')->name('fail');;
    
    });

Route::get('user/{id}', 'Web\UserController@index')->name('user');