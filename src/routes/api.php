<?php

use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->prefix('car')->group(function () {
    Route::get('getdata', 'Api\AddCarController@index');
    Route::post('getmodel', 'Api\AddCarController@getModel');
    Route::post('add', 'Api\AddCarController@addCar');
    Route::post('{id}/addDetails', 'Api\AddCarController@addCarDetails');
    Route::post('{id}/addTypeCost', 'Api\AddCarController@addCarUnlockTypeCost');
    Route::post('{id}/passport_face', 'Api\AddCarController@addPhotoPassportFace');
    Route::post('{id}/passport_visa', 'Api\AddCarController@addPhotoPassportRegistry');
    Route::post('{id}/docs', 'Api\AddCarController@addPhotoDocsCar');
    Route::post('{id}/osagoPhoto', 'Api\AddCarController@osagoImage');
    Route::post('{id}/kaskoPhoto', 'Api\AddCarController@kaskoImage');
    Route::post('{id}/osago', 'Api\AddCarController@osago');
    Route::post('{id}/kasko', 'Api\AddCarController@kasko');
    Route::post('{id}/photo', 'Api\AddCarController@addPhotoCar');
    Route::post('{id}/addfeatured', 'Api\AddCarController@addFeatured');
});

Route::group(['prefix' => 'auth/'], function () {
    Route::post('login', 'Api\AuthController@login');
    Route::post('register', 'Api\AuthController@signup');
});

// Апи с авторизацией
Route::group(
    ['middleware' => 'auth:api', 'namespace' => 'Api'],
    function() {

//        Route::post('carslist', 'GetCarsController@getSelection');
        Route::post('carslist/{id}', 'GetCarsController@getSelectionCars');
        Route::post('carcard/{id}', 'GetCarsController@getCarCard');

        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');


    }
);

// Апи без авторизацией
Route::group(
    ['namespace' => 'Api'],
    function() {
        Route::get('carslist/{search?}', 'GetCarsController@getSelectionCars')->where('search', '.*')->name('car.search');
        Route::group(
            [
                'prefix' => 'catalog/'
            ],
            function () {
                Route::match(['get', 'post'], '/', 'CatalogController@index');
                Route::get('/gear-types', 'CatalogController@getGearTypes');
                Route::get('/options', 'CatalogController@getOptions');
                Route::get('/car-types', 'CatalogController@getCarTypes');
                Route::get('/extra-options', 'CatalogController@getExtraOptions');
                Route::get('/locations', 'CatalogController@getLocations');
                Route::get('/extra', 'CatalogController@getExtraData');
                Route::post('/filter', 'CatalogController@filter');
            }
        );
        Route::group(
            [
                'prefix' => 'public-profile/{id}'
            ],
            function () {
                Route::get('/','PublicProfileController@allInfo');
                Route::get('/booking-amount','PublicProfileController@getBookingAmount');
                Route::get('/cars','PublicProfileController@getUsersAuto');
            }
        );
        Route::group([
            'prefix' => 'car-card/{id}'
        ], function () {
            Route::get('/', 'CarCardController@getCarInfo');
            Route::get('/features', 'CarCardController@getFeatures');
            Route::get('/similar-autos', 'CarCardController@getSimilarAutos');
            Route::get('/extra-features', 'CarCardController@getExtraFeatures');
            Route::post('/is-available', 'CarCardController@checkForAvailability');
            Route::get('/booked-dates', 'CarCardController@currentBookedDates');
            Route::get('/rent-dates', 'CarCardController@currentRentDates');
            Route::get('/delivery-places', 'CarCardController@deliveryPlaces');
            Route::get('/delivery-places-grouped', 'CarCardController@deliveryPlacesGrouped');
            Route::get('/extra', 'CarCardController@getExtraData');
            Route::get('/mileage', 'CarCardController@getIncludedDistance');
            Route::get('/sendValidatePhone', 'CarCardController@sendValidatePhone');
            Route::get('/validatePhone', 'CarCardController@validatePhone');

        });
    }
);

/* Route::group(
     [
         'prefix' => 'cars/'
     ],
     function() {
         Route::get('brands', 'Api\CarsController@getBrands');
         Route::get('models', 'Api\CarsController@getModels')->where('brandid', '[0-9]+');
         Route::get('create', 'Api\CarsController@getCustomFieldsCars');
         Route::post('create', 'Api\CarsController@createCar');
         Route::post('add', 'Api\CarsController@addCarFeature');
         Route::post('add/doc', 'Api\ImageController@addPhotoDocsCar');
         Route::post('add/photo', 'Api\ImageController@addPhotoCar');

         Route::get('test', 'Api\AddCarController@addCar');
     }
 );*/
