<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use App\User;

class RegisterUser extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Зарегестрированный пользователь
     *
     * @var User $user
     */
    private $user;

    /**
     * временная ссылка подтверждения почты
     * @var string
     */
    private $uri;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        $this->uri = URL::temporarySignedRoute(
            'user.confirm_email',
            now()->addMinutes(30)
        );
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->from(
            env('MAIL_FROM_ADDRESS')
        )->view(
            'emails.confirm',
            [
                'user' => $this->user,
                'uri' => $this->uri
            ]
        );
    }

    /**
     * сформированное тело письма
     * @return string html
     */
    public function getHtml()
    {
        
        // Тело письма
        return View::make(
            'emails.confirm',
            [
                'user' => $this->user,
                'uri' => $this->uri
            ]
        )->render();
    }
}
