<?php

namespace App\Models;

use App\DepositPrice;
use App\ReRunPrice;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Jackpopp\GeoDistance\GeoDistanceTrait;

/**
 * App\Models\Car
 *
 * @property string $vin
 * @property integer $time_alert
 * @property integer $mileage_inclusive
 * @property float $min_overmile_fee
 * @property float $max_overmile_fee
 * @property boolean $need_kasko_and_osago
 * @property integer $assessed_value
 * @property int $id
 * @property int $brand_id
 * @property int $model_id
 * @property int|null $year_of_issue
 * @property int|null $pts_image_id
 * @property int|null $sts_image_id
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $user_id
 * @property int|null $gear_id
 * @property int|null $seat
 * @property int|null $doors
 * @property int|null $car_type
 * @property int|null $cost_day
 * @property int|null $cost_hour
 * @property int|null $unlock_type_id
 * @property int|null $feature_id
 * @property string|null $datefrom
 * @property string|null $dateto
 * @property string|null $location
 * @property int|null $register_step
 * @property int|null $like
 * @property int|null $dislike
 * @property int|null $available
 * @property int|null $validate
 * @property int|null $car_data_id
 * @property int|null $osago
 * @property int|null $kasko
 * @property int|null $need_osago
 * @property int|null $need_kasko
 * @property int|null $cars_extra_id
 * @property int|null $description_id
 * @property string|null $location_description
 * @property string|null $mileage
 * @property int|null $color_id
 * @property int|null $oferta
 * @property int|null $validate_refusal_id
 * @property int|null $transmission_id
 * @property int|null $power
 * @property float|null $engine_volume
 * @property int|null $fuel_type_id
 * @property float $lat
 * @property float $lng
 * @property string|null $model_name
 * @property-read \App\Models\CarsData|null $CarsData
 * @property-read \App\Models\TimeAlertOwnRent|null $alert
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\BookingDate[] $bookingDates
 * @property-read \App\Models\BrandsCar $brand
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CarImage[] $carImages
 * @property-read \App\Models\CarsDeposit $carsDeposit
 * @property-read \App\Models\CarType|null $cartype
 * @property-read \App\Models\CarsColor|null $color
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DateRentCars[] $dateRentCars
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DeliveryCars[] $deliveryCars
 * @property-read \App\Models\CarsDescriptions $description
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Extra[] $extras
 * @property-read \App\Models\CarsFeature|null $feature
 * @property-read \App\Models\FuelType|null $fuelType
 * @property-read \App\Models\CarsGear|null $gear
 * @property-read \Model|\Illuminate\Database\Eloquent\Relations\HasMany|object|null $avatar
 * @property-read mixed $models_name
 * @property-read float|int $re_run_price
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CarImage[] $images
 * @property-read \App\Models\MileageInclusiveCars $mileageInclusive
 * @property-read \App\Models\ModelsCar $model
 * @property-read \App\Models\DeliveryCars $deliveryCars
 * @property-read \App\Models\RentTimeCars $rentTime
 * @property-read \App\Models\RentTimeCars $rentTimeCars
 * @property-read \App\Models\StsImage|null $stsImage
 * @property-read \App\Models\TimeAlertOwnRent|null $timeAlertOwnRent
 * @property-read \App\Models\CarTransmission|null $transmission
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car outside($distance, $measurement = null, $lat = null, $lng = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereAssessedValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereAvailable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereCarDataId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereCarType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereCarsExtraId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereColorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereCostDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereCostHour($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereDatefrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereDateto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereDescriptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereDislike($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereDoors($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereEngineVolume($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereFeatureId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereFuelTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereGearId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereKasko($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereLike($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereLng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereLocationDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereMaxOvermileFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereMileage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereMileageInclusive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereMinOvermileFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereModelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereModelName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereNeedKasko($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereNeedKaskoAndOsago($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereNeedOsago($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereOferta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereOsago($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car wherePower($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car wherePtsImageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereRegisterStep($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereSeat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereStsImageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereTimeAlert($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereTransmissionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereUnlockTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereValidate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereValidateRefusalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereVin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereYearOfIssue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car within($distance, $measurement = null, $lat = null, $lng = null)
 * @mixin \Eloquent
 */
class Car extends Model
{
    use GeoDistanceTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'brand_id',
        'gear_id',
        'model_id',
        'year_of_issue',
        'pts_image_id',
        'sts_image_id',
        'transmission_id',
        'power',
        'engine_volume',
        'cost_day',
        'cost_hour',
        'seat',
        'doors',
        'car_type',
        'cost_day',
        'register_step',
        'car_data_id',
        'osago',
        'kasko',
        'need_osago',
        'need_kasko',
        'available',
        'user_id',
        'unlock_type_id',
        'mileage_inclusive',
        'min_overmile_fee',
        'max_overmile_fee',
        'time_alert',
        'description_id',
        'location',
        'location_description',
        'mileage',
        'assessed_value',
        'color_id',
        'oferta',
        'vin',
        'lat',
        'lng',
        'transmission_id',
        'model_name',
        'osago_date',
        'kasko_date',
        'gos_nomer'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['models_name','re_run_price'];

    /**
     * Валидация данных при создании авто
     *
     * @return array Массив допустимы параметров с ролями
     */
    protected static function getValidateRolesCar(): array
    {
        return [
            'model_id' => 'required|integer',
            'brand_id' => 'required|integer',
            'gear_id' => 'nullable|integer',
            'car_type' => 'required|integer|exists:car_types,id'
        ];
    }

    /**
     * Валидация данных изменения деталей авто
     *
     * @return array Массив допустимы параметров с ролями
     */
    protected static function getValidateRolesCarDetails()
    {
        return [
            'year_of_issue' => 'nullable|integer',
            'seat' => 'nullable|integer',
            'doors' => 'nullable|integer',
        ];
    }

    /**
     * Валидация фотографий
     *
     * @return array Массив допустимы параметров с ролями
     */
    protected static function getValidateRolesCarImage(): array
    {
        return [
            'car_id' => 'required|numeric|exists:cars,id',
            'photo' => 'required|file|mimes:jpeg,bmp,png,pdf'
        ];
    }

    /**
     * Валидация данных изменения дополнительных полей авто
     *
     * @return array Массив допустимы параметров с ролями
     */
    protected static function getValidateRolesCarFeature(): array
    {
        return [
            'car_id' => 'required|integer|min:0|exists:cars,id',
            'air' => 'nullable|boolean',
            'bluetooth' => 'nullable|boolean',
            'audioInput' => 'nullable|boolean',
            'usbCharger' => 'nullable|boolean',
            'childSeat' => 'nullable|boolean',
            'cruiseControl' => 'nullable|boolean',
            'gps' => 'nullable|boolean',
        ];
    }

    /**
     * Валидация данных при создании авто
     *
     * @return array Массив допустимы параметров с ролями
     */
    public static function getMileageInclusive(): array
    {
        return [
            'car_id' => 'required|integer',
            'day' => 'required|integer',
            'week' => 'required|integer',
            'month' => 'required|integer',
        ];
    }

    /**
     * @param Builder|null $query
     * @param null $fromDate
     * @param null $toDate
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder
     */
    public static function getCars(Builder $query = null, $fromDate = null, $toDate = null)
    {
        $fromDate = self::getFromDate($fromDate);
        $toDate = self::getToDate($fromDate, $toDate);
        $query = self::getQuery($query);
        $query = self::checkForAvailability($query, $fromDate, $toDate);
        $query = self::checkForCarAge($query, '>=', 10);
        return $query;
    }

    public static function checkForAvailability($query, $fromDate, $toDate)
    {
        $query = self::checkForDateRentTime($query, $fromDate, $toDate);
        $query = self::checkForBookings($query, $fromDate, $toDate);
        return self::checkForMinMaxRentTime($query, $fromDate, $toDate);
    }


    /**
     * @param Builder|null $query
     * @param null $fromDate
     * @param null $toDate
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder
     */
    public static function checkForDateRentTime(Builder $query, $fromDate, $toDate)
    {
        return $query->whereHas('dateRentCars', function (Builder $query) use ($fromDate, $toDate) {
                $query->whereDate('date_rent_cars.from', '<=', $fromDate)
                    ->whereDate('date_rent_cars.to', '>=', $toDate)
                    ->where('date_rent_cars.active', '=', '1');
            }, '>', 0);

    }

    /** Проверяет сдана ли тачка в пул
     * @param Builder $query
     * @param null $toDate
     * @return Builder
     */
    public static function checkForHasDateRentTime(Builder $query, $toDate = null)
    {
        $toDate = $toDate ?? Carbon::now();
        return $query->whereHas('dateRentCars', function (Builder $query) use ($toDate) {
            $query->whereDate('date_rent_cars.to', '>=', $toDate)
            ->where('date_rent_cars.active', '=', '1');;
        }, '>', 0);

    }

    /** Возвращают авто каталога либо ввиде запроса либо ввиде коллекции
     * @param bool $returnBuilder
     * @return Builder|Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getCatalogCars($returnBuilder = true)
    {
        $query = self::getValidAndAvailableAuto();
        $query = self::checkForCarAge($query,'>=', 10);
        return $returnBuilder ? self::checkForHasDateRentTime($query) : self::checkForHasDateRentTime($query)->get();
    }

    /**
     * @param Builder $query
     * @param $operator
     * @param $value
     * @return Builder
     */
    public static function checkForCarAge(Builder $query, $operator, $value)
    {
        $age = Carbon::now()->subYears($value)->year;

        return $query->where(function (Builder $q) use ($operator, $age) {
            $q->where('year_of_issue', $operator, $age)
                ->orWhereNull('year_of_issue');
        });

    }

    /**
     * @param Builder $query
     * @param Carbon $fromDate
     * @param Carbon $toDate
     * @return Builder
     */
    public static function checkForMinMaxRentTime(Builder $query, Carbon $fromDate, Carbon $toDate)
    {
        $difference = $toDate->diff($fromDate)->days * 86400;
        return $query
            ->with('rentTime')
            ->whereHas('rentTime', function (Builder $query) use ($difference) {
                $query->where('rent_time_cars.min_rent_time', '<=', $difference)
                    ->where('rent_time_cars.max_rent_time', '>=', $difference);
            }, '>', 0);
    }

    public static function checkMinMaxRentTime($car_id, Carbon $fromDate, Carbon $toDate)
    {
        $difference = $toDate->diff($fromDate)->days * 86400;
        $rentTimeCars = RentTimeCars::where('car_id', $car_id)->first();
        if ($rentTimeCars['min_rent_time'] > $difference) {
            return 0; //Забронирована дата меньше чем минимальная разрешенная
        } elseif ($rentTimeCars['max_rent_time'] < $difference) {
            return 1; //Забронирована дата больше чем максимальная разрешенная
        } else{
            return 2; //Все окей
        }
    }


    /**
     * @param null $fromDate
     * @return Carbon
     */
    public static function getFromDate($fromDate = null)
    {
        return $fromDate ? Carbon::createFromFormat('Y-m-d H:i:s', $fromDate) : Carbon::now();

    }

    /**
     * @param $fromDate
     * @param null $toDate
     * @return Carbon
     */
    public static function getToDate(Carbon $fromDate, $toDate = null)
    {
        return $toDate ? Carbon::createFromFormat('Y-m-d H:i:s', $toDate) : (clone $fromDate)->addDay();
    }

    /**
     * @param null $query
     * @return Builder
     */
    private static function getQuery($query = null)
    {
        return ($query ?? self::getValidAndAvailableAuto());
    }


    /**
     * @param Builder $query
     * @param $fromDate
     * @param $toDate
     * @return Builder|\Illuminate\Database\Query\Builder
     */
    public static function checkForBookings(Builder $query, $fromDate, $toDate)
    {
        return $query
            ->with('bookingDates')
            ->whereHas('bookingDates', function (Builder $query) use ($fromDate, $toDate) {
                $query->where(function (Builder $query) use ($fromDate) {
                    $query
                        ->where('booking_date.datefrom', '>=', $fromDate)
                        ->orWhere('booking_date.dateto', '>=', $fromDate);
                })->where(function (Builder $query) use ($toDate) {
                    $query
                        ->where('booking_date.datefrom', '<=', $toDate)
                        ->orWhere('booking_date.dateto', '<=', $toDate);
                });
            }, '=', 0);
    }

    /**
     * @return Car|\Illuminate\Database\Eloquent\Builder
     */
    public static function getValidAndAvailableAuto()
    {
        return self::with(['brand', 'model', 'gear', 'cartype', 'carImages', 'dateRentCars', 'deliveryCars'])
            ->where('available', 1)
            ->where('validate', 1);
    }

    /**
     * @param $id
     * @return array|\Illuminate\Http\JsonResponse
     */
    public static function selectOrError($id)
    {
        $car = self::with(self::getFullInfoEagerLoads())->select(self::getFullInfoSelects())->find($id);

        if (!$car) {
            return response()->json('error(car not found)');
        }

        return \Helpers::replaceNullToEmptyString($car->toArray());
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public static function getImages(Builder $query)
    {
        return $query
            ->join('cars_images as i', 'i.car_id', '=', 'cars.id')
            ->where('avatar', 1);
    }

    /**
     * @param Builder $query
     * @param $model_id
     * @return Builder
     */
    public static function filterByModel(Builder $query, $model_id)
    {
        return $query->where('model_id', $model_id);
    }

    /**
     * @param Builder $query
     * @param $brand_id
     * @return Builder
     */
    public static function filterByBrand(Builder $query, $brand_id)
    {
        return $query->where('brand_id', $brand_id);
    }

    /**
     * @param Builder $query
     * @param $gear_id
     * @return Builder
     */
    public static function filterByGear(Builder $query, $gear_id)
    {
        return $query->where('gear_id', $gear_id);
    }

    /**
     * @param Builder $query
     * @param $car_type_id
     * @return Builder
     */
    public static function filterByCarType(Builder $query, $car_type_id)
    {
        return $query->where('car_type', $car_type_id);
    }

    /**
     * @param Builder $query
     * @param $location_id
     * @return Builder
     */
    public static function filterByLocation(Builder $query, $location_id)
    {

        $query = $query->with('deliveryCars')->whereHas('deliveryCars', function (Builder $query) use ($location_id) {
            $query->where('place', $location_id);
        }, '>', 0);

        return $query;
    }

    public static function filterByCity(Builder $query, $city)
    {

        return $query->where('city', $city);
    }

    /**
     * @param Builder $query
     * @param $coordinates
     * @return Builder
     */
    public static function filterByCoordinates(Builder $query, $coordinates)
    {
        $radius = env('SEARCH_RADIUS', 50);
        $coordinates = explode(",", $coordinates);
        $lat = $coordinates[0];
        $lng = $coordinates[1];
        $carsByNear = Car::within($radius, 'kilometers', $lat, $lng)->pluck('id')->toArray();
        $filteredCarsIds = (clone $query)->pluck('cars.id')->toArray();
        $filteredByNearCarsIds = array_intersect($carsByNear, $filteredCarsIds);
        return $query->whereIn('cars.id', $filteredByNearCarsIds);
    }

    /**
     * @param Builder $query
     * @param $minPrice
     * @return Builder
     */
    public static function filterByMinPrice(Builder $query, $minPrice)
    {
        return $query->where('cost_day', '>=', $minPrice);
    }

    /**
     * @param Builder $query
     * @param $minPlaces
     * @return Builder
     */
    public static function filterByMinPlaces(Builder $query, $minPlaces)
    {
        return $query->where('seat', '>=', $minPlaces);
    }

    /**
     * @param Builder $query
     * @param $maxPrice
     * @return Builder
     */
    public static function filterByMaxPrice(Builder $query, $maxPrice)
    {
        return $query->where('cost_day', '<=', $maxPrice);
    }

    /**
     * @param Builder $query
     * @param $selectedOptions
     * @return Builder
     */
    public static function filterBySelectedOptions(Builder $query, $selectedOptions)
    {
        if (count($selectedOptions)) {
            $query = $query->join('cars_feature as cf', 'cars.feature_id', '=', 'cf.id');
            foreach ($selectedOptions as $selectedOption) { 
                $query = $query->where("cf.$selectedOption", 1);
            }
        }
        return $query;
    }

    /**
     * @param Builder $query
     * @param $selectedExtraOptions
     * @return Builder
     */
    public static function filterBySelectedExtraOptions(Builder $query, $selectedExtraOptions)
    {
        if (count($selectedExtraOptions)) {
            $query = $query->with('extras')->whereHas('extras', function (Builder $query) use ($selectedExtraOptions) {
                $query->whereIn('extra.id', $selectedExtraOptions);
            }, '=', count($selectedExtraOptions));
        }
        return $query;
    }


    /**
     * @return array
     */
    public static function getSelectParameters()
    {
        return ['cars.id', 'cost_day', 'year_of_issue', 'car_type', 'available', 'validate', 'brand_id', 'model_id', 'gear_id', 'seat', 'model_name'];
    }

    /**
     * @return array
     */
    public static function getProfileSelectParameters()
    {
        return ['cars.id', 'cost_day', 'year_of_issue', 'brand_id', 'model_id', 'seat', \DB::raw('count(*) as booking_count'), 'model_name'];
    }

    public static function getProfileInfoEagerLoads()
    {
        return [
            'carImages:car_id,name,avatar',
            'model:id,name',
            'brand:id,name',
        ];
    }

    public static function getProfileInfo($query)
    {
        return $query
            ->leftJoin('booking_date as bd', 'bd.car_id', 'cars.id')
            ->with(Car::getProfileInfoEagerLoads())
            ->select(self::getProfileSelectParameters())
            ->groupBy('cars.id');
    }

    /**
     * @return array
     */
    public static function getFullInfoEagerLoads()
    {
        return [
            'carImages:car_id,name',
            'user:id,name,validate,avatar',
            'model:id,name',
            'brand:id,name',
            'gear:id,name',
            'cartype:id,name',
            'mileageInclusive:car_id,day,week,month',
            'description:cars_id,description',
            'color:id,name',
            'transmission:id,name,slug',
            'fuelType:id,name,slug',
            'rentTimeCars:car_id,min_rent_time,max_rent_time'
        ];

    }

    /**
     * @return array
     */
    public static function getFullInfoSelects()
    {
        return ['cars.id', 'assessed_value', 'location_description', 'mileage', 'location', 'cars.kasko', 'cars.osago', 'seat', 'cost_day', 'user_id', 'model_id', 'year_of_issue', 'brand_id', 'gear_id', 'car_type', 'color_id','transmission_id','power','engine_volume','fuel_type_id','cars.lat','cars.lng','model_name'];
    }




    /**
     * Валидация данных при создании авто
     *
     * @return array Массив допустимы параметров с ролями
     */
    public static function getRentTimeCars(): array
    {
        return [
            'car_id' => 'required|integer',
            'min_rent_time' => 'required',
            'max_rent_time' => 'required',
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookingDates()
    {
        return $this->hasMany('App\Models\BookingDate');
    }


    /**
     * @return array Список всех текущих забронированных дней по дням
     */
    public function bookingDatesByDays()
    {
        $result = [];
        foreach ($this->currentBookingDates as $currentBookingDate) {
            $fromDate = self::getFromDate($currentBookingDate->datefrom);
            $toDate = self::getToDate($fromDate, $currentBookingDate->dateto);
            $days = $toDate->diff($fromDate)->days;
            $result[] = $fromDate->toDateString();
            for ($i = 0; $i < $days; $i++) {
                $result[] = $fromDate->addDay()->toDateString();
            }
        }
        return $result;
    }

    /**
     * @return array
     */
    public function rentDatesByDays()
    {
        return $this->currentRentTimes;
    }

    /** Все текущие забронированные даты
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Query\Builder
     */
    public function currentBookingDates()
    {
        return $this->bookingDates()
            ->whereDate('dateto', '>=', Carbon::now());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Query\Builder
     */
    public function currentRentTimes()
    {
        return $this->dateRentCars()->whereDate('to', '>=', Carbon::now())->where('active', 1)->select(['from', 'to']);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function model()
    {
        return $this->belongsTo('App\Models\ModelsCar');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo('App\Models\BrandsCar');
    }

    public function transmission()
    {
        return $this->belongsTo('App\Models\CarTransmission','transmission_id','id');
    }

    public function fuelType()
    {
        return $this->belongsTo('App\Models\FuelType');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gear()
    {
        return $this->belongsTo('App\Models\CarsGear');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cartype()
    {
        return $this->belongsTo('App\Models\CarType', 'car_type', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function feature()
    {
        return $this->belongsTo('App\Models\CarsFeature');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rentTimeCars()
    {
        return $this->belongsTo('App\Models\RentTimeCars', 'id', 'car_id');
    }

    public function getSelectedFeatures()
    {
        $result = [];
        /* @var  $featuresNames Collection */
        $featuresNames = FeatureName::pluck('ru_name', 'en_name');
        if ($this->feature and $this->feature->count()) {
            foreach ($this->feature->toArray() as $feature => $value) {
                if ($value === 1) {
                    $result[$feature] = $featuresNames->get($feature);
                }

            }
        }
        return $result;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function stsImage()
    {
        return $this->belongsTo('App\Models\StsImage');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function carImages()
    {
        return $this->hasMany('App\Models\CarImage', 'car_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function extras()
    {
        return $this->belongsToMany('App\Models\Extra', 'extra_cars', 'cars_id', 'extra_id')
            ->wherePivot('active', 1)
            ->withPivot(['extra_cost', 'extra_time_id']);
    }

    /**
     * @return array
     */
    public function extraFeatures()
    {
        $result = [];
        if ($this->extras and $this->extras->count()) {
            foreach ($this->extras->toArray() as $extra) {
                $result_item = [
                    'name' => $extra['name'],
                    'desc' => $extra['desc'],
                    'price' => $extra['pivot']['extra_cost'] ?? 0
                ];
                $extra_time_id = $extra['pivot']['extra_time_id'] ?? null;
                if ($extra_time_id) {
                    $result_item['time'] = ExtraTime::find($extra_time_id)->name ?? '';
                }
                $result[] = $result_item;
            }
        }

        return $result;
    }

    public function deliveryPlacesGrouped()
    {
        $addresses = RentPlace::pluck("name", "id")->toArray();
        $result = [
            "airport" => [],
            "free" => [
                "id" => 0,
                "price" => 0,
                "title" => $this->location
            ]
        ];
        foreach ($this->deliveryCars as $delivery) {
            if ($delivery->place === 9) {
                $result["delivery"] = [
                    "price" => $delivery->cost,
                    "title" => "На указанный вами адрес",
                    "id" => 9
                ];
            } else {
                $result["airport"][] = [
                    "price" => $delivery->cost,
                    "title" => $addresses[$delivery->place]
                ];
            }
        }
        return $result;
    }

    public function deliveryPlaces()
    {
        $addresses = RentPlace::pluck("name", "id")->toArray();
        $result = [
            [
                "id" => 0,
                "price" => 0,
                "title" => $this->location
            ]
        ];
        foreach ($this->deliveryCars as $delivery) {
            if ($delivery->place === 9) {
                $result[] = [
                    "price" => $delivery->cost,
                    "title" => "На указанный вами адрес",
                    "id" => $delivery->place
                ];
            } else {
                $result[] = [
                    "price" => $delivery->cost,
                    "title" => $addresses[$delivery->place],
                    "id" => $delivery->place
                ];
            }
        }
        return $result;
    }




    public function CarsData()
    {
        return $this->belongsTo('App\Models\CarsData', 'car_data_id');
    }

    public function timeAlertOwnRent()
    {
        return $this->belongsTo(TimeAlertOwnRent::class, 'time_alert');
    }

    public function dateRentCars()
    {
        return $this->hasMany(DateRentCars::class);
    }

    public function deliveryCars()
    {
        return $this->hasMany(DeliveryCars::class);
    }

    public function mileageInclusive()
    {
        return $this->hasOne(MileageInclusiveCars::class, 'car_id');
    }

    public function rentTime()
    {
        return $this->hasOne(RentTimeCars::class, 'car_id');
    }

    public function description()
    {
        return $this->hasOne(CarsDescriptions::class, 'cars_id');
    }

    public function alert()
    {
        return $this->belongsTo(TimeAlertOwnRent::class, 'time_alert');
    }

    public function color()
    {
        return $this->belongsTo(CarsColor::class, 'color_id');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public static function getValidatedCars()
    {
        $cars = DB::table('cars as c')
            ->leftJoin('users as u', 'c.user_id', 'u.id')
            ->leftJoin('cars_data as data', 'c.car_data_id', 'data.id')
            ->leftJoin('brands_cars as b', 'c.brand_id', 'b.id')
            ->leftJoin('models_cars as m', 'c.model_id', 'm.id')
            ->where('c.validate', 1)
            ->select('c.created_at', 'c.osago_date', 'c.kasko_date', 'c.id as car_id', 'u.id as user_id', 'u.name as user_name', 'u.email', 'u.phone', 'b.name as brand', 'm.name as model', 'data.osago_image', 'data.kasko_image')
            ->get();

        return $cars;
    }

    /**
     * @return array
     */
    public static function getFilterFunctions()
    {
        return [
            'brand_id' => 'filterByBrand',
            'model_id' => 'filterByModel',
            'gear_id' => 'filterByGear',
            'car_type_id' => 'filterByCarType',
            'minPrice' => 'filterByMinPrice',
            'maxPrice' => 'filterByMaxPrice',
            'minPlaces' => 'filterByMinPlaces',
            'selectedOptions' => 'filterBySelectedOptions',
            'selectedExtraOptions' => 'filterBySelectedExtraOptions',
            'order' => 'customOrderBy',
            'location_id' => 'filterByLocation',
            'city' => 'filterByCity',
            'coordinates' => 'filterByCoordinates'
        ];
    }


    /**
     * @param $id
     * @return Car
     */
    public static function getCarToUserOrStaff($id)
    {
        //Если есть доступ от администрации для редактирования любых автомобилей, то ищем среди всех автомобилей
        if (Auth::user()->hasAccessToEdit()) {
            return Car::findOrFail($id);
        } //Если нет, то только среди своих
        else {
            return Auth::user()->cars()->findOrFail($id);
        }

    }

    /**
     * @param $query
     * @param $order
     * @param string $orderBy
     * @return mixed
     */
    public static function customOrderBy($query, $order, $orderBy = 'cost_day')
    {
        return $query->orderBy($orderBy, $order);
    }


    public function carsDeposit()
    {
        return $this->hasOne('App\Models\CarsDeposit');
    }


    /**
     * @return int
     */
    public function getCalculatedDeposit()
    {
        if (isset($this->carsDeposit->price))
            return $this->carsDeposit->price;

        $depositPrices = DepositPrice::orderBy('car_price', 'desc')->get();
        foreach ($depositPrices as $depositPrice) {
            if ($this->assessed_value >= $depositPrice->car_price) {
                return $depositPrice->deposit_price;
            }
        }

        return 0;
    }

    public function getModelsNameAttribute()
    {
        if ($this->model_name)
            return $this->model_name;
        else {
            return $this->model->name ?? '';
        }
    }

    /**
     * @return float|int
     */
    public function getReRunPriceAttribute()
    {
        $reRunPrices = ReRunPrice::orderBy('car_price', 'asc')->get();
        foreach ($reRunPrices as $reRunPrice) {
            if ($this->assessed_value <= $reRunPrice->car_price) {
                return $this->assessed_value * $reRunPrice->coef;
            }
        }

        return 0;
    }

    /** Все фото авто
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany('App\Models\CarImage');
    }

    /** Получить аватар авто по аттрибуту avatar. Пример: $car->avatar
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany|object|null
     */
    public function getAvatarAttribute()
    {
        return $this->images()->where('avatar', 1)->first();
    }
    /** Высчисляем и возвращаем включенный пробег
     * @param Car $car
     * @param Carbon $fromDate
     * @param Carbon $toDate
     * @return float|int
     */
    public static function calculateIncludedDistance(Car $car, Carbon $fromDate, Carbon $toDate)
    {
        $difference = $toDate->diff($fromDate);
        $sum = $difference->m * $car->mileageInclusive->month
            + ($difference->d % 7) * $car->mileageInclusive->day
            + ((int)($difference->d / 7)) * $car->mileageInclusive->week;

        if ($difference->h > 0) {
            $sum += $car->mileageInclusive->day;
        }

        return $sum;
    }

}
