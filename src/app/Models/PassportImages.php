<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PassportImages
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PassportImages newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PassportImages newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PassportImages query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PassportImages whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PassportImages whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PassportImages whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PassportImages whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PassportImages extends Model
{
    protected $table = 'passport_images';
    protected $fillable = [
        'name',
        'id',
    ];
}
