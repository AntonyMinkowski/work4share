<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\RentTime
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $name
 * @property int|null $time
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RentTime newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RentTime newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RentTime query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RentTime whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RentTime whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RentTime whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RentTime whereTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RentTime whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class RentTime extends Model
{
    protected $table = 'rent_time';
    static function plural_type($n) { 
        return ($n%10==1 && $n%100!=11 ? 0 : ($n%10>=2 && $n%10<=4 && ($n%100<10 || $n%100>=20) ? 1 : 2)); 
    } 
}
