<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Extra
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $name
 * @property string|null $desc
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Extra newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Extra newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Extra query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Extra whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Extra whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Extra whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Extra whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Extra whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Extra extends Model
{
    protected $table = 'extra';
}
