<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DateRentCars
 *
 * @property int $id
 * @property int $car_id
 * @property string $from
 * @property string $to
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $active
 * @property-read \App\Models\Car $car
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DateRentCars newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DateRentCars newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DateRentCars query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DateRentCars whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DateRentCars whereCarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DateRentCars whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DateRentCars whereFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DateRentCars whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DateRentCars whereTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DateRentCars whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DateRentCars extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'car_id',
        'to',
        'from',
        'active'
    ];

    protected $table = 'date_rent_cars';

    public function car()
    {
        return $this->belongsTo(Car::class);
    }
}
