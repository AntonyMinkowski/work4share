<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CarsColor
 *
 * @property int $id
 * @property string|null $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsColor newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsColor newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsColor query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsColor whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsColor whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsColor whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsColor whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CarsColor extends Model
{
    //
}
