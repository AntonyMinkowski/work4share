<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ModelsCar
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $brand_id
 * @property int $is_other
 * @property-read \App\Models\BrandsCar $brand
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ModelsCar newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ModelsCar newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ModelsCar query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ModelsCar whereBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ModelsCar whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ModelsCar whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ModelsCar whereIsOther($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ModelsCar whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ModelsCar whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ModelsCar extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public function brand()
    {
        return $this->belongsTo('App\Models\BrandsCar', 'models_cars_brand_id_foreign', 'brand_id');
    }
}
