<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BookingMileage
 *
 * @property int $id
 * @property int|null $booking_id
 * @property string|null $mileage_before
 * @property string|null $mileage_after
 * @property string|null $mileage_photo_before
 * @property string|null $mileage_photo_after
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingMileage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingMileage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingMileage query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingMileage whereBookingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingMileage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingMileage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingMileage whereMileageAfter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingMileage whereMileageBefore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingMileage whereMileagePhotoAfter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingMileage whereMileagePhotoBefore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingMileage whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BookingMileage extends Model
{
    protected $table = 'booking_mileages';

    protected $fillable = [
        'booking_id',
        'mileage_before',
        'mileage_after',
        'mileage_photo_before',
        'mileage_photo_after'
    ];
}
