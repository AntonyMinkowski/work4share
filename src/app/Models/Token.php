<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Events\TokenSaved;
use Illuminate\Support\Carbon;

/**
 * App\Models\Token
 *
 * @property int $id
 * @property string $hash
 * @property string $phone
 * @property int $user_id
 * @property int $used
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Token apply($phone)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Token newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Token newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Token query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Token whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Token whereHash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Token whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Token wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Token whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Token whereUsed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Token whereUserId($value)
 * @mixin \Eloquent
 */
class Token extends Model
{
    /**
     * Время валидных токенов в минутах
     * @var integer
     */
    const EXPIRATION_TIME = 20;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    protected $dispatchesEvents = [
        'creating' => TokenSaved::class,
    ];

    public function scopeApply($query, $phone)
    {
        return $query->where(
            'phone',
            $phone
        )->where(
            'used',
            false
        )->where(
            'updated_at',
            '>' ,
            Carbon::now()->subMinutes(self::EXPIRATION_TIME)
        )->orderBy('updated_at', 'desc');
    }

    /**
     * Is the current token expired
     *
     * @return bool
     */
    public function isExpired()
    {
        return $this->created_at->diffInMinutes(Carbon::now()) > static::EXPIRATION_TIME;
    }
}
