<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BookingStatus
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingStatus whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingStatus whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BookingStatus extends Model
{
    protected $table = 'booking_status';
}
