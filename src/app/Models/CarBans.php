<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CarBans
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarBans newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarBans newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarBans query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarBans whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarBans whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarBans whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarBans whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CarBans extends Model
{
    //
}
