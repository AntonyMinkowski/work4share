<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PaymentDelivery
 *
 * @property int $id
 * @property int $delivery_cars_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentDelivery newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentDelivery newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentDelivery query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentDelivery whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentDelivery whereDeliveryCarsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentDelivery whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentDelivery whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PaymentDelivery extends Model
{
    protected $table = 'payment_delivery';
    protected $fillable = [
        'delivery_cars_id'
    ];
}
