<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CarsFeature
 *
 * @property int $id
 * @property int $air
 * @property int $bluetooth
 * @property int $audio_input
 * @property int $usb_charger
 * @property int $child_seat
 * @property int $cruise_control
 * @property int $gps
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsFeature newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsFeature newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsFeature query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsFeature whereAir($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsFeature whereAudioInput($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsFeature whereBluetooth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsFeature whereChildSeat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsFeature whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsFeature whereCruiseControl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsFeature whereGps($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsFeature whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsFeature whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarsFeature whereUsbCharger($value)
 * @mixin \Eloquent
 */
class CarsFeature extends Model
{
    /** @var string имя таблицы */
    protected $table = 'cars_feature';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'air',
        'car_id',
        'bluetooth',
        'audio_input',
        'usb_charger',
        'child_seat',
        'cruise_control',
        'gps'
    ];
}
