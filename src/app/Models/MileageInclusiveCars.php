<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\MileageInclusiveCars
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $car_id
 * @property int $day
 * @property int $week
 * @property int $month
 * @property-read \App\Models\Car $cars
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MileageInclusiveCars newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MileageInclusiveCars newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MileageInclusiveCars query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MileageInclusiveCars whereCarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MileageInclusiveCars whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MileageInclusiveCars whereDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MileageInclusiveCars whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MileageInclusiveCars whereMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MileageInclusiveCars whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MileageInclusiveCars whereWeek($value)
 * @mixin \Eloquent
 */
class MileageInclusiveCars extends Model
{
    protected $table = 'mileage_inclusive_cars';

    protected $fillable = [
        'car_id',
        'day',
        'week',
        'month'
    ];
    public function cars()
    {
        return $this->belongsTo('App\Models\Car', 'car_id');
    }
}
