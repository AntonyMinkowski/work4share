<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ExtraCars
 *
 * @property int $id
 * @property int|null $cars_id
 * @property int|null $extra_id
 * @property int|null $extra_time_id
 * @property int|null $extra_cost
 * @property int|null $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Extra|null $extra
 * @property-read \App\Models\ExtraTime|null $time
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExtraCars newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExtraCars newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExtraCars query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExtraCars whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExtraCars whereCarsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExtraCars whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExtraCars whereExtraCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExtraCars whereExtraId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExtraCars whereExtraTimeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExtraCars whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExtraCars whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ExtraCars extends Model
{
    protected $table = 'extra_cars';

    protected $fillable = [
        'cars_id',
        'active',
        'extra_id',
        'extra_time_id',
        'extra_cars',
        'extra_cost'
    ];

    public function extra()
    {
        return $this->belongsTo(Extra::class, 'extra_id');
    }
    public function time()
    {
        return $this->belongsTo(ExtraTime::class, 'extra_time_id');
    }
}
