<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ChatRoom
 *
 * @property int $id
 * @property int $car_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatRoom newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatRoom newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatRoom query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatRoom whereCarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatRoom whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatRoom whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatRoom whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ChatRoom whereUserId($value)
 * @mixin \Eloquent
 */
class ChatRoom extends Model
{
    protected $table = 'chat_room';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'car_id',
        'user_id',
    ];
}
