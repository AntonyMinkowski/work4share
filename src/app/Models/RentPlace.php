<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\RentPlace
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DeliveryCars[] $deliveryCars
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RentPlace newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RentPlace newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RentPlace query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RentPlace whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RentPlace whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RentPlace whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RentPlace whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class RentPlace extends Model
{
    protected $table = 'rent_place';

    public function deliveryCars()
    {
        return $this->hasMany('App\Models\DeliveryCars', 'place');
    }
}
