<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\RentTimeCars
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $car_id
 * @property int|null $min_rent_time
 * @property int|null $max_rent_time
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RentTimeCars newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RentTimeCars newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RentTimeCars query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RentTimeCars whereCarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RentTimeCars whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RentTimeCars whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RentTimeCars whereMaxRentTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RentTimeCars whereMinRentTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RentTimeCars whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class RentTimeCars extends Model
{
    protected $fillable = [
        'car_id',
        'min_rent_time',
        'max_rent_time'
    ];
}
