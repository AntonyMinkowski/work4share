<?php

namespace App\Events;

use App\Models\Token;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Hash;
/**
 *
 */
class TokenSaved
{
    use SerializesModels;

    public $token;

    public function __construct(Token $token)
    {
        $this->token = $token;
        $this->token->hash = $this->generateHash();
    }

    /**
     * генерируем хеш, чтобы не хранить код в чистом виде
     * @return string хешированный код
     */
    private function generateHash()
    {
        return Hash::make($this->generateCode());
    }

    /**
     * генерация шестизначного кода
     * @param  integer $code_length [description]
     * @return [type]               [description]
     */
    private function generateCode($code_length = 5)
    {
        $min = pow(10, $code_length);
        $max = $min * 10 - 1;
        $this->code = mt_rand($min, $max);

        return $this->code;
    }
}
