<?php

namespace App\Services;

use App\User;
use Illuminate\Support\Facades\Storage;

/**
 * Сервис для работы с первональными фото
 *
 * Class PersonalImagesService
 */
class PersonalImagesService
{

    private $storage;
    private $prefix_path;
    private $user;

    public function __construct(User $user)
    {
        $this->storage = Storage::disk('ice');
        $this->prefix_path = 'personal';
        $this->user = $user;
    }

    public function save(string $file_name, $file)
    {

        $full_name = $this->getFullName($this->user, $file_name);

        $this->storage->put(
            $full_name,
            $file
        );

        return $this->storage->exists($full_name);

    }

    private function getPath(User $user): string
    {
        return sprintf('%s/%s/', $this->prefix_path, $user->id);
    }

    private function getFullName(User $user, string $file_name): string
    {
        return sprintf('%s%s', $this->getPath($user), $file_name);
    }
}
