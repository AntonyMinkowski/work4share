<?php

namespace App\Services\SMSService;

/**
 *
 */
interface InterfaceSMS
{
    public function setPhone(string $phone);

    public function getPhone(): string;

    public function setMessage(string $message);

    public function getMessage(): string;

    public function getDataArray(): array;
}
