<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use App\Services\NormalizeKeysToModel;
use Validator;
use App\Models\{Car, ModelsCar, BrandsCar, CarsGear, CarsFeature, CarType};
use Illuminate\Support\Facades\DB;

/**
 * Контроллер работы с авто
 */
class CarsController extends Controller
{
    /**
     * Получаем кастомные поля для заполнения перед сохранением авто
     *
     * @return Collection      Коллекция полей для заполнения [ModelsCar, BrandsCar, CarsGear]
     */
    protected function getProperty(): Collection
    {
        return collect([
            'brand' => BrandsCar::all(),
            'gear' => CarsGear::all(),
            'car_type' => CarType::all()
        ]);
    }

    public function getBrands()
    {
        $brands_cars = DB::select('select `id`, `name` from brands_cars');
        return response()->json(['brands' => $brands_cars]);
    }

    public function getModels(Request $request)
    {
        return response()->json(
            collect(
                BrandsCar::findOrFail($request->get('brandid'))->models
                )->map(function ($model) {
                    return collect($model)->only([
                        'id',
                        'name',
                        'brand_id'
                    ])->toArray();
                })
        );
    }

    /**
     * Сохраняем дополнительные поля авто
     *
     * @param  Collection $params коллекция значений полей
     * @param  Car        $car    Модель авто, у которой изменяем поля
     * @return Collection         содержит результат выполнения сохранения
     */
    protected function saveCarFeature(Collection $params, Car $car): Collection
    {
        $result = collect();
        try {
            $result->put('carId', $car->id);
            if ($feature_id = $car->feature_id) {
                $feature = CarsFeature::updateOrCreate(
                    $params->put('id', $feature_id)->toArray()
                );
                $feature->save();
            } else {
                $feature = CarsFeature::create($params->toArray());
            }

            $car->feature_id = $feature->id;
            $result->put('result', $car->save());
        } catch (QueryException $e) {
            $result->put('result', false);
            $result->put('code', "1");
        } finally {
            return $result;
        }
    }

    /**
     * Создаем новый объект "авто" с привязкой к текущему авторизованному юзеру
     *
     * @param  Collection $collect [description]
     * @return Collection          [description]
     */
    protected function saveCar(Collection $collect): Collection
    {
        $result = collect();
        $car = Car::create($collect->toArray());
        $user = Auth::user();
        $car->user_id = $user->id;


        try {
            $result->put('result', $car->save());
            $result->put('carId', $car->id);
        } catch (QueryException $e) {
            $result->put('result', false);
            $result->put('code', "1");
        } finally {
            return $result;
        }
    }

    /**
     * Валидация данных изменения дополнительных полей авто
     *
     * @return array Массив допустимы параметров с ролями
     */
    protected static function getValidateRolesCarFeature(): array
    {
        return [
            'CarId' => 'required|integer|min:0|exists:cars,id',
            'air' => 'nullable|boolean',
            'bluetooth' => 'nullable|boolean',
            'audioInput' => 'nullable|boolean',
            'usbCharger' => 'nullable|boolean',
            'childSeat' => 'nullable|boolean',
            'cruiseControl' => 'nullable|boolean',
            'gps' => 'nullable|boolean',
            'unlockType' => 'nullable|integer'
        ];
    }

    /**
     * Валидация данных изменения дополнительных полей авто
     *
     * @return array Массив допустимы параметров с ролями
     */
    protected static function getValidateRolesCar(): array
    {
        return [
            'modelid' => 'required|integer',
            'brandid' => 'required|integer',
            'gearid' => 'nullable|integer',
            'yearOfIssue' => 'nullable|date_format:Y',
            'seat' => 'nullable|integer',
            'doors' => 'nullable|integer',
            'carType' => 'required|integer|exists:car_types,id'
        ];
    }
}
