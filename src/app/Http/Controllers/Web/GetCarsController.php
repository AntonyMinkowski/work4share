<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\SearchRequest;
use App\Models\{BrandsCar, Car, CarsFeature, CarsGear, CarType, ModelsCar, RentPlace};
use App\Resources\Feature;
use App\Services\SearchCarsService;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Mail\NotificationsEmail;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class GetCarsController extends Controller
{
    public function getSelection () {
        $selectionforcars = DB::table('selectionforcars')->select('id', 'name', 'color')->get();
        $brandsCars = DB::table('brands_cars')
            ->select('brands_cars.id', 'brands_cars.name')
            ->orderBy('name')
            ->get();
        return view('catalog.selection-list', [
            'selectionforcars' => $selectionforcars,
            'brandsCars' => $brandsCars
        ]);
    }

    public function getFirstCars () {
        $query = DB::table('selectionCars')
            ->join('cars', 'selectionCars.car_id', '=', 'cars.id')
            ->join('brands_cars', 'cars.brand_id', '=', 'brands_cars.id')
            ->leftjoin('car_types', 'cars.car_type', '=', 'car_types.id')
            ->leftjoin('cars_gears', 'cars.gear_id', '=', 'cars_gears.id')
            ->leftjoin('cars_images', 'cars.id', '=', 'cars_images.car_id')
            ->join('models_cars', 'cars.model_id', '=', 'models_cars.id')
            ->select('cars.id', 'cars.available', 'cars_images.name as image', 'brands_cars.name as brand', 'models_cars.name as model', 'cars.year_of_issue', 'cars.seat', 'cars.doors', 'cars.cost_day', 'car_types.name as car_type', 'cars_gears.name as gear');


        if (isset($_GET['brandid'])) {
            $query = $query
                ->where('cars.brand_id', $_GET['brandid']);
        }
        if (isset($_GET['modelid'])) {
            $query = $query
                ->where('cars.model_id', $_GET['modelid']);
        }

        $selectionCars = $query
            ->where('cars.available', '1')
            ->where('cars.validate', '1')
            ->get();

        $brandsCars = DB::table('brands_cars')
            ->select('brands_cars.id', 'brands_cars.name')
            ->orderBy('name')
            ->get();

        return view('cars.list', [
            'selectionCars' => $selectionCars,
            'brandsCars' => $brandsCars,
            'param' => $_GET
        ]);
    }

    public function getSelectionCars(SearchRequest $request) {
        $params = collect($request->validated());

        $feature = (new Feature(new CarsFeature))->toArray($request);
        $params->put('feature', collect($feature)->filter());
        /** @var SearchCarsService $search */
        $search = app(SearchCarsService::class);
        /** @var LengthAwarePaginator $paginator */
        $paginator = $search
            ->search($params)
            ->paginate(9)
            ->appends($request->validated());

        $cars = $paginator->map(function (Car $car) {
            return collect((new \App\Resources\Car($car)));
        });

        $brandsCars = BrandsCar::all();
        $gearCars = CarsGear::all();
        $typeCars = CarType::all();
        $rentPlace = RentPlace::all();

        $models = false;
        if ($brand_id = $request->get('brandid')) {
            $models = ModelsCar::where('brand_id', $brand_id)->get();
        }

        return view('cars.list', [
            'paginator' => $paginator,
            'selectionCars' => $cars,
            'brandsCars' => $brandsCars,
            'gearCars' => $gearCars,
            'typeCars' => $typeCars,
            'rentPlace' => $rentPlace,
            'features' => collect($feature),
            'models' => $models,

        ]);
    }

    public function getCars () {
        $query = DB::table('selectionCars')
            ->join('cars', 'selectionCars.car_id', '=', 'cars.id')
            ->join('brands_cars', 'cars.brand_id', '=', 'brands_cars.id')
            ->leftjoin('car_types', 'cars.car_type', '=', 'car_types.id')
            ->leftjoin('cars_gears', 'cars.gear_id', '=', 'cars_gears.id')
            ->leftjoin('cars_images', 'cars.id', '=', 'cars_images.car_id')
            ->join('models_cars', 'cars.model_id', '=', 'models_cars.id')
            ->select('cars.id', 'cars_images.name as image', 'brands_cars.name as brand', 'models_cars.name as model', 'cars.year_of_issue', 'cars.seat', 'cars.doors', 'cars.cost_day', 'car_types.name as car_type');


        if ($id > 0) {
            $query = $query
                ->where('selectionCars.selection_id', $id);
        }
        if (isset($_GET['brandid'])) {
            $query = $query
                ->where('cars.brand_id', $_GET['brandid']);
        }
        if (isset($_GET['modelid'])) {
            $query = $query
                ->where('cars.model_id', $_GET['modelid']);
        }

        $selectionCars = $query
            ->where('cars.available', '1')
            ->where('cars.validate', '1')
            ->get();

        $brandsCars = DB::table('brands_cars')
            ->select('brands_cars.id', 'brands_cars.name')
            ->orderBy('name')
            ->get();

        return view('cars.list', [
            'selectionCars' => $selectionCars,
            'brandsCars' => $brandsCars,
            'param' => $_GET
        ]);
    }

    public function getCarCard (Request $request, $id) {

        $selectionCars = DB::table('cars')
            ->join('brands_cars', 'cars.brand_id', '=', 'brands_cars.id')
            ->leftjoin('car_types', 'cars.car_type', '=', 'car_types.id')
            ->leftjoin('cars_gears', 'cars.gear_id', '=', 'cars_gears.id')
            ->leftjoin('cars_feature', 'cars.feature_id', '=', 'cars_feature.id')
            ->leftjoin('models_cars', 'cars.model_id', '=', 'models_cars.id')
            ->leftjoin('date_rent_cars', 'cars.id', '=', 'date_rent_cars.car_id')
            ->leftjoin('rent_time_cars', 'cars.id', '=', 'rent_time_cars.car_id')
            ->leftjoin('cars_descriptions', 'cars.id', '=', 'cars_descriptions.cars_id')
            ->where('cars.id', $id)
            ->select('cars.id', 'cars.available', 'brands_cars.name as brand', 'models_cars.name as model', 'cars_descriptions.description as description', 'cars.year_of_issue',  'cars.location', 'cars.seat', 'cars.doors', 'cars.cost_day', 'car_types.name as car_type', 'cars_gears.name as gear', 'cars_feature.air', 'cars_feature.bluetooth', 'cars_feature.audio_input', 'cars_feature.usb_charger', 'cars_feature.child_seat', 'cars_feature.cruise_control', 'cars_feature.gps', 'date_rent_cars.from as from', 'date_rent_cars.to as to', 'rent_time_cars.min_rent_time as min_rent_time', 'rent_time_cars.max_rent_time as max_rent_time','cars.validate')
            ->get();

        if (count($selectionCars) and $selectionCars[0]->validate !== 1) {
            return abort(404);
        }

        $bookingDates = DB::table('booking_date')
            ->select('booking_date.datefrom as from', 'booking_date.dateto as to')
            ->where('booking_date.car_id', $id)
            ->where('datefrom', '!=', '2009-01-01 00:00:00')
            ->where('dateto', '!=', '2009-01-01 00:00:00')
            ->get();


        foreach ($bookingDates as $key => $value) {
            $value->from = date('d.m.Y h:m', strtotime($value->from));
            $value->to = date('d.m.Y h:m', strtotime($value->to));
        }

        $bookingDatesCalcul = DB::table('booking_date')
            ->select('booking_date.datefrom as from', 'booking_date.dateto as to')
            ->where('booking_date.car_id', $id)
            ->where('datefrom', '!=', '2009-01-01 00:00:00')
            ->where('dateto', '!=', '2009-01-01 00:00:00')
            ->get();

        $deliveryCarsPlaces = DB::table('delivery_cars')
            ->where('delivery_cars.car_id', $id)
            ->pluck('place');

        $rentPlace = [];
        $rentPlace = DB::table('rent_place')
            ->leftjoin('delivery_cars', function($join) use ($id) {
                $join->on('rent_place.id', 'delivery_cars.place')
                    ->where('delivery_cars.car_id', $id);
            })
            ->select('rent_place.id', 'rent_place.name', 'delivery_cars.cost')
            ->whereIn('rent_place.id', $deliveryCarsPlaces)
            ->get();

        $mileage = DB::table('mileage_inclusive_cars')
            ->select('mileage_inclusive_cars.day as day', 'mileage_inclusive_cars.week as week', 'mileage_inclusive_cars.month as month')
            ->where('mileage_inclusive_cars.car_id', $id)
            ->first();

        $extraItems = DB::table('extra_cars')
            ->leftjoin('extra', 'extra_cars.extra_id', '=', 'extra.id')
            ->leftjoin('extra_time', 'extra_cars.extra_time_id', '=', 'extra_time.id')
            ->where('extra_cars.cars_id', $id)
            ->select('extra.id',
                'extra.name as extra_name',
                'extra_time.name as extra_time',
                'extra_cars.extra_cost as extra_cost')
            ->get();

        $query = DB::table('feature_name')
            ->select('feature_name.en_name', 'feature_name.ru_name')
            ->get();

        /* TODO Такого быть не должно :) Нужно будет сделать нормальный запрос */
        $features = [];
        if ($selectionCars[0]->air == 1) $features['air'] = 1;
        if ($selectionCars[0]->bluetooth == 1) $features['bluetooth'] = 1;
        if ($selectionCars[0]->audio_input == 1) $features['audio_input'] = 1;
        if ($selectionCars[0]->usb_charger == 1) $features['usb_charger'] = 1;
        if ($selectionCars[0]->child_seat == 1) $features['child_seat'] = 1;
        if ($selectionCars[0]->cruise_control == 1) $features['cruise_control'] = 1;
        if ($selectionCars[0]->gps == 1) $features['gps'] = 1;

        $nameFeature = [];
        foreach($query as $key => $value) {
            if (isset($features[$value->en_name])) {
                $nameFeature[$value->en_name] = $value->ru_name;
            }
        }

        $images = DB::table('cars_images')
            ->select('cars_images.name as img_url')
            ->where('cars_images.car_id', $id)
            ->get();


        return view(
            'rent-auto.rent', [
            'selectionCars' => $selectionCars,
            'images'        => $images,
            'rentPlace'     => $rentPlace,
            'nameFeature'   => $nameFeature,
            'features'      => $features,
            'bookingDates'  => $bookingDates,
            'bookingDatesCalcul'  => $bookingDatesCalcul,
            'mileage'       => $mileage,
            'extraItems'    => $extraItems,
            'from'          => $request->from,
            'to'            => $request->to,
            'location'      => $request->location
        ]);
    }

    public function getModels(Request $request)
    {
        return response()->json(
            collect(
                BrandsCar::findOrFail($request->get('brandid'))->models
            )->map(function ($model) {
                return collect($model)->only([
                    'id',
                    'name',
                    'brand_id'
                ])->toArray();
            })
        );
    }

    public function getFilterRent()
    {
        $rent_place = RentPlace::all();
        return view('cars.filter-rent', ['rent_place' => $rent_place]);
    }

    public function filterRent(Request $request)
    {

    }

    public function getCheckout(Request $request)
    {
        if ($request->all() === []) {
            return \Redirect::to('/');
        }

        $car = json_decode($request->selectionCars[0]);

        $sum = $car[0]->cost_day * $request->number_days;

        if (isset($request->location_delivery)) { $sum = $sum += $request->location_delivery; }

        $info_order = [
            'from' => $request->from,
            'to' => $request->to,
            'sum' =>  $sum,
            'number_days' => $request->number_days,
            'delivery_cost' => $request->location_delivery,
            'included_distance' => $request->included_distance
        ];

        return view(
            'rent-auto.checkout', [
            'car' => $car,
            'info_order' => $info_order,
            'isValidate' => Auth::user()->validate
        ]);
    }

    public function booking(Request $request)
    {

        $car = json_decode($request->car[0], true);
        $info_order = json_decode($request->info_order[0], true);

        $booking_date = DB::table('booking_date')->insert([
            'user_id' => Auth::id(),
            'car_id' => $car['id'],
            'datefrom' => $info_order['from'],
            'dateto' => $info_order['to'],
            'status' => 3,
            'action' => 0
        ]);

        $payment_bookings = DB::table('payment_bookings')->insert([
            'amount_booking' => $info_order['sum'],
            'total_amount_booking' => $info_order['sum']
        ]);

        return \Redirect::to('/');
    }
}
