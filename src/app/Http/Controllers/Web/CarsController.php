<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Common\CarsController as Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;
use Illuminate\Http\Response;
use App\Services\NormalizeKeysToModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Models\{
    Car,
    MileageInclusive,
    CarsFeature,
    CarUnlockType,
    TimeAlertOwnRent,
    MileageInclusiveCars,
    RentTime,
    RentTimeCars,
    CarBans,
    CarsBans,
    PersonalUser,
    CarsColor,
    CarType,
    CarImage
};


/**
 * Контроллер работы с авто
 *
 * @property string $vin
 * @property integer $time_alert
 * @property integer $mileage_inclusive
 * @property float $min_overmile_fee
 * @property float $max_overmile_fee
 * @property boolean $need_kasko_and_osago

 */
class CarsController extends Controller
{

    /**
     * Получаем кастомные поля для заполнения перед сохранением авто
     *
     * @param  Request $request [description]
     * @return Response     [description]
     */
    public function getCreatePage(Request $request)
    {
        return view(
            'cars.create',
            [
                'data' => $this->getProperty()->map(function ($model) {
                    return collect($model)->map(function ($item) {
                        return collect($item)->only(['id', 'name'])->toArray();
                    });
                }),
                'car_type' => CarType::orderBy('name', 'asc')->get()
            ]
        );
    }

    /**
     * Создание авто пользователем
     *
     * @param  Request $request
     * @return JsonResponse
     */
    public function createCar(Request $request)
    {   
        $validate = Validator::make(
            $request->all(),
            self::getValidateRolesCar()
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        
        $car = Car::create($request->except('_token'));

        // Заполняем новыми данными которые получили
        $mileage_cars = MileageInclusiveCars::create([
            'car_id' => $car->id,
            'day' => 250,
            'week' => 1500,
            'month' => 3000
        ]);

        foreach (CarBans::all() as $item) {
            CarsBans::create([
                'cars_id' => $car->id,
                'bans_id' => $item->id,
                'active' => false
            ]);
        }

        $car->update([
            'mileage_inclusive' => $mileage_cars->id,
            'register_step' => 1,
            'user_id' => Auth::id()
        ]);


        

        
        return redirect()->route('car.show', $car->id)->with('status', true);
    }

    public function getPageOwnerCar(Request $request, $car_id)
    {
        $user = Auth::user();
        $car = Car::find($car_id);
        $feature = $car->feature ? $car->feature : CarsFeature::make();
        $unlock_type = CarUnlockType::all();
        $mileageInclusive = MileageInclusive::pluck('name', 'id');
        $timeAlertList = TimeAlertOwnRent::pluck('name', 'id');
        $rent_time = RentTime::all();

        




        $passport_face = $user->personalUser->passport_face;
        $passport_visa = $user->personalUser->passport_visa;


        $image = null;
        foreach (CarImage::all() as $key => $value) {
            if ($value['car_id'] == $car->id) {
                $image = true;
            } 
        }    
    
        $query = DB::table('feature_name')
            ->select('feature_name.en_name', 'feature_name.ru_name')
            ->get();

        $nameFeature = array();

        foreach($query as $key => $value) {
            $nameFeature[$value->en_name] = $value->ru_name;
        }

        
        return view(
            'cars.show',
            [
                'data'  =>   collect($car)
                                ->put('brand', $car->brand)
                                ->put('model', $car->model)
                                ->put('gear', $car->gear)
                                ->put('type', $car->cartype)
                                ->put('feature', $feature->only(
                                        'gps',
                                        'air',
                                        'bluetooth',
                                        'child_seat',
                                        'audio_input',
                                        'usb_charger',
                                        'cruise_control'
                                    )
                                ),
                'dataFeature' => $nameFeature,
                'unlock_type' => $unlock_type,
                'register_step' => $car->register_step,
                'personal_user' => $user->personalUser,
                'mileageInclusive' => $mileageInclusive,
                'timeAlertList' => $timeAlertList,
                'rent_time' => $rent_time,
                'colors' => CarsColor::all(),
                'user' => User::find(Auth::id()),
                'car' => $car,
                'step_1' => $car->year_of_issue || $car->seat || $car->mileage || $car->assessed_value || $car->color_id,
                'step_2' => $car->cost_day || $car->unlock_type_id,
                'step_3' => $car->rentTime,
                'step_4' => $user['personalUser']['passport_face'] || $user['personalUser']['passport_visa'] || $user['personalUser']['selfie'],
                // 'step_5' => $car->CarsData['sts_image_part_one'] || $car->CarsData['sts_image_part_two'],
                'step_5' => $car->vin,
                'step_6' => $car->CarsData['pts_image_part_one'] || $car->CarsData['pts_image_part_two'],
                'step_7' => $image,
                'step_8' => $car['osago'],
                'step_9' => $car['kasko'],
                'step_10' => $car['feature_id'],
                'step_11' => $user->confirmed
            ]
        );
    }

    public function updateCar(Request $request, $car_id)
    {
        $car = Auth::user()->cars()->findOrFail($car_id);

        $validate = Validator::make(
            $request->all(),
            self::getValidateRolesCarUpdate()
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }


        $car->update($request->all());

//         $result = collect();
//         try {
// //            $this->addCarFeature($request, $car->id);
//             $result->put('result', $car->save());
//             $result->put('carId', $car->id);
//         } catch (QueryException $e) {
//             $result->put('result', false);
//             $result->put('code', "1");
//         } finally {
//             return response()->json($result);
//         }
    }

    public function addCarFeature(Request $request, int $car_id)
    {
        $params = collect(
            $request->only(
                'air',
                'bluetooth',
                'audio_input',
                'usb_charger',
                'child_seat',
                'cruise_control',
                'gps'
            ))->map(
                function ($item) {
                    return (bool) $item;
                }
            );

        $validate = Validator::make(
            array_merge(
                ['CarId' => $car_id],
                $params->all()
            ),
            self::getValidateRolesCarFeature()
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        $car = Auth::user()->cars()->findOrFail($car_id);

        $normolize = NormalizeKeysToModel::class;
        $params = $params->mapWithKeys(function ($item, $key) use ($normolize) {
            return [$normolize::normalizeToCar($key) => $item];
        });

        $response = $this->saveCarFeature(
            $params,
            $car
        );


        return $response;



    }

    public function addCarUnlockTypeCost(Request $request, int $car_id)
    {
        $car = Auth::user()->cars()->findOrFail($car_id);
        $types = CarUnlockType::all();

        $car->update(['unlock_type_id' => (int)$request->type]);
        $car->update(['cost_day' => (int)$request->cost_day]);
        if($request->time_alert) {
            $car->update(['time_alert' => (int)$request->time_alert]);
        }


        return $car_id;
    }

    /**
     * Обновляем модель Cars
     * Поля для обновления указанны @see \App\Models\Car::getMileageInclusive
     *
     * @param Request $request
     * @param int     $car_id
     * @return \Illuminate\Support\Facades\Response     Обьект авто
     */
    public function addMileageInclusive(Request $request, int $car_id)
    {
        //Проверяем параметры
        $validate = Validator::make(
            $request->all(),
            Car::getMileageInclusive()
        );

        //Если есть ошибка выводим ошибку
        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        $car = Car::find($car_id);

        //Получаем данные указанные в фильтре
        $data = $request->validate(Car::getMileageInclusive());
        
        // Заполняем новыми данными которые получили
        $mileage_cars = MileageInclusiveCars::create($data);

        $car->update(['mileage_inclusive' => $mileage_cars->id]);

        return response()->json($mileage_cars);
    }


    public function addRentTime(Request $request, int $car_id)
    {
        //Проверяем параметры
        $validate = Validator::make(
            $request->all(),
            Car::getRentTimeCars()
        );

        //Если есть ошибка выводим ошибку
        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        $rent_time_cars = '';

        $min = $request->min_rent_time * 86400;
        $max = $request->max_rent_time * 86400;

        $rent_time_cars = RentTimeCars::create([
            'min_rent_time' => $min,
            'max_rent_time' => $max,
            'car_id' => $car_id
        ]);
        return $rent_time_cars;
    
    }

    public function vin(Request $request, int $car_id)
    {
        //Проверяем параметры
        $validate = Validator::make(
            $request->all(),
            [
                'vin' => 'string'
            ]
        );

        //Если есть ошибка выводим ошибку
        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        
        $car = Auth::user()->cars()->findOrFail($car_id);
        $car->update([
            'vin' => $request->vin
        ]);
    }



    public function osago(Request $request, int $car_id)
    {
        $car = Auth::user()->cars()->findOrFail($car_id);
        if (isset($request->osago_has)) {
            $car->update([
                'osago' => $request->osago_has,
            ]);
        } else {
            $car->update([
                'need_osago' => $request->need_osago,
            ]);
        }
    }

    public function kasko(Request $request, int $car_id)
    {
        $car = Auth::user()->cars()->findOrFail($car_id);
        if (isset($request->kasko_has)) {
            $car->update([
                'kasko' => $request->kasko_has,
            ]);
        } else {
            $car->update([
                'kasko' => $request->need_kasko,
            ]);
        }
    }

    public function passportData(Request $request, $car_id)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'passport_seria' => 'required|string',
                'passport_nomer' => 'required|string',
                'passport_issued_by' => 'required|string',
                'passport_issued' => 'required|string',
            ]
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }
        
        $user = User::find(Auth::id());
        $personal_user = PersonalUser::find($user->personalUser['id']);

        if (!$personal_user->passport_face || !$personal_user->passport_visa || !$personal_user->selfie) {
            return response()->json([
                'message' => 'Добавление фотографий паспорта обязательно для верификации, иначе мы не сможем добавить вас на платформу'
            ]);
        }

        $personal_user->update($request->except('_token'));
    }

    public function redirect(Request $request, $car_id)
    {

        \Log::info('redirect');
        \Log::info($request);

        $validate = Validator::make(
            $request->all(),
            [
                'policy' => 'required',
            ]
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        Car::find($car_id)->update(['register_step' => 2]);

        Car::find($car_id)->update([
            'oferta' => $request->policy
        ]);

        return route('profile.car', $car_id);
    }

    protected static function getValidateRolesCarUpdate()
    {
        return [
            'year_of_issue' => 'nullable|integer',
            'seat' => 'nullable|integer',
            'doors' => 'nullable|integer',
            'cost' => 'nullable|integer',
            'car_type' => 'nullable|integer',
            'cost_day' => 'nullable|integer'
        ];
    }

    /**
     * Валидация данных изменения дополнительных полей авто
     *
     * @return array Массив допустимы параметров с ролями
     */
    protected static function getValidateRolesCar(): array
    {
        return [
            'model_id' => 'required|integer',
            'brand_id' => 'required|integer',
            'gearid' => 'nullable|integer',
            'year_of_issue' => 'nullable|integer',
            'seat' => 'nullable|integer',
            'doors' => 'nullable|integer',
            'car_type' => 'required|integer|exists:car_types,id'
        ];
    }
}
