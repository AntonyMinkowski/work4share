<?php

namespace App\Http\Controllers\Web\UserProfile;

use App\Http\Controllers\Controller;
use App\Mail\NotificationsEmail;
use App\Models\{BookingDate,
    BookingFiel,
    BookingMileage,
    BookingPhoto,
    BookingRefusal,
    BookingStatus,
    Car,
    CarsFeature,
    DateRentCars,
    DeliveryCars,
    PaymentBooking};
use App\Models\CarImage;
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Library\Connectors\navixy;
use Mail;
use Validator;

// use Illuminate\Support\Facades\Auth;

class GetMyCarsController extends Controller
{
    public function getMyCars () {
        $myCars = DB::table('cars')
            ->where('cars.user_id', Auth::id())
            ->join('brands_cars', 'cars.brand_id', '=', 'brands_cars.id')
            ->leftjoin('car_types', 'cars.car_type', '=', 'car_types.id')
            ->leftjoin('cars_gears', 'cars.gear_id', '=', 'cars_gears.id')
            ->join('models_cars', 'cars.model_id', '=', 'models_cars.id')
            ->select('cars.id', 'cars.user_id', 'cars.available', 'cars.validate', 'brands_cars.name as brand', 'models_cars.name as model', 'cars.year_of_issue', 'cars.seat', 'cars.doors', 'cars.cost_day', 'car_types.name as car_type', 'cars_gears.name as gear')
            ->get();


        $query = DB::table('cars_images')
            ->select('name', 'car_id' )
            ->where('avatar', 1)
            ->get();

        $arrayImage = array();

        foreach($query as $key => $value) {
            $arrayImage[$value->car_id]['name'] = $value->name;
        }

        return view('user.profile.my-cars', [
            'myCars' => $myCars, 
            'arrayImage' => $arrayImage,
        ]);
    }

    public function getMyCar ($id) {
        $query = DB::table('cars')
            ->join('brands_cars', 'cars.brand_id', '=', 'brands_cars.id')
            ->leftjoin('car_types', 'cars.car_type', '=', 'car_types.id')
            ->leftjoin('cars_gears', 'cars.gear_id', '=', 'cars_gears.id')
            ->join('models_cars', 'cars.model_id', '=', 'models_cars.id')
            ->where('cars.id', $id)
            ->select('cars.id', 'brands_cars.name as brand', 'cars.validate', 'models_cars.name as model', 'cars.year_of_issue', 'cars.available', 'cars.seat', 'cars.doors', 'cars.cost_day', 'car_types.name as car_type', 'cars_gears.name as gear', 'cars.oferta');

            $myCarData = $query
                ->get();

        $query = DB::table('cars_tracker')
            ->where('cars_tracker.car_id', $id)
            ->select( 'cars_tracker.install_tracker' )
            ->get();
    
        if (count($query) > 0) {
            $install_tracker = $query[0]->install_tracker;
        } else {
            $install_tracker = 0;
        }

        $query = DB::table('cars_images')
            ->where('cars_images.car_id', $id)
            ->select( 'cars_images.name', 'cars_images.id', 'cars_images.avatar' );

        $myCarsImages = $query
            ->get();

        $query = DB::table('feature_name')
            ->select('feature_name.en_name', 'feature_name.ru_name')
            ->get();

        $nameFeature = array();

        $car = Auth::user()->cars()->findOrFail($id);
        $feature = $car->feature ? $car->feature : CarsFeature::make();
        
        foreach($query as $key => $value) {
            $nameFeature[$value->en_name] = $value->ru_name;
        }

        $date_rent_cars = DateRentCars::where('car_id', $id)->get();
        
        $bookingDate = BookingDate::with('cars')
            ->join('users', 'booking_date.user_id', '=', 'users.id')
            ->where('booking_date.car_id', $id)
            ->where('booking_date.status', '!=', 21)
            ->where('booking_date.status', '!=', 22)
            ->select('booking_date.*', 'users.name as user_name','action');
        $bookingDatesCurrent = (clone $bookingDate)->where('action', 1)->get();
        $bookingDatesNotPaid = (clone $bookingDate)->where('action', 0)->get();
        $bookingDatesHistory = (clone $bookingDate)->where('action', 2)->get();
        if ($bookingDate !== []) {
            $i = 0;
            foreach ($bookingDate as $item) {
                $image_car = DB::table('cars_images')
                    ->select('cars_images.name as img_url')
                    ->where('cars_images.car_id', $item->car_id)
                    ->first();

                $item->img_url =  $image_car->img_url;
            $i++;
            }
        }

        return view('user.profile.my-car-card', [
            'myCarData' => $myCarData,
            'myCarsImages' => $myCarsImages,
            'data'  =>   collect($car)
            ->put('brand', $car->brand)
            ->put('model', $car->model)
            ->put('gear', $car->gear)
            ->put('register_step', $car->register_step)
            ->put('location', $car->location)
            ->put('location_description', $car->location_description)
            ->put('type', $car->cartype)
            ->put('unlock', $car->unlock_type_id)
            ->put('feature', $feature->only(
                    'gps',
                    'air',
                    'bluetooth',
                    'child_seat',
                    'audio_input',
                    'usb_charger',
                    'cruise_control'
                )
            ),
            'dataFeature' => $nameFeature,
            'bookingDate' => $bookingDate,
            'bookingDatesCurrent' => $bookingDatesCurrent,
            'bookingDatesHistory' => $bookingDatesHistory,
            'bookingDatesNotPaid' => $bookingDatesNotPaid,
            'install_tracker' => $install_tracker,
            'date_rent_cars' => $date_rent_cars
        ]);
    }

    public function bookingCard($booking_id, Request $request) {


        $booked = BookingDate::where('id', $booking_id)->with('cars')->first();

        if (isset($booked->cars->user_id) and $booked->cars->user_id === Auth::id() and $booked->status != 21) {
            $bookedPhotoTenant = BookingPhoto::where('booking_id', $booking_id)->where('state', 2)->get();
            $statusName = BookingStatus::where('id', $booked->status)->pluck('name')->first();
            $paymentBooking = PaymentBooking::where('booking_id', $booked->id)->first();

            $booking_photoLandlord = BookingPhoto::where('booking_id', $booked->id)->where('state', 1)->get();
            $booking_fiel = BookingFiel::where('booking_id', $booked->id)->first();
            $booking_mileage = BookingMileage::where('booking_id', $booked->id)->first();

            $booked->booking_photoLandlord =  $booking_photoLandlord;
            $booked->booking_fiel =  $booking_fiel;
            $booked->booking_mileage =  $booking_mileage;

            /* @var $car Car */
            $car = $booked->cars;
            $images = DB::table('cars_images')
                ->select('cars_images.name as img_url')
                ->where('cars_images.car_id', $car->id)
                ->get();

            $toDate = Carbon::createFromFormat('Y-m-d H:i:s', $booked->dateto);
            $fromDate = Carbon::createFromFormat('Y-m-d  H:i:s', $booked->datefrom);

            $numberOfDays = $booked->count_day ?? ($toDate->diff($fromDate)->days + 1);


            //Сумма за день умноженая на количество забронированых дней
            $rentPlace = [];
            if ($booked->delivery_place) {
                $location_delivery = $booked->delivery_place;
                $rentPlace = DB::table('rent_place')
                    ->select('rent_place.id', 'rent_place.name')
                    ->where('rent_place.id', $booked->delivery_place)
                    ->first();
            }

            //Получаем цену
            $deliveryCost = DeliveryCars::where('car_id', $car->id)->where('place', $booked->delivery_place)->pluck('cost');
            if (isset($deliveryCost[0])) {
                $deliveryCost = $deliveryCost[0];
            } else {
                $deliveryCost = 0;
            }
            $calculatedDeposit = $booked->deposit ?? $car->getCalculatedDeposit();


            $info_order = [
                'sum' => $paymentBooking['total_amount_booking'],
                'number_days' => $numberOfDays,
                'delivery_cost' => $deliveryCost ?? 0,
                'deposit' => $calculatedDeposit,
                'included_distance' => $booked->mileage,
            ];



            return view('user.profile.my-car-booking', [
                'booked' => $booked,
                'bookedPhotoTenant' => $bookedPhotoTenant,
                'car' => $car,
                'statusName' => $statusName,
                'images' => $images,
                'info_order' => $info_order,
                'rentPlace' => $rentPlace,
            ]);
        } else {
            abort(404);
        }

    }

    public function rescission($booking_id, Request $request) {

        if ($booking_id && $request->rescission_description) {

            $booking_refusal = new BookingRefusal;
            $booking_refusal->booking_id = $booking_id;
            $booking_refusal->own = 1;
            $booking_refusal->description = $request->rescission_description;
            $booking_refusal->save();

            $booking_date = BookingDate::where('id', $booking_id)->first();
            $booking_date->datefrom = '2009-01-01 00:00';
            $booking_date->dateto = '2009-01-01 00:00';
            $booking_date->status = 23;
            $booking_date->save();

            $user_car = User::find($booking_date->user_id);
            
            if ($user_car && $user_car->email_notif === 1) {
    
                $subject = 'Ваша бронь отменена владельцем';
                $textMessage = '<p style="outline: none!important;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;font-size: 14px;color: #000;text-align: center;width: 275px;line-height: 1.5;margin: 32px auto 0;padding-bottom: 36px;color: #000"> 
                К сожалению, владелец отказал в бронировании, но мы сделаем все, чтобы предоставить Вам автомобиль
                </p>';
                $buttonText = 'Перейти в каталог';
                $url = \Config::get('app.url') . '/catalog';
    
                Mail::to($user_car->email)
                    ->send(new NotificationsEmail($subject, $textMessage, $url, $buttonText));
            }
    
            if ($user_car && $user_car->sms_notif === 1) {
                $login = 'marussia_f1_team';
                $pass = 'SOAUTO_SMS';
                $endpoint = 'https://smsc.ru/sys/send.php';
                $client = new \GuzzleHttp\Client();
    
                $response = $client->request('GET', $endpoint, ['query' => [
                    'login' => $login, 
                    'psw' => $pass,
                    'phones' => $user_car->phone,
                    'mes' => 'К сожалению, владелец отказал в бронировании, но мы сделаем все, чтобы предоставить Вам автомобиль.'
                ]]);
            }            
            
            return back();
        }

        return response()->json(500);
    }    

    public function confirm($booking_id)
    {
        if ($booking_id) {

            $booking_date = BookingDate::where('id', $booking_id)->first();

            $user_id = Car::where('id', $booking_date->car_id)->pluck('user_id')->first();
            
            if ($user_id === Auth::id()) {

                $booking_date->status = 7;
                $booking_date->action = 1;
                $booking_date->save();

                if ($booking_date !== []) {
                    DB::table('chat_room')->insert(
                        ['car_id' => $booking_date->car_id, 'user_id' => $booking_date->user_id]
                    );
                }

                $user_car = User::find($booking_date->user_id);

                if ($user_car && $user_car->email_notif === 1) {
        
                    $subject = 'Ваша бронь подтверждена владельцем';
                    $textMessage = '<p style="outline: none!important;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;font-size: 14px;color: #000;text-align: center;width: 275px;line-height: 1.5;margin: 32px auto 0;padding-bottom: 36px;color: #000"> 
                    Твоя бронь подтверждена. Теперь у тебя есть возможность пообщаться с владельцем в чате и обсудить детали встречи.
                    </p>';
                    $buttonText = 'Перейти в мои поездки';
                    $url = \Config::get('app.url') . '/profile/trips/card/' . $booking_date->car_id;
        
                    Mail::to($user_car->email)
                        ->send(new NotificationsEmail($subject, $textMessage, $url, $buttonText));
                }
        
                if ($user_car && $user_car->sms_notif === 1) {
                    $login = 'marussia_f1_team';
                    $pass = 'SOAUTO_SMS';
                    $endpoint = 'https://smsc.ru/sys/send.php';
                    $client = new \GuzzleHttp\Client();
        
                    $response = $client->request('GET', $endpoint, ['query' => [
                        'login' => $login, 
                        'psw' => $pass,
                        'phones' => $user_car->phone,
                        'mes' => 'Твоя бронь подтверждена. Теперь у тебя есть возможность пообщаться с владельцем в чате и обсудить детали встречи.'
                    ]]);
                }
            } else {
                abort(404);
            }
        }

        return back();
    }    

    public function confirmPayment($booking_id)
    {
        if ($booking_id) {

            $booking_date = BookingDate::where('id', $booking_id)->first();

            $user_id = Car::where('id', $booking_date->car_id)->pluck('user_id')->first();

            if ($user_id === Auth::id()) {

                $booking_date->status = 9;
                $booking_date->save();
                
            } else {
                abort(404);
            }
        }

        return back();
    }    

    public function addCarInterior($booking_id, Request $request)
    {
        if ($request->hasFile('myCarCard_car_interior')) {

            $validate = Validator::make(
                $request->all(),
                [
                    'myCarCard_car_interior' => 'required|file|mimes:jpeg,bmp,png',
                ]
            );
    
            if ($validate->fails()) {
                return response()->json($validate->errors(), 500);
            }

            $hash = $this->createHashBooking($booking_id, Auth::id());

            Storage::disk('hot')->put(
                $hash,
                $request->file('myCarCard_car_interior')->get(),
                array('ACL'=>'public-read')
            );
            if (Storage::disk('hot')->exists($hash)) {
                $booking_photo = new BookingPhoto;
                $booking_photo->booking_id = $booking_id;
                $booking_photo->name = $hash;
                $booking_photo->state = 2;
                $booking_photo->save();

                $this->bookingDateUsrGaveKay($booking_id);
            }
        } else {
            return response()->json('myCarCard_car_interior not found', 500);
        }
    }

    public function addCarFuelResidue($booking_id, Request $request)
    {
        if ($request->hasFile('myCarCard_fuel_residue')) {

            $validate = Validator::make(
                $request->all(),
                [
                    'myCarCard_fuel_residue' => 'required|file|mimes:jpeg,bmp,png',
                ]
            );
    
            if ($validate->fails()) {
                return response()->json($validate->errors(), 500);
            }

            $hash = $this->createHashBooking($booking_id, Auth::id());

            $booking_fiel = BookingFiel::where('booking_id', $booking_id)->first();
            if ($booking_fiel !== [] && isset($booking_fiel->booking_id)) {
                Storage::disk('hot')->put(
                    $hash,
                    $request->file('myCarCard_fuel_residue')->get(),
                    array('ACL'=>'public-read')
                );
                if (Storage::disk('hot')->exists($hash)) {
                    $booking_fiel->fiel_photo_after = $hash;
                    $booking_fiel->save();

                    $this->bookingDateUsrGaveKay($booking_id);
                }
            } else {
                Storage::disk('hot')->put(
                    $hash,
                    $request->file('myCarCard_fuel_residue')->get(),
                    array('ACL'=>'public-read')
                );
                if (Storage::disk('hot')->exists($hash)) {
                    $booking_fiel = new BookingFiel;
                    $booking_fiel->booking_id = $booking_id;
                    $booking_fiel->fiel_photo_after = $hash;
                    $booking_fiel->save();

                    $this->bookingDateUsrGaveKay($booking_id);
                }
            }
        } else {
            return response()->json('myCarCard_fuel_residue not found', 500);
        }
    }

    public function addCarMileagePhoto($booking_id, Request $request)
    {
        if ($request->hasFile('myCarCard_run')) {

            $validate = Validator::make(
                $request->all(),
                [
                    'myCarCard_run' => 'required|file|mimes:jpeg,bmp,png',
                ]
            );
    
            if ($validate->fails()) {
                return response()->json($validate->errors(), 500);
            }

            $hash = $this->createHashBooking($booking_id, Auth::id());
            
            $booking_mileage = BookingMileage::where('booking_id', $booking_id)->first();
            if ($booking_mileage !== [] && isset($booking_mileage->booking_id)) {
                Storage::disk('hot')->put(
                    $hash,
                    $request->file('myCarCard_run')->get(),
                    array('ACL'=>'public-read')
                );
                if (Storage::disk('hot')->exists($hash)) {
                    $booking_mileage->mileage_photo_after = $hash;
                    $booking_mileage->save();

                    $this->bookingDateUsrGaveKay($booking_id);
                }
            } else {
                Storage::disk('hot')->put(
                    $hash,
                    $request->file('myCarCard_run')->get(),
                    array('ACL'=>'public-read')
                );
                if (Storage::disk('hot')->exists($hash)) {
                    $booking_mileage = new BookingMileage;
                    $booking_mileage->booking_id = $booking_id;
                    $booking_mileage->mileage_photo_after = $hash;
                    $booking_mileage->save();

                    $this->bookingDateUsrGaveKay($booking_id);
                }
            }
        } else {
            return response()->json('myCarCard_run not found', 500);
        }
    }

    public function addCarMileageInput($booking_id, Request $request)
    {
        if ($request && $booking_id) {
            $booking_mileage = BookingMileage::where('booking_id', $booking_id)->first();
            $booking_mileage->mileage_after = $request->mileage;
            $booking_mileage->save();

            $this->bookingDateUsrGaveKay($booking_id);
        }

        return \Redirect::back()->with('response', 'Ваш пробег засчитан');
    }

    public function bookingDateUsrGaveKay($booking_id) {

        $booking_photo = BookingPhoto::where('booking_id', $booking_id)->where('state', 2)->first();
        $booking_fiel = BookingFiel::where('booking_id', $booking_id)->first();
        $booking_mileage = BookingMileage::where('booking_id', $booking_id)->first();

        if (isset($booking_photo->name) && isset($booking_fiel->fiel_photo_after) && isset($booking_mileage->mileage_photo_after) && isset($booking_mileage->mileage_after)) {
            $booking_date = BookingDate::where('id', $booking_id)->first();
            $booking_date->usrGaveKayAfterRent = 1;
            $booking_date->save();
        }
        return true;
    }

    /**
     * Записываем новое главное фото автомобиля
     *
     * @param  CarImage    Модель фотографий
     * @param  Images      $images - список всех фотографий
     * @param  Request     $request - запрос
     * @return             результат сохранения
     */

    public function getMyCarLocation ($id) {
        $tracker_id = DB::table('cars_tracker')
            ->where('car_id', $id)
            ->get();

        $navixy = new navixy;

        $data = $navixy->getLocationCar($tracker_id[0]->tracker_id);

        return response()
            ->json($data);
    }

    public function getTracking($id) {
        $tracker_id = DB::table('cars_tracker')
            ->where('car_id', $id)
            ->get();

        $navixy = new navixy;

        $data = $navixy->getTracking($tracker_id[0]->tracker_id, $_GET['from'], $_GET['to']);

        return response()
            ->json($data);
    }

    public function toRentCar (Request $request) {
        // Получаем данные и сразу проверяем
        $postData = $request->validate([
            'car_id' => 'required|integer',
            'to' => 'required|date_format:"d.m.Y H:i"',
            'from' => 'required|date_format:"d.m.Y H:i"',
        ]);

        // Задаем нужный формат для даты
        $postData['to'] = date('Y-m-d H:i:s', strtotime($postData['to']));
        $postData['from'] = date('Y-m-d H:i:s', strtotime($postData['from']));

        $date_rent_cars = DateRentCars::create([
            'car_id' => $request->car_id,
            'active' => $request->active,
            'to' => $postData['to'],
            'from' => $postData['from'],
        ]);


        // $existCar = DB::table('date_rent_cars')
        //     ->where('car_id', $postData['car_id'])
        //     ->get();

        // if (count($existCar) > 0) {
        //     DB::table('date_rent_cars')
        //     ->where('car_id', $postData['car_id'])
        //     ->update(['from' => $postData['from'], 'to' => $postData['to']]);
        // } else {
        //     DB::table('date_rent_cars')->insert(
        //         [
        //             'car_id' => $postData['car_id'],
        //             'from' => $postData['from'],
        //             'to' => $postData['to']
        //         ]
        //     );
        // }

        $car = Auth::user()->cars()->findOrFail($postData['car_id']);
        $car->available = 1;
        $car->save();


       

        return response()->json([
            'success' => 'moveCarToPool',
            'message' => 'Ваш автомобиль передан в пул'
        ]);
    }
    public function editRentCar(Request $request, $car_id)
    {
        DateRentCars::find($request->id)->update([
            'active' => !$request->active
        ]);

        if ($request->active) {
           return response()->json([
               'message' => 'Ваш автомобиль снят с аренды'
           ]);
        } else{
            return response()->json([
                'message' => 'Ваш автомобиль сдан в аренду'
            ]);
        }
    }

    public function deleteRentCar(Request $request, $car_id)
    {
        DateRentCars::find($request->id)->delete();

        return response()->json([
            'message' => 'Вы удалил эту запись'
        ]);
    }

    public function fromRentCar (Request $request) {
        $car = Auth::user()->cars()->findOrFail($request->car_id);
        $car->update([
            'available' => 0
        ]);
        return response()->json([
            'message' => 'Ваше авто снято с аренды'
        ]);
    }

    public function createHashBooking ($booking_id, $user_id)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';

        for ($i = 0; $i < 20; $i++) {
            $randstring .= $characters[rand(0, strlen($characters) - 1)];
        }

        return ($booking_id + $user_id) . $randstring;
    }
}
