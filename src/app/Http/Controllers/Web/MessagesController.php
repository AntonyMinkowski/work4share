<?php


namespace App\Http\Controllers\Web;


/**
 * Мои сообщения
 *
 * Class MessagesController
 * @package App\Http\Controllers\Web
 */
class MessagesController
{
    public function list() {
        return view('messages.list', []);
    }
}