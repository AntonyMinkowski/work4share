<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Mail\NotificationsEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function send(Request $request)
    {
        Mail::to($request->emailTo)
                ->subject($request->subject)
                ->send(new NotificationsEmail($request->textMessage, $request->url, $request->buttonText));
    }
}