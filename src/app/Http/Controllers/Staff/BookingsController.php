<?php

namespace App\Http\Controllers\Staff;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\BookingDate;

class BookingsController extends Controller
{
    public function start() {

        $route = DB::table('staff_module')
            ->select('staff_module.route', 'staff_module.name')
            ->join('staff_access', 'staff_access.module_id', '=', 'staff_module.id')
            ->where('staff_access.user_id', Auth::id())
            ->get();
       
        $bookings = BookingDate::
            leftjoin('cars', 'booking_date.car_id', '=', 'cars.id')
            ->leftjoin('brands_cars', 'cars.brand_id', '=', 'brands_cars.id')
            ->leftjoin('models_cars', 'cars.model_id', '=', 'models_cars.id')
            ->leftjoin('users as tenant', 'booking_date.user_id', '=', 'tenant.id')
            ->leftjoin('users as owner', 'cars.user_id', '=', 'owner.id')
            ->leftjoin('booking_status', 'booking_date.status', '=', 'booking_status.id')
            ->select('booking_date.id', 'booking_date.datefrom','booking_date.dateto','booking_date.status', // booking
                     'cars.id as car_id','brands_cars.name as brand_name','models_cars.name as model_name', // car
                     'tenant.name as tenant_name','tenant.phone as tenant_phone','tenant.email as tenant_email',  // tenant
                     'owner.name as owner_name','owner.phone as owner_phone','owner.email as owner_email', // owner
                     'booking_status.name as status_name') // status_name
            ->get();

        return view('staff.bookings', ['route' => $route, 'bookings' => $bookings]);
    }
}
