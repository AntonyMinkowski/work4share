<?php

namespace App\Http\Controllers\Staff;

use App\DepositPrice;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class DepositAmountsController extends Controller
{
    public function index()
    {
        $depositPrices = DepositPrice::orderBy('car_price')->get();
        $route = CheckAutoController::getRoutes();
        return view('staff.deposit-prices', compact('depositPrices', 'route'));
    }

    public function updateDepositPrice(Request $request)
    {
        $depositPrice = null;
        if (Auth::user()->hasAccessToEdit(7)) {
            if ($request->deposit_price_id) {
                $depositPrice = DepositPrice::find($request->deposit_price_id);
                if ($depositPrice) {
                    $depositPrice->update([
                        'deposit_price' => $request->deposit_price,
                        'car_price' => $request->car_price
                    ]);
                }

            } else {
                $depositPrice = new DepositPrice([
                    'deposit_price' => $request->deposit_price,
                    'car_price' => $request->car_price
                ]);
                $depositPrice->save();
            }
        }
        return redirect()->back();

    }

    public function deleteDepositPrice($id)
    {
        if (Auth::user()->hasAccessToEdit(7)) {
            DepositPrice::find($id)->delete();
        }
        return redirect()->back();
    }
}
