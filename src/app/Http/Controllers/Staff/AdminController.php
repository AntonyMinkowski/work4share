<?php

namespace App\Http\Controllers\Staff;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function start() {

        $route = DB::table('staff_module')
            ->select('staff_module.route', 'staff_module.name')
            ->join('staff_access', 'staff_access.module_id', '=', 'staff_module.id')
            ->where('staff_access.user_id', Auth::id())
            ->get();

        $users = DB::table('users')
            ->select('users.id', 'users.NAME', 'users.email', 'users.email_verified_at', 'users.validate')
            ->get();

        return view('staff.admin', ['route' => $route, 'users' => $users]);
    }
}
