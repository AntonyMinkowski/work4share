<?php

namespace App\Http\Controllers\Api;

use App\Models\{Car, BrandsCar, ModelsCar, CarsGear, CarType, CarUnlockType, CarsFeature};
// use App\Http\Controllers\Controller;
use App\Http\Controllers\Common\ImageController as Controller;
use App\Services\NormalizeKeysToModel;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Validator;
use Auth;

class addCarController extends Controller
{
    public function index()
    {
        $collect = collect([
            'brand' => BrandsCar::select('id','name')->get(),
            'gear' => CarsGear::select('id','name')->get(),
            'car_type' => CarType::select('id','name')->get()
        ]);
        return response()->json($collect);
    }

    /**
     * При выборе бренда, загружаем список моделей бренда
     * @param Request $request
     * @return Response     Массив моделей для бренда
     */
    public function getModel(Request $request)
    {
        $model = ModelsCar::where('brand_id', $request->brand_id)->select('id','name', 'brand_id')->get();
        return response()->json($model);
    }

    /**
     * Добавлям авто в базу данных(Шаг 1)
     * @param Request $request
     * @return Response     Обьек авто
     */
    public function addCar(Request $request)
    {
        //Проверяем параметры
        $validate = Validator::make(
            $request->all(),
            Car::getValidateRolesCar()
        );
        //Если есть ошибка выводим ошибку
        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        //Создаем авто с пришедими к нам параметрами
        $car = Car::create($request->toArray());
        //Указываем шаг создания авто и присваиваем авто к пользователю
        $car->update([
            'register_step' => 1,
            'user_id' => Auth::user()->id
        ]);

        return response()->json($car);
    }
     /**
     * Добавляем новые поля к обьекту авто(Год, Кол-во пассажиров, Кол-во дверей) в авто(Шаг 2)
     * @return Response     Обьект авто
     */
    public function addCarDetails(Request $request, int $car_id)
    {
        //Проверяем параметры
        $validate = Validator::make(
            $request->all(),
            Car::getValidateRolesCarDetails()
        );
        //Если есть ошибка выводим ошибку
        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }
        //Находим обьект машины в базе
        $car = Car::find($car_id);
        //Заполняем новыми данными которые получили
        $car->fill($request->toArray());
        return response()->json($car);
    }

     /**
     * Добавляем новые к обьекту авто (Стоимость в день, Тип подключения)
     * @return Response     Обьект авто
     */
    public function addCarUnlockTypeCost(Request $request, int $car_id)
    {
        $car = Car::find($car_id);
        $types = CarUnlockType::all();

        $car->update([
            'unlock_type_id' => (int)$request->unlock_type_id,
            'cost_day' => (int)$request->cost_day

        ]);

        return response()->json($car);
    }


    public function addPhotoPassportFace(Request $request, int $car_id)
    {
        $car = Car::find($car_id);
        

        $validate = Validator::make(
            array_merge(
                ['car_id' => $car_id],
                $request->all()
            ),
            Car::getValidateRolesCarImage()
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        return response()->json($this->savePassport($car, $request, 'passport_face'));
    }

    /**
     * Добавляем фото паспорта страница прописки
     *
     * @param  Request $request запрос
     * @return JsonResponse     сформированный ответ
     */
    public function addPhotoPassportRegistry(Request $request, int $car_id)
    {
        $car = Car::find($car_id);

        $validate = Validator::make(
            array_merge(
                ['car_id' => $car_id],
                $request->all()
            ),
            Car::getValidateRolesCarImage()
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        return response()->json($this->savePassport($car, $request, 'passport_visa'));
    }
    /**
     * Добавляем фотографии документов
     * Нужно передать параметр id с названием типа загружаемого документа {
     * pts_image_part_one,
     * pts_image_part_two,
     * sts_image_part_one,
     * sts_image_part_one
     * }
     */
    public function addPhotoDocsCar(Request $request, int $car_id)
    {
        $car = Car::find($car_id);
        $type = $request->id;

        $validate = Validator::make(
            array_merge(
                ['car_id' => $car_id],
                $request->all()
            ),
            Car::getValidateRolesCarImage()
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        return response()->json($this->savePtsImage($car, $request, $type));
    }
    /**
     * Осаго фото
     */
    public function osagoImage(Request $request, int $car_id)
    {
        $car = Auth::user()->cars()->findOrFail($car_id);

        $validate = Validator::make(
            array_merge(
                ['car_id' => $car_id],
                $request->all()
            ),
            Car::getValidateRolesCarImage()
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        return response()->json($this->saveOsagoKaskoImage($car, $request, 'osago_image'));
    }
    /**
     * Каско фото
     */
    public function kaskoImage(Request $request, int $car_id)
    {
        $car = Auth::user()->cars()->findOrFail($car_id);

        $validate = Validator::make(
            array_merge(
                ['car_id' => $car_id],
                $request->all()
            ),
            Car::getValidateRolesCarImage()
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        return response()->json($this->saveOsagoKaskoImage($car, $request, 'kasko_image'));
    }

    public function osago(Request $request, int $car_id)
    {
        $car = Car::find($car_id);
        if (isset($request->osago_has)) {
            $car->update([
                'osago' => $request->osago_has,
            ]);
        } else {
            $car->update([
                'need_osago' => $request->need_osago,
            ]);
        }
        return response()->json($car);
    }

    public function kasko(Request $request, int $car_id)
    {
        $car = Car::find($car_id);
        if (isset($request->kasko_has)) {
            $car->update([
                'kasko' => $request->kasko_has,
            ]);
        } else {
            $car->update([
                'kasko' => $request->need_kasko,
            ]);
        }
        return response()->json($car);
    }

    /**
     * Добавляем фото авто
     *
     * @param  Request $request запрос
     * @return JsonResponse     сформированный ответ
     */
    public function addPhotoCar(Request $request, int $car_id)
    {
        $car = Auth::user()->cars()->findOrFail($car_id);
        $validate = Validator::make(
            array_merge(
                ['car_id' => $car_id],
                $request->all()
            ),
            Car::getValidateRolesCarImage()
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        return response()->json($this->saveCarImage($car, $request));
    }
    public function addFeatured(Request $request, int $car_id)
    {
        $params = collect(
            $request->only(
                'air',
                'bluetooth',
                'audio_input',
                'usb_charger',
                'child_seat',
                'cruise_control',
                'gps'
            ))->map(
                function ($item) {
                    return (bool) $item;
                }
            );


        $validate = Validator::make(
            array_merge(
                ['car_id' => $car_id],
                $params->all()
            ),
            Car::getValidateRolesCarFeature()
        );

        if ($validate->fails()) {
            return response()->json($validate->errors(), 500);
        }

        $car = Car::find($car_id);
        $car->update(['register_step' => 2]);

        if ($car->feature_id == null) {
            $feature = CarsFeature::create($params->toArray());
            $car->feature_id = $feature->id;
        } else {
            $feature = CarsFeature::where('id', $car->feature_id)->update($params);
        }


        return response()->json($feature);


    }
}
