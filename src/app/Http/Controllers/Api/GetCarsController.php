<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\SearchRequest;
use App\Resources\Feature;
use App\Services\SearchCarsService;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\{Car, ModelsCar, BrandsCar, CarsGear, CarsFeature, CarType};

class GetCarsController extends Controller
{

    public function getBrands()
    {
        $brands_cars = DB::select('select `id`, `name` from brands_cars');
        return response()->json($brands_cars);
    }
    
    public function getSelection () {
        $selectionforcars = DB::table('selectionforcars')->select('id', 'name', 'image_url')->get();
        return response()->json($selectionforcars);
    }


    public function getSelectionCars(SearchRequest $request) {
        $params = collect($request->validated());

        $feature = (new Feature(new CarsFeature))->toArray($request);
        $params->put('feature', collect($feature)->filter());
        /** @var SearchCarsService $search */
        $search = app(SearchCarsService::class);
        /** @var LengthAwarePaginator $paginator */
        return response()->json(
            $search
            ->search($params)
            ->paginate(9)
            ->appends($request->validated())
            ->map(function (Car $car) {
                return collect((new \App\Resources\Car($car)));
            })
        );
    }

    public function getCars () {
        $query = DB::table('selectionCars')
            ->join('cars', 'selectionCars.car_id', '=', 'cars.id')
            ->join('brands_cars', 'cars.brand_id', '=', 'brands_cars.id')
            ->leftjoin('car_types', 'cars.car_type', '=', 'car_types.id')
            ->leftjoin('cars_gears', 'cars.gear_id', '=', 'cars_gears.id')
            ->leftjoin('cars_images', 'cars.id', '=', 'cars_images.car_id')
            ->join('models_cars', 'cars.model_id', '=', 'models_cars.id')
            ->select('cars.id', 'cars_images.name as image', 'brands_cars.name as brand', 'models_cars.name as model', 'cars.year_of_issue', 'cars.seat', 'cars.doors', 'cars.cost_day', 'car_types.name as car_type', 'cars_gears.name as gear');
            

        if ($id > 0) {
            $query = $query
                ->where('selectionCars.selection_id', $id);
        }
        if (isset($_POST['brandid'])) {
            $query = $query
            ->where('cars.brand_id', $_POST['brandid']);
        }
        if (isset($_POST['modelid'])) {
            $query = $query
            ->where('cars.model_id', $_POST['modelid']);
        }
        
        $selectionCars = $query
        ->where('cars.available', '1')
        ->where('cars.validate', '1')
        ->get();

        $brandsCars = DB::table('brands_cars')
            ->select('brands_cars.id', 'brands_cars.name')
            ->orderBy('name')
            ->get();

        return view('viewCars.listCars', ['selectionCars' => $selectionCars,
                                          'brandsCars' => $brandsCars,
                                          'param' => $_POST]);
    }

    public function getCarCard ($id) {
        $query = DB::table('cars')
            ->join('brands_cars', 'cars.brand_id', '=', 'brands_cars.id')
            ->leftjoin('car_types', 'cars.car_type', '=', 'car_types.id')
            ->leftjoin('cars_gears', 'cars.gear_id', '=', 'cars_gears.id')
            ->join('models_cars', 'cars.model_id', '=', 'models_cars.id')
            ->where('cars.id', $id)
            ->select('cars.id', 'cars.available', 'brands_cars.name as brand', 'models_cars.name as model', 'cars.year_of_issue', 'cars.seat', 'cars.doors', 'cars.cost_day', 'car_types.name as car_type', 'cars_gears.name as gear');
           
            $selectionCars = $query
                ->get();

        $images = DB::table('cars_images')
            ->select('cars_images.name as img_url')
            ->where('cars_images.car_id', $id)
            ->get();

            return response()->json(['selectionCars' => $selectionCars, 'images' => $images]);
    }

    public function getModels(Request $request)
    {
        return response()->json(
            collect(
                BrandsCar::findOrFail($request->get('brandid'))->models
                )->map(function ($model) {
                    return collect($model)->only([
                        'id',
                        'name',
                        'brand_id'
                    ])->toArray();
                })
        );
    }
}
