<?php

namespace App\Http\Middleware;

use Closure;

class HandleUtmTags
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->utm_source and $request->utm_medium and $request->utm_campaign) {
            session([
                'utm' => [
                    'source' => $request->utm_source,
                    'medium' => $request->utm_medium,
                    'campaign' => $request->utm_campaign
                ]
            ]);
        }
        return $next($request);
    }
}
