<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;

/**
 * App\User
 *
 * @property string $birthday
 * @property integer $driving_experience
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $passport_images_id
 * @property int|null $isAdmin
 * @property int|null $isOwner
 * @property int|null $isCustomer
 * @property int|null $validate
 * @property int|null $personal_user_id
 * @property string|null $phone
 * @property int $confirmed
 * @property int|null $passport_face
 * @property int|null $passport_registry
 * @property int|null $event_push_type
 * @property int|null $gear_type
 * @property int|null $email_notif
 * @property int|null $sms_notif
 * @property int|null $app_notif
 * @property string|null $avatar
 * @property string|null $description
 * @property string|null $preferred_type_KPP
 * @property string|null $country
 * @property string|null $city
 * @property string|null $vk_link
 * @property string|null $fb_link
 * @property string|null $inst_link
 * @property int|null $isActive
 * @property int|null $validate_refusal_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\BookingDate[] $bookingDates
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Car[] $cars
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read mixed $age
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \App\Models\PersonalUser|null $personalUser
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Token[] $tokens
 * @property-read \App\Models\UtmData $utmData
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User applyToken()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAppNotif($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereConfirmed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDrivingExperience($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailNotif($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEventPushType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFbLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereGearType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereInstLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIsAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIsCustomer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIsOwner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassportFace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassportImagesId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassportRegistry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePersonalUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePreferredTypeKPP($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereSmsNotif($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereValidate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereValidateRefusalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereVkLink($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable 
// implements MustVerifyEmail
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'passport_face', 'passport_registry', 'personal_user_id', 'confirmed', 'phone', 'birthday', 'driving_experience'
    ];

    protected $redirectTo = '/';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isOwner(): bool
    {
        return (bool)$this->cars()->count();
    }

    public function cars()
    {
        return $this->hasMany('App\Models\Car');
    }

    public function tokens()
    {
        return $this->hasMany('App\Models\Token');
    }

    public function personalUser()
    {
        return $this->belongsTo('App\Models\PersonalUser', 'personal_user_id', 'id', 'users_personal_user_id_foreign');
    }

    public function scopeApplyToken($query)
    {
        return $query->tokens()->apply();
    }


    public function utmData()
    {
        return $this->hasOne('App\Models\UtmData');
    }


    /**
     * @param int $module_id
     * @param int $role_id
     * @return bool
     */
    public function hasAccessToEdit($module_id = 6, $role_id = 1)
    {
        $has = DB::table('users as u')
            ->join('staff_access as s', 's.user_id', 'u.id')
            ->where('s.module_id', $module_id)
            ->first();
        $is_admin = DB::table('users as u')
            ->join('staff as s', 's.user_id', 'u.id')
            ->where('s.role_id', $role_id)
            ->first();
        return $has or $is_admin;

    }

    public static function userBookingCounts()
    {
        return self::join('booking_date as bd','bd.user_id','=','users.id')->orderBy('users.id')->select('users.id as user_id',DB::raw('count(*) as count '))->groupBy('user_id')->get()->toArray();
    }

    public function bookingDates()
    {
        return $this->hasMany('App\Models\BookingDate');
    }

    public function countBookingDates()
    {
        return $this->bookingDates()->count();
    }
    public static function getUserInfo($id)
    {
       $user = self::select(self::getUserInfoFields())->find($id);
        return \Helpers::replaceNullToEmptyString($user->toArray());
    }
    public static function getUserInfoFields()
    {
        return ['name', 'email_verified_at', 'created_at', 'validate', 'confirmed', 'avatar', 'description', 'city'];
    }

    public function getAgeAttribute()
    {
        if ($this->birthday) {
            $birthday = Carbon::createFromFormat('Y-m-d', $this->birthday);
            return Carbon::now()->diff($birthday)->y;
        }
        return 0;

    }
    


}
