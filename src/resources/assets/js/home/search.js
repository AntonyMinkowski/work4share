import $ from'jquery';
let $coordinatesInput = $('#coordinates');

//Поиск, при фокусе открыть поисковые результаты
$('.search').focus(function() {
  $(this).closest('.form__search').find('.form__search__block').fadeIn();
});
//Поиск, выполнить поиск по введенным данным
$('.search').keyup(function() {
  search();
  
});
//Поиск, ввести выбранный вариант доставки в значение поля input
$('.form__search li').click(function() {
  $(this).closest('.form__search').find('input').val($(this).text());
  

  if ($(this).hasClass('location')) {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (position) {
        console.log(position);
        $coordinatesInput.val(`${position.coords.latitude},${position.coords.longitude}`);
      }, function () {
        console.log('your address not found')
      });
    } else {
      console.log('you dont navigator')
    }
  } else {
    $coordinatesInput.val('');
  }

}); 
//Функция поиска
function search() {
  // Declare variables
  var input, filter, ul, li, a, i, txtValue;
  input = document.getElementById('search');
  filter = input.value.toUpperCase();
  ul = document.getElementById('search_ul');
  li = ul.getElementsByTagName('li');

  // Loop through all list items, and hide those who don't match the search query
  for (i = 0; i < li.length; i++) {
    a = li[i].getElementsByTagName('a')[0];
    txtValue = a.textContent || a.innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      li[i].style.display = '';
    } else {
      li[i].style.display = 'none';
    }
  }
}
//Поиск, потеря фокуса, скрытие блока поиска
$('.search').blur(function() {
  $(this).closest('.form__search').find('.form__search__block').fadeOut();
});