try {
    window.$ = window.jQuery = require('jquery');


    var year_of_issue = '';
    var seat = '';
    var doors = '';
    var costDay = '';
    var btnLoadPhotoSts = false;
    var btnLoadPhotoAuto = false;

    var changeBrand = 0;
    var changeModel = 0;
    var changeGear = 0;
    var changeCar = 0;

    function btnSaveData () {
        console.log("btnSaveData " + year_of_issue + " " + seat + " " + doors);
        if (year_of_issue > 0 && seat > 0 && doors > 0) {
            $("#loadPhotoSts").show("slow");
            $( "input[name=year_of_issue]" ).remove();
            $( "input[name=doors]" ).remove();
            $( "input[name=seat]" ).remove();
            $( "input[name=cost_day]" ).remove();

            $("#car_fields_year").append("<p>" + year_of_issue + "</p>");
            $("#car_fields_doors").append("<p>" + doors + "</p>");
            $("#car_fields_seat").append("<p>" + seat + "</p>");
            $("#car_fields_cost_day").append("<p>" + costDay + "</p>");

        }
    }

    $(document).ready(function(){

        $( "input[name=year_of_issue]" ).focusout(function() {
            var value = $( this ).val();
            year_of_issue = value;
            
        });

        $( "input[name=doors]" ).focusout(function() {
            var value = $( this ).val();
            doors = value;
        });

        $( "input[name=seat]" ).focusout(function() {
            var value = $( this ).val();
            seat = value;
        });

        $( "input[name=cost_day]" ).focusout(function() {
            var value = $( this ).val();
            costDay = value;
        });


        $("#btnLoadPhotoSts").click(function() {
            btnLoadPhotoSts = true;

            if (btnLoadPhotoSts && btnLoadPhotoAuto) {
                $("#loadPhotoFeature").show("slow");
                $("#btnLoadPhotoSts").hide("slow");
                $("#StsPhoto").hide("slow");
                $("#btnLoadPhotoAuto").hide("slow");
                $("#AutoPhoto").hide("slow");
            }

        });

        $("#btnLoadPhotoAuto").click(function() {
            btnLoadPhotoAuto = true;

            if (btnLoadPhotoSts && btnLoadPhotoAuto) {
                $("#loadPhotoFeature").show("slow");
                $("#btnLoadPhotoAuto").hide("slow");
                $("#StsPhoto").hide("slow");
                $("#btnLoadPhotoSts").hide("slow");
                $("#AutoPhoto").hide("slow");
            }

        });

        

        $.ajaxSetup({
            headers:
            { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });

        $(".upload_sts_btn").click(function(){
            var pathname = window.location.pathname;
            var request;
            var img = new FormData($("#upload_pts")[0]);

            var size = 0;
            for(var pair of img.entries()) {
                if (pair[1] instanceof Blob)
                    size += pair[1].size;
                else
                    size += pair[1].length;
            }

            if(size==0) {
                console.log("файл пустой или не выбран");
                return false;
            }
            request = $.ajax({
                url: pathname + '/sts',
                method: "POST",
                data: new FormData($("#upload_pts")[0]),
                datatype: "json",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
            });

            request.done(function(msg) {

                console.log(msg);
            });

            request.fail(function(jqXHR, textStatus) {
                $("#result").html("Request failed: " + textStatus);

                console.log('Error!' + textStatus);
            });

        });

        $("#car_fields").on("submit", (function(evt){
            evt.preventDefault();
            var pathname = window.location.pathname;
            request = $.ajax({
                url: pathname + '/update',
                type: 'POST',
                data: $( this ).serialize()
            });

            request.done(function(msg) {
                $("#result").html(msg);

                console.log('Success!');
                $("#btnSaveData").hide("slow");

                btnSaveData();

            });

            request.fail(function(jqXHR, textStatus) {
                $("#result").html("Request failed: " + textStatus);

                console.log('Error!' + textStatus);
            });
       }));

        $("#form_params_auto").on("submit", (function(evt){
            evt.preventDefault();
            var pathname = window.location.pathname;
            request = $.ajax({
                url: pathname + '/feature',
                type: 'POST',
                data: $( this ).serialize()
            });

          request.done(function(msg) {
              $("#result").html(msg);

              console.log('Success!');
              console.log(msg)
              

              $("#btnSaveData").hide("slow");
          });

          request.fail(function(jqXHR, textStatus) {
              $("#result").html("Request failed: " + textStatus);

              console.log('Error!' + textStatus);
          });
        }));


        $(".upload_image_btn").click(function(){
            var pathname = window.location.pathname;
            var request;
            var img = new FormData($("#upload_image")[0]);
            console.log($("#upload_image")[0]);
            

            var size = 0;
            for(var pair of img.entries()) {
                if (pair[1] instanceof Blob)
                    size += pair[1].size;
                else
                    size += pair[1].length;
            }

            if(size==0) {
                console.log("файл пустой или не выбран");
                return false;
            }

            request = $.ajax({
                url: pathname + '/image',
                method: "POST",
                data: new FormData($("#upload_image")[0]),
                datatype: "json",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
            });

            request.done(function(msg) {
                $("#result").html(msg);

                console.log(msg);
            });

            request.fail(function(jqXHR, textStatus) {
                $("#result").html("Request failed: " + textStatus);

                console.log('Error!' + textStatus);
            });

        });

        // событие выбора бренда. подгужаем для выбранного бренда досьупные модели
        $( "#brandid" ).change(function () {
            // скрываем список моделей
            $("#model").addClass("d-none");
            var str = "";
            $.ajax({
                method: "GET",
                url: "/car/models",
                data: {
                    brandid: $(this).val()
                },
                success: function(data){
                    
                    
                    var select_list = $('#model')
                    select_list.find('.form-cars__option').remove();
                    $.each( data, function( key, val ) {
                        console.log(val);
                        select_list.append(
                            "<option class='form-cars__option' value='"
                            + val.id
                            + "'>"
                            + val.name
                            + "</option>");
                    });
                    // теперь можно показать
                    $("#model").removeClass("d-none");
                }
            });
        });
    });


    function subminButton () {
        if (changeBrand > 0 && changeModel > 0 && changeGear > 0 && changeCar > 0) {
          $("#submitCreateCar").show("slow");
        }
    }



    $('#brandid').on('change', function() {
      changeBrand = this.value;
      subminButton();
      });

    $('#modelid').on('change', function() {
      changeModel = this.value;
      subminButton();
      });

    $('#gearid').on('change', function() {
      changeGear = this.value;
      subminButton();
      });

    $('#carTypeid').on('change', function() {
      changeCar = this.value;
      subminButton();
      });
} catch (e) {}
