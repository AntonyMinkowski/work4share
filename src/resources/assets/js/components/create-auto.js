const loading = $('.loading');
filepond();
osagokasko();
//Страница создания авто, сохраняем время аренды 'rent_time_cars'
$('body').on('submit', '.changing-card form:not("#sendValidateSms")', function(e){
    e.preventDefault();
    const form = $(this);
    loading.addClass('active')
  $.ajax({
      type: form.attr('method'),
      url: form.attr('action'),
      data: form.serialize()
  }).done(function(res) {
    // console.log(res);
    
    $('.changing-card .card-list').load(' .changing-card .card-list > *', function(){
        filepond();
        osagokasko();
    });
    setTimeout(function(){
        loading.removeClass('active')
    }, 1000)
  }).fail(function(err) {
    //   console.log(err.responseJSON);
      
    loading.removeClass('active');
    alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
  });
});

$('body').on('submit','#passportData',function(e){
    e.preventDefault();
    const form = $(this);
    const loading = $('.loading');
    loading.addClass('active')
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize()
    }).done(function(res) {
        console.log(res);
        if (res.message) {
            $('#modal').modal('show');
            $('#modal .modal-body').text(res.message);    
        }

        $('.card__photo__sts').removeClass('d-none').addClass('d-flex');
        
        setTimeout(function(){
        loading.removeClass('active')
        }, 1000)
        
    }).fail(function(err) {   
        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');

        // for (item in err.responseJSON) {
        //     $(`input[name*='${item}']`).addClass('error');
        // }
        setTimeout(function(){
            loading.removeClass('active')
        }, 1000)
    });
});


$('body').on('submit','#redirect',function(e){
    e.preventDefault();
    const form = $(this);
    const loading = $('.loading');
    loading.addClass('active')
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize()
    }).done(function(res) {
        console.log(res);
        form.find('.error').addClass('d-none');
        location.href = res;
        Cookies.set('modal', 'show');
        setTimeout(function(){
            loading.removeClass('active')
        }, 1000)
        
    }).fail(function(err) {   
        form.find('.error').removeClass('d-none');
        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
        setTimeout(function(){
            loading.removeClass('active')
        }, 1000)
    });
});

$('.btn-close').click(function(){
    $('#modal').modal('toggle');
});



function filepond(){
    FilePond.registerPlugin(
        FilePondPluginImagePreview,
    );

    FilePond.setOptions({
        labelIdle: 'Перетащите файлы в эту область или нажмите чтобы загрузить',
        labelFileProcessing: 'Загрузка',
        labelFileProcessingComplete: 'Загрузка завершена',
        labelTapToCancel: 'завершить',
        labelTapToUndo: 'отменить',
        labelFileProcessingError: 'Ошибка загрузки',
        labelTapToRetry: 'повторить'
    });





    //Загрузка лицевой части паспорта
    FilePond.create(
        document.querySelector('#passport_face'),
        {
            name: 'photo',
            server: {
                url: window.location.pathname + '/passport_face',
                process: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: function(res){
                        console.log(res);
                    },
                    onerror: function(res){
                        console.log(res);
                        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                    }
                }
            }
        }
    );
    //Загрузка задней части паспорта
    FilePond.create(
        document.querySelector('#passport_visa'),
        {
            name: 'photo',
            server: {
                url: window.location.pathname + '/passport_registry',
                process: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: function(res){
                        console.log(res);
                    },
                    onerror: function(res){
                        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                    }
                }
            }
        }
    );
    //Загрузка задней части паспорта
    FilePond.create(
        document.querySelector('#selfie'),
        {
            name: 'photo',
            server: {
                url: window.location.pathname + '/selfie',
                process: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: function(){
                        loading.addClass('active')
                        $('.changing-card .card-list').load(' .changing-card .card-list > *', function(){
                            filepond();
                            osagokasko();
                        });
                        setTimeout(function(){
                            loading.removeClass('active')
                        }, 1000)
                    },
                    onerror: function(err){
                        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                    }
                }
            }
        }
    );
    //Загрузка СТС 1
    FilePond.create(
        document.querySelector('#sts1'),
        {
            name: 'photo',
            server: {
                url: window.location.pathname + '/sts?id=sts_image_part_one',
                process: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: function(res){
                        console.log(res);
                    },
                    onerror: function(res){
                        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                    }
                }
            }
        }
    );
    //Загрузка СТС 2
    FilePond.create(
        document.querySelector('#sts2'),
        {
            name: 'photo',
            server: {
                url: window.location.pathname + '/sts?id=sts_image_part_two',
                process: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: function(res){
                        loading.addClass('active')
                        $('.changing-card .card-list').load(' .changing-card .card-list > *', function(){
                            filepond();
                            osagokasko();
                        });
                        setTimeout(function(){
                            loading.removeClass('active')
                        }, 1000)
                    },
                    onerror: function(res){
                        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                    }
                }
            }
        }
    );
    //Загрузка ПТС 1
    FilePond.create(
        document.querySelector('#pts1'),
        {
            name: 'photo',
            server: {
                url: window.location.pathname + '/sts?id=pts_image_part_one',
                process: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: function(res){
                        console.log(res);
                    },
                    onerror: function(res){
                        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                    }
                }
            }
        }
    );
    //Загрузка ПТС 2
    FilePond.create(
        document.querySelector('#pts2'),
        {
            name: 'photo',
            server: {
                url: window.location.pathname + '/sts?id=pts_image_part_two',
                process: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: function(res){
                        console.log(res);
                        loading.addClass('active')
                        $('.changing-card .card-list').load(' .changing-card .card-list > *', function(){
                            filepond();
                            osagokasko();
                        });
                        setTimeout(function(){
                            loading.removeClass('active')
                        }, 1000)
                    },
                    onerror: function(res){
                        loading.removeClass('active')
                        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                    }
                }
            }
        }
    );
    //Загрузка фото для авто
    FilePond.create(
        document.querySelector('#photo'),
        {
            name: 'photo',
            allowMultiple: true,
            server: {
                url: window.location.pathname + '/image',
                process: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: function(res){
                        $('body').on('click', '#savePhotoCar', function(){
                            loading.addClass('active')
                            $('.changing-card .card-list').load(' .changing-card .card-list > *', function(){
                                filepond();
                                osagokasko();
                            });
                            setTimeout(function(){
                                loading.removeClass('active')
                            }, 1000)
                        });
                    },
                    onerror: function(res){
                    
                        
                        loading.removeClass('active')
                        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                    }
                }
            }
        }
    );

    //Загрузка аватара для пользоваетля
    FilePond.create(
        document.querySelector('#avatar'),
        {
            name: 'avatar',
            labelIdle: 'Загрузите сюда ваш аватар',
            server: {
                url: window.location.pathname + '/uploadAvatar',
                process: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: function(res){
                        console.log(res);
                        $('.card__photo').fadeIn();
                    },
                    onerror: function(res){
                        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                    }
                }
            }
        }
    );
    //Загрузка ОСАГО
    FilePond.create(
        document.querySelector('#osago'),
        {
            name: 'photo',
            labelIdle: 'Загрузите сюда ваш полис ОСАГО',
            server: {
                url: window.location.pathname + '/osago_image',
                process: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: function(res){
                        console.log(res);
                        loading.addClass('active');
                        $('.changing-card .card-list').load(' .changing-card .card-list > *', function(){
                            filepond();
                            osagokasko();
                        });
                        setTimeout(function(){
                            loading.removeClass('active')
                        }, 1000)
                    },
                    onerror: function(res){
                        loading.removeClass('active');

                        console.log(res)
                        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                    }
                }
            }
        }
    );
    //Загрузка КАСКО
    FilePond.create(
        document.querySelector('#kasko'),
        {
            name: 'photo',
            labelIdle: 'Загрузите сюда ваш полис КАСКО',
            server: {
                url: window.location.pathname + '/kasko_image',
                process: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: function(res){
                        loading.addClass('active')
                        $('.changing-card .card-list').load(' .changing-card .card-list > *', function(){
                            filepond();
                            osagokasko();
                        });
                        setTimeout(function(){
                            loading.removeClass('active')
                        }, 1000)
                    },
                    onerror: function(res){
                        loading.removeClass('active')
                        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                    }
                }
            }
        }
    );

}

function osagokasko(){
    $('input[name=osago_has]').change(function(){
        let value = $(this).val();
        let osagoNeed = $('.osago__need');
        let load = $(this).closest('.card__osago').find('.card-list__Upload');
        let btn = $(this).closest('.card__osago').find('.btn');

        if (value == 0) {
            osagoNeed.animate({ opacity: 0 });
            load.removeClass('d-block').addClass('d-none');
            btn.removeClass('d-none').addClass('d-block');
            $('#modal').modal('show');
            $('#modal .modal-body').text('К сожалению, мы не можем допустить автомобиль с ограниченным полисом ОСАГО. ');
    
        } else{
            osagoNeed.animate({ opacity: 1 });
            btn.removeClass('d-block').addClass('d-none');
            load.removeClass('d-none').addClass('d-block');
        }
    });
    $('input[name=kasko_has]').change(function(){
        let value = $(this).val();
        let load = $('.card__osago').find('.card-list__Upload');
        let kaskoNeed = $('.kasko__need');
        let photo = $('.kasko__photo')
        let btn = $(this).closest('.card__osago').find('.btn');
        photo.animate({ opacity: 0 }) 
        if (value == 0) {
            kaskoNeed.animate({ opacity: 0 })
            btn.removeClass('d-none').addClass('d-block');
        } else{
            kaskoNeed.animate({ opacity: 1 })
            btn.removeClass('d-block').addClass('d-none')
        }

    });

    $('input[name=need_kasko]').change(function(){
        let value = $(this).val();
        let photo = $('.kasko__photo')
        let btn = $(this).closest('.card__osago').find('.btn');

        if (value == 0) {
            btn.removeClass('d-none').addClass('d-block')
            photo.animate({ opacity: 0 }) 
        } else{
            photo.animate({ opacity: 1 })
            btn.removeClass('d-block').addClass('d-none')
        }

    });
}

//Страница создания авто, сохраняем "Год выпуска", "Количество сидения", "Количество дверей"
$('#saveYearSeatsDoors').submit(function(e){
    e.preventDefault();
    let form = $(this);
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize()
    }).done(function(res) {
        console.log(res);
        
        $('.card__cost').removeClass('d-none').addClass('d-flex');
        form.find('button').animate({ opacity: 0 })
    }).fail(function(err) {
        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
    });
});

//Страница создания авто, сохраняем "Тип подключения", "Цена за час"
$('#saveTypeCost').submit(function(e){
    e.preventDefault();
    let form = $(this);
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize()
    }).done(function(res) {
        $('.card__rent').removeClass('d-none').addClass('d-flex');
        form.find('button').animate({ opacity: 0 })
    }).fail(function(err) {
        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
    });
});

// //Страница создания авто, сохраняем "Тип подключения", "Цена за час"
// $('#mileage_inclusive').submit(function(e){
//     e.preventDefault();
//     let form = $(this);
//     $.ajax({
//         type: form.attr('method'),
//         url: form.attr('action'),
//         data: form.serialize()
//     }).done(function(res) {
//         $('.card__rent').removeClass('d-none').addClass('d-flex');
//         form.find('button').animate({ opacity: 0 })
//         console.log(res);
        
//     }).fail(function(err) {
//         alert(err.responseJSON.message);
        
//         alert('Упс...Что-то пошло не так!')
//     });
// });

$('#saveFeatured').submit(function(e){
    e.preventDefault();
    let form = $(this);
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize()
    }).done(function(res) {

        if ($('.card__verification').length) {
            $('.card__verification').removeClass('d-none').addClass('d-block');     
        } else{
            $('.next').removeClass('d-none').addClass('d-block');     
        }
    }).fail(function(err) {
        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
    });
});

// При выборе значения "из рук в руки" в поле Тип подключения.
// Отображаем поле "Укажите время, за которое нужно уведомить Вас о передаче автомобиля"
$('body').on('change', '#connection_type > input', function(){
    const searchKey = 'рук в руки';
    let $this = $(this);
    // Получаем название выбранного значения
    $typeName = $this.next('label').text();
    
    // Сверяем значения
    let $select = $('select[name=time_alert]');
    if ($typeName.indexOf(searchKey) !== -1) {
        // Отображаем
        $select.attr('required', 'required');
        $select.parent('div').removeClass('d-none');
    } else {
        // Прячим
        $select.removeAttr('required');
        $select.parent('div').addClass('d-none');
        $('#modal').modal('show');
        $('#modal .modal-body').text('Подключение автомобилей к системе SOAUTO Connect начнется в ближайшее время. Добавьте авто на платформу, сдавайте его передавая ключи из рук-в-руки, как только система начнет работать, мы вас уведомим по email.');
    }
})
