@extends('layouts.home')
@section('body_class', 'page')
@section('content')
<div class="info__page">
  <div class="container">
    <div class="row">
      <div class="col-lg-9">
        <h1>{{trans('pages.info.1')}}</h1>
        <div class="info__page__block active" id="first">
          <h2>{{trans('pages.info.2')}}</h2>
          <ul>
            <li>
              {{trans('pages.info.3')}}
          </li>
            <li>
              {{trans('pages.info.4')}}
          </li>
          </ul>
        </div>
        <div class="info__page__block" id="second">
          <h2>{{trans('pages.info.5')}}</h2>
          <ul>
            <li>
              {{trans('pages.info.6')}}
          </li>
            <li>
              {{trans('pages.info.7')}}
          </li>
            <li>
              {{trans('pages.info.8')}}
          </li>
            <li>
              {{trans('pages.info.9')}}
          </li>
            <li>
              {{trans('pages.info.10')}}
          </li>
            <li>
              {{trans('pages.info.11')}}
          </li>
            <li>
              {{trans('pages.info.12')}}
          </li>
            <li>
              {{trans('pages.info.13')}}
          </li>
            <li>
              {{trans('pages.info.14')}}
          </li>
            <li>
              {{trans('pages.info.15')}}
          </li>
          </ul>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="info__page__list">
          <h3>{{trans('pages.info.16')}}</h3>
          <ul>
            <li class="active">
              <a href="#first">{{trans('pages.info.17')}}</a>
            </li>
            <li>
              <a href="#second">{{trans('pages.info.18')}}</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection