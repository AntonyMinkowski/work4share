@extends('layouts.home')
@section('body_class', 'page')
@section('content')
<div class="how__page">
  <section style="background-image: url('/image/how/intro-bg.jpg')" class="how__page__intro page__intro">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div>
            <h1>{{trans('pages.howitworks.1')}}</h1>
            <p>{{trans('pages.howitworks.2')}}</p>
            <div class="d-flex justify-content-center flex-column flex-md-row">
              <a href="{{route('booking')}}" class="btn btn-primary btn-purple"> <i class="icon icon-car"></i>{{trans('pages.howitworks.3')}}</a>
              <a href="{{route('handOver')}}" class="btn btn-primary btn-white">{{trans('pages.howitworks.4')}}</a></div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="how__page__road" id="road">
    <div class="container">
      <div class="row">
        <div class="col-12 d-none d-xl-flex">
          <div class="left">
            <h2>{{trans('pages.howitworks.5')}}</h2>
            <div class="how__page__road__block left__block">
              <h3>{{trans('pages.howitworks.6')}}</h3>
              <p>{{trans('pages.howitworks.7')}}</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>{{trans('pages.howitworks.8')}}</h3>
              <p>{{trans('pages.howitworks.9')}}</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>{{trans('pages.howitworks.10')}}</h3>
              <p>{{trans('pages.howitworks.11')}}</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>{{trans('pages.howitworks.12')}}</h3>
              <p>{{trans('pages.howitworks.13')}}</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>{{trans('pages.howitworks.14')}}</h3>
              <p>{{trans('pages.howitworks.15')}}</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>{{trans('pages.howitworks.16')}}</h3>
              <p>{{trans('pages.howitworks.17')}}</p>
            </div>
          </div>
          <div class="center">
            <div class="car"><img src="/image/car_on_road.png" alt=""></div>
          </div>
          <div class="right">
            <h2>{{trans('pages.howitworks.18')}}</h2>
            <div class="how__page__road__block right__block">
              <h3>{{trans('pages.howitworks.19')}}</h3>
              <p>{{trans('pages.howitworks.20')}}</p>
              <p>{{trans('pages.howitworks.21')}}</p>
            </div>
            <div class="how__page__road__block right__block">
              <h3>{{trans('pages.howitworks.22')}}</h3>
              <p>{{trans('pages.howitworks.23')}}</p>
            </div>
            <div class="how__page__road__block right__block">
              <h3>{{trans('pages.howitworks.24')}}</h3>
              <p>{{trans('pages.howitworks.25')}}</p>
            </div>
            <div class="how__page__road__block right__block">
              <h3>{{trans('pages.howitworks.26')}}</h3>
              <p>{{trans('pages.howitworks.27')}}</p>
            </div>
            <div class="how__page__road__block right__block">
              <h3>{{trans('pages.howitworks.28')}}</h3>
              <p>{{trans('pages.howitworks.29')}}</p>
            </div>
          </div>
        </div>

        <div class="col-12 d-none d-lg-flex d-xl-none medium">
          <div class="left">
            <h2>{{trans('pages.howitworks.30')}}</h2>
            <div class="how__page__road__block left__block">
              <h3>{{trans('pages.howitworks.31')}}</h3>
              <p>{{trans('pages.howitworks.32')}}</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>{{trans('pages.howitworks.33')}}</h3>
              <p>{{trans('pages.howitworks.34')}}</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>{{trans('pages.howitworks.35')}}</h3>
              <p>{{trans('pages.howitworks.36')}}</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>{{trans('pages.howitworks.37')}}</h3>
              <p>{{trans('pages.howitworks.38')}}</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>{{trans('pages.howitworks.39')}}</h3>
              <p>{{trans('pages.howitworks.40')}}</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>{{trans('pages.howitworks.41')}}</h3>
              <p>{{trans('pages.howitworks.42')}}</p>
            </div>
          </div>
          <div class="center">
            <div class="car"><img src="/image/car_on_road.png" alt=""></div>
          </div>
          <div class="right">
            <h2>{{trans('pages.howitworks.43')}}</h2>
            <div class="how__page__road__block right__block">
              <h3>{{trans('pages.howitworks.44')}}</h3>
              <p>{{trans('pages.howitworks.45')}}</p>
              <p>{{trans('pages.howitworks.46')}}</p>
            </div>
            <div class="how__page__road__block right__block">
              <h3>{{trans('pages.howitworks.47')}}</h3>
              <p>{{trans('pages.howitworks.48')}}</p>
            </div>
            <div class="how__page__road__block right__block">
              <h3>{{trans('pages.howitworks.49')}}</h3>
              <p>{{trans('pages.howitworks.50')}}</p>
            </div>
            <div class="how__page__road__block right__block">
              <h3>{{trans('pages.howitworks.51')}}</h3>
              <p>{{trans('pages.howitworks.52')}}</p>
            </div>
            <div class="how__page__road__block right__block">
              <h3>{{trans('pages.howitworks.53')}}</h3>
              <p>{{trans('pages.howitworks.54')}}</p>
            </div>
          </div>
        </div>

        <div class="col-12 justify-content-end d-flex d-lg-none d-xl-none small">
          <div class="center">
            <div class="car"><img src="/image/car_on_road.png" alt=""></div>
          </div>          
          <div class="right">
            <h2>{{trans('pages.howitworks.55')}}</h2>
            <div class="how__page__road__block left__block">
              <h3>{{trans('pages.howitworks.56')}}</h3>
              <p>{{trans('pages.howitworks.57')}}</p>
            </div>
            <div class="how__page__road__block right__block">
              <h3>{{trans('pages.howitworks.58')}}</h3>
              <p>{{trans('pages.howitworks.59')}}</p>
              <p>{{trans('pages.howitworks.60')}}</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>{{trans('pages.howitworks.61')}}</h3>
              <p>{{trans('pages.howitworks.62')}}</p>
            </div>
            <div class="how__page__road__block right__block">
              <h3>{{trans('pages.howitworks.63')}}</h3>
              <p>{{trans('pages.howitworks.64')}}</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>{{trans('pages.howitworks.65')}}</h3>
              <p>{{trans('pages.howitworks.66')}}</p>
            </div>
            <div class="how__page__road__block right__block">
              <h3>{{trans('pages.howitworks.67')}}</h3>
              <p>{{trans('pages.howitworks.68')}}</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>{{trans('pages.howitworks.69')}}</h3>
              <p>{{trans('pages.howitworks.70')}}</p>
            </div>
            <div class="how__page__road__block right__block">
              <h3>{{trans('pages.howitworks.71')}}</h3>
              <p>{{trans('pages.howitworks.72')}}</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>{{trans('pages.howitworks.73')}}</h3>
              <p>{{trans('pages.howitworks.74')}}</p>
            </div>
            <div class="how__page__road__block right__block">
              <h3>{{trans('pages.howitworks.75')}}</h3>
              <p>{{trans('pages.howitworks.76')}}</p>
            </div>
            <div class="how__page__road__block left__block">
              <h3>{{trans('pages.howitworks.77')}}</h3>
              <p>{{trans('pages.howitworks.78')}}</p>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </section>
  <section class="how__page__delivery">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="heading">
            <h2 class="white">{{trans('pages.howitworks.79')}}</h2>
            <p class="yellow">{{trans('pages.howitworks.80')}}</p>
            <h3>{{trans('pages.howitworks.81')}}</h3>
            <div class="row">
              <div class="col-lg-4">
                <div class="how__page__delivery__block"><i class="icon icon-point-on"></i>
                  <p>{{trans('pages.howitworks.82')}}</p>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="how__page__delivery__block"><i class="icon icon-plane"></i>
                  <p>{{trans('pages.howitworks.83')}}</p>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="how__page__delivery__block"><i class="icon icon-point"></i>
                  <p>{{trans('pages.howitworks.84')}}</p>
                </div>
              </div>
            </div>
            <div class="d-flex justify-content-center flex-column flex-md-row"><a href="#" class="btn btn-primary btn-purple"> <i class="icon icon-car"></i>{{trans('pages.howitworks.84')}}</a><a href="#" class="btn btn-primary">{{trans('pages.howitworks.84')}}</a></div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection