@extends('layouts.home')
@section('body_class', 'page')
@section('content')
<div class="handover__page">
  <section style="background-image: url('/image/hand-over/intro-bg.jpg')" class="handover__page__intro page__intro">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div>
            <h1>{{trans('pages.handover.1')}}</h1>
            <p>{{trans('pages.handover.2')}}</p>
            <div class="d-flex justify-content-center"><a href="{{route('car.create.show')}}" class="btn btn-primary btn-purple"> <i class="icon icon-car"></i>{{trans('pages.handover.3')}}</a></div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="handover__page__earn">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="heading">
            <h2 class="black">{{trans('pages.handover.4')}}</h2>
            <p class="font-weight-bold">{{trans('pages.handover.5')}}</p>
            <p>{{trans('pages.handover.6')}}</p>
            <div class="d-flex justify-content-center"><a href="{{route('how')}}" class="btn btn-primary">{{trans('pages.handover.7')}}</a></div>
          </div>
        </div>
        <div class="col-lg-7">
          <div class="handover__page__earn__block">
            <h3>{{trans('pages.handover.8')}}</h3>
            <p>{{trans('pages.handover.9')}}</p>
          </div>
        </div>
        <div class="col-lg-4 offset-lg-1">
          <div class="handover__page__earn__block">
            <h3>{{trans('pages.handover.10')}}</h3>
            <p>{{trans('pages.handover.11')}}</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section style="background-image: url('/image/hand-over/connect-bg.jpg')" class="handover__page__connect">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="heading">
            <h2 class="yellow">{{trans('pages.handover.12')}}</h2>
          </div>
          <div class="d-flex justify-content-center"><img src="/image/hand-over/connect-logo.png" alt=""></div>
          <p class="font-weight-bold">{{trans('pages.handover.13')}}</p>
          <p>{{trans('pages.handover.14')}}</p>
          <div class="d-flex justify-content-center"><a href="{{route('contact')}}" class="btn btn-primary">{{trans('pages.handover.15')}}</a></div>
        </div>
      </div>
    </div>
  </section>
  <section class="quote">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <p>{{trans('pages.handover.16')}}</p>
          <p class="small">{{trans('pages.handover.17')}}</p>
        </div>
      </div>
    </div>
  </section>
  <section class="handover__page__spirit">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="heading">
            <h2 class="white">{{trans('pages.handover.18')}}</h2>
          </div>
        </div>
        <div class="row">
          <div class="end d-flex flex-column align-items-end">
            <i class="icon icon-shield"></i>
            <h3>{{trans('pages.handover.19')}}</h3>
            <p>{{trans('pages.handover.20')}}</p>
          </div>
          <div class="start d-flex flex-column">
            <i class="icon icon-users"></i>
            <h3>{{trans('pages.handover.21')}}</h3>
            <p>{{trans('pages.handover.22')}}</p>
          </div>
        </div>
        <div class="col-12">
          <h4>{{trans('pages.handover.23')}}
            <span>{{trans('pages.handover.24')}}</span>
          </h4>
        </div>
      </div>
    </div>
  </section>
  <section class="handover__page__registry">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2>{{trans('pages.handover.25')}}</h2>
          <div class="d-flex justify-content-center"><a href="{{route('car.create.show')}}" class="btn btn-primary btn-purple"> <i class="icon icon-car"></i>{{trans('pages.handover.26')}}</a></div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection