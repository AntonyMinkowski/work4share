@extends('layouts.home')
@section('body_class', 'page')
@section('content')

<div class="contact__page">
  <section class="contact__page__intro">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h1>{{trans('pages.contact.1')}}</h1>
          <p>{{trans('pages.contact.2')}}</p>
        </div>
      </div>
    </div>
  </section>
  <section class="contact__page__form">
    <div class="container">
      <div class="row">
        <div class="col-lg-2">
          <div class="contact__page__form__block">
            <p>{{trans('pages.contact.3')}}</p><a href="tel:+7 937 536-24-51">+7 937 536-24-51</a>
          </div>
        </div>
        <div class="col-lg-2 offset-lg-1">
          <div class="contact__page__form__block">
            <p>{{trans('pages.contact.4')}}</p><a href="tel:8 800 551 53 01">8 800 551 53 01</a>
          </div>
        </div>
        <div class="col-lg-6 offset-lg-1">
          <div class="contact__page__form__block">
            <p>{{trans('pages.contact.5')}}</p>
            <h3>{{trans('pages.contact.6')}}</h3>
            <form class="form__contact"><input type="text" name="name" placeholder="Тема*"><textarea name="message">{{trans('pages.contact.7')}}</textarea><input type="tel" name="phone" placeholder="Телефон*"><input type="email" name="email" placeholder="Email*"><span>* – {{trans('pages.contact.8')}}</span>
              <div class="checkbox"><input id="ckeckbox" type="checkbox" name="checkbox" checked><label for="ckeckbox" class="label"></label><label for="ckeckbox" class="label-for">{{trans('pages.contact.9')}}</label></div><button type="submit" class="btn btn-primary btn-purple">{{trans('pages.contact.10')}}</button></form>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="quote black">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <p>{{trans('pages.contact.11')}}</p>
          <p class="small">{{trans('pages.contact.12')}}</p>
        </div>
      </div>
    </div>
  </section>
  <section class="contact__page__info">
    <div class="container">
      <div class="row">
        <div class="col-lg-7">
          <h3>{{trans('pages.contact.13')}}</h3>
          <p class="font-weight-bold">{{trans('pages.contact.14')}}</p>
          <p>{{trans('pages.contact.15')}}</p>
          <p>{{trans('pages.contact.16')}}</p>
        </div>
        <div class="col-lg-2 offset-lg-2"><img src="/image/logo.svg" alt=""></div>
      </div>
    </div>
  </section>
  {{-- <section style="background-image: url('/image/map.jpg')" class="contact__page__map"></section> --}}
</div>

@endsection