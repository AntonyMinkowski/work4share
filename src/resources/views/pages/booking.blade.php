@extends('layouts.home')
@section('body_class', 'page')
@section('content')

<div class="booking__page">
  <section style="background-image: url('/image/booking/intro-bg.jpg')" class="booking__page__intro">
    <h1>{{trans('pages.booking.1')}}</h1>
    <p>{{trans('pages.booking.2')}}</p>
    <div class="d-flex justify-content-center"><a href="{{route('car.catalog.index')}}" class="btn btn-primary"> <img src="/image/car.svg" alt="">{{trans('pages.booking.3')}}</a></div>
  </section>
  <section class="booking__page__why">
    <div class="container">
      <div class="row">
        {{-- <div class="col-12 d-none d-sm-none d-md-block">
          <div class="heading">
            <h2>{{trans('pages.booking.4')}}</h2>
          </div>
          <div class="booking__page__why__icons"><i class="icon icon-car"></i><span>+ </span><i class="icon icon-car"></i><span>+</span><i class="icon icon-car"></i></div>
          <div class="booking__page__why__headings">
            <h3>{{trans('pages.booking.5')}}<span>+</span>{{trans('pages.booking.6')}}<span>+</span>{{trans('pages.booking.7')}}</h3>
          </div>
          <div class="booking__page__why__texts">
            <p>{{trans('pages.booking.8')}}</p>
            <p>{{trans('pages.booking.9')}}</p>
            <p>{{trans('pages.booking.10')}}</p>
          </div>
        </div> --}}
        <div class="col-12 d-flex flex-wrap">
          <div class="heading w-100">
            <h2>{{trans('pages.booking.11')}}</h2>
          </div>
          <div class="booking__page__why__block">
            <i class="icon icon-easy"></i>
            <h3>{{trans('pages.booking.12')}}</h3>
            <p>{{trans('pages.booking.13')}}</p>
          </div>
          <div class="booking__page__why__block">
            <i class="icon icon-profit"></i>
            <h3>{{trans('pages.booking.14')}}</h3>
            <p>{{trans('pages.booking.15')}}</p>
          </div>
          <div class="booking__page__why__block">
            <i class="icon icon-comfort"></i>
            <h3>{{trans('pages.booking.16')}}</h3>
            <p>{{trans('pages.booking.17')}}</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="booking__page__secure">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="heading">
            <h2>{{trans('pages.booking.19')}}</h2>
          </div>
          <div class="d-flex justify-content-center">
              <i class="icon icon-shield"></i>
          </div>
          <p class="yellow">{{trans('pages.booking.20')}}</p>
          <p>{{trans('pages.booking.21')}}</p>
        </div>
      </div>
    </div>
  </section>
  <section class="booking__page__how">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="heading">
            <h2>{{trans('pages.booking.22')}}</h2>
          </div>
        </div>
        <div class="col-12 d-flex justify-content-between booking__page__how__row">
          <div class="booking__page__how__block">
            <div>1</div>
            <h3>{{trans('pages.booking.23')}}</h3>
            <p>{{trans('pages.booking.24')}}</p>
          </div>
          <div class="booking__page__how__block">
            <div>2</div>
            <h3>{{trans('pages.booking.25')}}</h3>
            <p>{{trans('pages.booking.26')}}</p>
          </div>
          <div class="booking__page__how__block">
            <div>3</div>
            <h3>{{trans('pages.booking.27')}}</h3>
            <p>{{trans('pages.booking.28')}}</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="quote">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <p>{{trans('pages.booking.29')}}</p>
          <p class="small">{{trans('pages.booking.30')}}</p>
        </div>
      </div>
    </div>
  </section>
  <section class="category category--booking">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2>{{trans('pages.booking.31')}}</h2>
        </div>
        <div class="col d-flex flex-wrap"><a href="{{route('car.catalog.index')}}" class="category__block black">
            <h3>{{trans('pages.booking.32')}}</h3>
            <p>{{trans('pages.booking.33')}}</p><img src="/image/category/black.png" alt="">
          </a><a href="{{route('car.catalog.index')}}" class="category__block yellow">
            <h3>{{trans('pages.booking.34')}}</h3>
            <p>{{trans('pages.booking.35')}}</p><img src="/image/category/yellow.png" alt="">
          </a><a href="{{route('car.catalog.index')}}" class="category__block white">
            <h3>{{trans('pages.booking.36')}}</h3>
            <p>{{trans('pages.booking.37')}}</p><img src="/image/category/white.png" alt="">
          </a><a href="{{route('car.catalog.index')}}" class="category__block red">
            <h3>{{trans('pages.booking.38')}}</h3>
            <p>{{trans('pages.booking.39')}}</p><img src="/image/category/red.png" alt="">
          </a><a href="{{route('car.catalog.index')}}" class="category__block blue">
            <h3>{{trans('pages.booking.39')}}</h3>
            <p>{{trans('pages.booking.40')}}</p><img src="/image/category/blue.png" alt="">
          </a><a href="{{route('car.catalog.index')}}" class="category__block grey">
            <h3>{{trans('pages.booking.41')}}</h3>
            <p>{{trans('pages.booking.42')}}</p><img src="/image/category/grey.png" alt="">
          </a></div>
      </div>
    </div>
  </section>
</div>

@endsection