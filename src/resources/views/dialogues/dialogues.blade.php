@extends('layouts.app')

@section('content')
<div id="chat-dialogues">
    <?php 
        $propsArray = json_encode([
            'chatCars' => $chatCars,
            'user' => $user
        ]); 
    ?>
    <chat-dialogues :props-array="{{ $propsArray }}"></chat-dialogues>
</div>
@endsection