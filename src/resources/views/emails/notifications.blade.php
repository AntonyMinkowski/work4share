<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SoAuto</title>
</head>
<body style="outline: none!important;border: 0;font-size: 100%;font: inherit;vertical-align: baseline;box-sizing: border-box;background-color: #f5f8fa;color: #74787e;height: 100%;line-height: 1.4;width: 100%!important;word-break: break-word;margin: 0;padding: 0; background: #fff; font-family: 'Circe-regular';">
    <table style="outline: none!important;padding: 0;border: 0;font-size: 100%;font: inherit;vertical-align: baseline;font-family: Avenir,Helvetica,sans-serif;box-sizing: border-box;border-collapse: collapse;border-spacing: 0;max-width: 645px;width: 645px;margin: auto;">
        <tbody><tr><td>
            <!--========Header=========-->
            <table style="outline: none!important;padding: 0;border: 0;font-size: 100%;font: inherit;vertical-align: baseline;font-family: Avenir,Helvetica,sans-serif;box-sizing: border-box;border-collapse: collapse;border-spacing: 0;margin: 59px 0 0;width: 100%;">
                <tbody><tr><td>
                    <div class="main-container" style="outline: none!important;border: 0;font-size: 100%;font: inherit;vertical-align: baseline;font-family: Avenir,Helvetica,sans-serif;box-sizing: border-box;max-width: 645px;padding: 0 15px;margin: 0 auto;">
                        <div class="header-content" style="outline: none!important;padding: 0;border: 0;font-size: 100%;font: inherit;vertical-align: baseline;font-family: Avenir,Helvetica,sans-serif;box-sizing: border-box;margin: 0 25px;">
                            <a href="http://soauto.ru" class="logo" style="text-decoration: none!important;border: 0;display: inline-block;color: #007bff;">
                                <img src="https://hb.bizmrg.com/soautomedia/media/Header/logo.png" alt="Logo">
                            </a>
                            <a href="tel:88005459988" class="menu-num-block" style="outline: none!important;margin: 0;padding: 0;font-size: 100%;font: inherit;vertical-align: baseline;font-family: Avenir,Helvetica,sans-serif;box-sizing: border-box;text-decoration: none!important;border: 0;color: #007bff;display: inline-block;width: 156px;text-align: right;float: right;">
                                <span class="menu-number" style="font-size: 17px;color: #000;text-transform: uppercase;font-family: 'Druk-medium';">+7 (800) 551 53 01</span>
                                <span class="menu-number-text" style="font-size: 13px;color: #000;font-family: 'Circe-light';">Бесплатная горячая линия</span>
                            </a>
                        </div>
                    </div>
                </td></tr></tbody>
            </table>
                <table style="outline: none!important;margin: 0;padding: 0;border: 0;font-size: 100%;font: inherit;vertical-align: baseline;font-family: Avenir,Helvetica,sans-serif;box-sizing: border-box;border-collapse: collapse;border-spacing: 0;text-align: center;margin: auto;width: 100%;">
                <tbody><tr><td>
                    <div style="outline: none!important;margin: 0;padding: 0; border: 0;font-size: 100%;font: inherit;vertical-align: baseline;font-family: Avenir,Helvetica,sans-serif;box-sizing: border-box;margin-top: 57px;">
                        <h1 class="main-title" style="outline: none!important;margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;font-weight: bold;margin-top: 0;font-family: 'Druk-medium';font-size: 22px;color: #000;text-align: center;">
                            Привет
                        </h1>
                        @isset($textMessage)
                            {!! $textMessage !!}
                        @endisset
                        @if (isset($url) && $url !== '' && isset($buttonText) && $buttonText !== '')
                                <a href="{{ $url }}" style="outline: none!important;margin: 0;padding: 0;border: 0;font-size: 100%;font: inherit;vertical-align: baseline;font-family: Avenir,Helvetica,sans-serif;box-sizing: border-box;border-collapse: collapse;border-spacing: 0;cursor: pointer;text-decoration: none!important;border: 0;display: inline-block;">
                                    <button style="outline: none!important;margin: 0;padding: 0;border: 0;font-size: 100%;font: inherit;vertical-align: baseline;font-family: Avenir,Helvetica,sans-serif;box-sizing: border-box;border-collapse: collapse;border-spacing: 0;cursor: pointer;text-decoration: none!important;border: 0;display: inline-block;font-family: 'Circe-bold';font-size: 18px;color: #000;background: #F6D200;width: 225px;height: 56px;border-radius: 30px;-webkit-border-radius: 30px;-moz-border-radius: 30px;-ms-border-radius: 30px;-o-border-radius: 30px;margin: 34px auto 0;cursor: pointer;display: block;">
                                        {!! $buttonText !!}
                                    </button>
                                </a>
                        @endisset
                        {{-- <hr style="outline: none!important;font-family: Avenir,Helvetica,sans-serif;box-sizing: border-box;width: 165px;height: 2px;background: #f6d200;margin: 0 auto;border: none;">  --}}
                        <h5 class="main-create-account" style="outline: none!important;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;font-family: 'Circe-light';
                        font-size: 18px;color: #000;max-width: 420px;width: 100%;text-align: center;margin: 38px auto 0;line-height: 1.444;">
                            С Уважением, SOAUTO
                        </h5>
                        <div class="bottom-block" style="outline: none!important;padding: 0;border: 0;font-size: 100%;font: inherit;
                        vertical-align: baseline;font-family: Avenir,Helvetica,sans-serif;box-sizing: border-box;display: inline-flex;margin: 40px auto;text-align: center;">
                            <span class="community-text" style="text-align: left;width: 110px;color: #808080;line-height: 1.286;;">Сообщество автолюбителей</span>
                            <a href="#" class="cummunity-img">
                                <img src="https://hb.bizmrg.com/soautomedia/media/Main-block/Logo-grey.png" alt="Logo" class="cummunity-img">
                            </a>
                        </div>
                        <div class="social" style="outline: none!important;margin: 0;padding: 0;border: 0;font-size: 100%;font: inherit;vertical-align: baseline;font-family: Avenir,Helvetica,sans-serif;box-sizing: border-box;">
                            <ul style="list-style: none;display: inline-flex;margin: auto;padding: 0;">
                                <li style="margin-left: 0px;margin-right: 10px;">
                                    <a href="#">
                                        <img src="https://hb.bizmrg.com/soautomedia/media/social/fb-icon.png" alt="img">
                                    </a>
                                </li>
                                <li style="margin-left: 0px;margin-right: 10px;">
                                    <a href="#">
                                        <img src="https://hb.bizmrg.com/soautomedia/media/social/youtube.png" alt="img">
                                    </a>
                                </li>
                                <li style="margin-left: 0px;margin-right: 10px;">
                                    <a href="#">
                                        <img src="https://hb.bizmrg.com/soautomedia/media/social/instagram.png" alt="img">
                                    </a>
                                </li>
                                <li style="margin-left: 0px;margin-right: 10px;">
                                    <a href="#">
                                        <img src="https://hb.bizmrg.com/soautomedia/media/social/vk.png" alt="img">
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </td></tr></tbody>
            </table>
            <table style="margin-top:70px;width:100%;">
                <tbody><tr><td>
                    <div class="main-container">
                        
                        @isset($url)
                            <a href="{{ $url }}" class="footer-link">
                                {{ $url }}
                            </a>
                        @endisset
                        <span class="copyright">© 2019 SOAUTO. All rights reserved.</span>
                    </div>
                </td></tr></tbody>
            </table>
        </td></tr></tbody>
    </table>
    <!--============Script=============-->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>

</html>