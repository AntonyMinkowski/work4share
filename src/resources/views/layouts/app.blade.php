<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/footer.css') }}" rel="stylesheet">
    <script>
        var car_id = 0;
    </script>

    @stack('scripts')

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MMCHQQX');</script>
    <!-- End Google Tag Manager -->

    <!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
    m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
 
    ym(53583991, "init", {
         clickmap:true,
         trackLinks:true,
         accurateTrackBounce:true,
         webvisor:true
    });
 </script>
 <noscript><div><img src="https://mc.yandex.ru/watch/53583991" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
 <!-- /Yandex.Metrika counter -->
</head>

<body>

    <!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MMCHQQX"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
        
    <div class="loading"><img src="/image/loading.gif" alt=""></div>

	<header class="header">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-4 col-sm-6 col-6">
					<a href="/"><img src="/image/logo.png" alt="логотип socar" class="logo"></a>
				</div>
				<div class="col-md-3 col-sm-6 col-6">
                    @if (Auth::check())
					
                    @endif
				</div>
				<div class="col-md-5 col-sm-12v col-12">
					<div class="techsupport d-flex justify-content-end">
						<p class="techsupport__phone">
							<a href="tel:+78005515301">
								8 800 551 53 01
							</a>
						</p>
					</div>
					<div class="personal d-flex align-items-center justify-content-between">
                        @if (Auth::check())
                            <div class="personal__item d-flex justify-content-between">
                                <a href="{{ route('car.catalog.index')}}" class="personal__link">
                                    {{trans('layout.15')}}
                                </a>
                            </div>
                            <div class="personal__item d-flex justify-content-between">
                                <a href="{{ route('chat')}}" class="personal__link">
                                    {{trans('layout.16')}}
                                </a>
                            </div>
                            <div class="personal__item d-flex justify-content-between">
                                <a href="{{ route('profile.trips')}}" class="personal__link">
                                    {{trans('layout.17')}}
                                </a>
                            </div>
                            <div class="personal__item d-flex justify-content-between">
                                <a href="{{ route('profile.mycars')}}" class="personal__link">
                                    {{trans('layout.18')}}
                                </a>
                            </div>
                        @else
                            <div class="personal__item d-flex justify-content-between">
                                <a href="{{ route('login')}}" class="personal__link">
                                    {{trans('layout.19')}}
                                </a>
                            </div>
                            <div class="personal__item d-flex justify-content-between">
                                <a href="{{ route('register')}}" class="personal__link">
                                    {{trans('layout.20')}}
                                </a>
                            </div>
                        @endif
						@if (Auth::check())
						<div class="menu">
							<i class="far fa-user"></i>
							<div class="menu__hover">
								<a href="/profile">{{trans('layout.21')}}</a>
								<a href="/logout">{{trans('layout.22')}}</a>
							</div>
						</div>
						@endif
					</div>
				</div>
			</div>			
		</div>
	</header>
	<main>
		@yield('content')
    </main>
    
    <!--BEGIN header-->
    <footer class="footer">
        <div class="footer__top">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 d-flex flex-column">
                        <h2>{{trans('layout.1')}}</h2>
                        <p>{{trans('layout.2')}} </p>
                        <p class="last">{{trans('layout.3')}}</p>
                    </div>
                    <div class="col-md-4 offset-md-2 d-flex flex-wrap justify-content-between">
                        <h2 class="w-100">{{trans('layout.4')}}</h2>
                        <ul>
                            <li class="{{ request()->is('/') ? 'active' : '' }}"><a href="/">{{trans('layout.5')}}</a></li>
                            <li class="{{ Request::routeIs('how') ? 'active' : '' }}"><a href="{{route('how')}}">{{trans('layout.6')}}</a></li>
                            <li class="{{ Request::routeIs('booking') ? 'active' : '' }}"><a href="{{route('booking')}}">{{trans('layout.7')}}</a></li>
                            <li class="{{ Request::routeIs('handOver') ? 'active' : '' }}"><a href="{{route('handOver')}}">{{trans('layout.8')}}</a></li>
                            <li class="{{ Request::routeIs('contact') ? 'active' : '' }}"><a href="{{route('contact')}}">{{trans('layout.9')}}</a></li>
                        </ul>
                        <ul>
                            <li><a href="{{ route('info') }}">{{trans('layout.10')}}</a></li>
                            <li><a href="#">{{trans('layout.11')}}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__bottom">
            <div class="container">
                <div class="row">
                    <div class="col d-flex justify-content-between flex-column flex-sm-row">
                        <div class="logo"><img src="/image/logo.svg" alt=""></div>
                        <div class="coopyright">{{trans('layout.12')}}</div><a href="/soauto-oferta.pdf">{{trans('layout.13')}}</a><a target="_blank" href="/politika-konf-soauto.pdf">{{trans('layout.14')}}</a>
                        <div class="social"><a href="https://www.facebook.com/soautoru"><i class="icon icon-facebook"></i></a><a href="https://www.instagram.com/so.auto"><i class="icon icon-instagram"></i></a><a href="https://vk.com/soautoru"><i class="icon icon-vk"></i></a></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

{{--<header class="header">--}}
{{--    <div class="container">--}}
{{--        <div class="row align-items-center">--}}
{{--            <div class="col-4">--}}
{{--                <a href="/"><img src="/image/logo.png" alt="логотип socar" class="logo"></a>--}}
{{--            </div>--}}
{{--            <div class="col-3">--}}
{{--                <a href="{{ route('car.create.show') }}" class="more-cars">--}}
{{--                    + авто--}}
{{--                </a>--}}
{{--            </div>--}}
{{--            <div class="col-5">--}}
{{--                <div class="techsupport d-flex justify-content-end">--}}
{{--                    <p class="techsupport__phone">--}}
{{--                        <a href="tel:+78000000000">--}}
{{--                            8 800 000 00 00--}}
{{--                        </a>--}}
{{--                    </p>--}}
{{--                    <p class="techsupport__text">--}}
{{--                        Круглосуточная служба поддержки--}}
{{--                    </p>--}}
{{--                </div>--}}
{{--                <div class="personal d-flex align-items-center justify-content-between">--}}
{{--                    @if (Auth::check())--}}
{{--                        <div class="personal__item d-flex justify-content-between">--}}
{{--                            <a href="/profile/mycars" class="personal__link">--}}
{{--                                Мои авто--}}
{{--                            </a>--}}
{{--                        </div>--}}

{{--                        <div class="menu">--}}
{{--                            <i class="far fa-user"></i>--}}
{{--                            <div class="menu__hover">--}}
{{--                                <a href="/profile">Профиль</a>--}}
{{--                                <a href="/logout">Выйти</a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    @endif--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</header>--}}
{{--<main>--}}
{{--    @yield('content')--}}
{{--</main>--}}

    <script type="text/javascript"></script>
    <script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script src="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.js"></script>
    <script src="https://unpkg.com/filepond/dist/filepond.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/air-datepicker/2.2.3/js/datepicker.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script type="text/javascript">



    // показать свернуть блок с кнопкой

    function look(t){
        p=document.getElementById(t);
        l=document.getElementById("a-"+t);
        if(p.style.display=="none"){
            l.innerHTML="Свернуть фильтры";
            p.style.display="block";}
        else{
            l.innerHTML="Показать все фильтры";
            p.style.display="none";}
    }




    $('.rent-form__datepicker').data('datepicker')

    FilePond.registerPlugin(
        FilePondPluginImagePreview,
    );

    FilePond.setOptions({
        labelIdle: 'Перетащите файлы в эту область или нажмите чтобы загрузить',
        labelFileProcessing: 'Загрузка',
        labelFileProcessingComplete: 'Загрузка завершена',
        labelTapToCancel: 'завершить',
        labelTapToUndo: 'отменить',
        labelFileProcessingError: 'Ошибка загрузки',
        labelTapToRetry: 'повторить'
    });





    //Загрузка лицевой части паспорта
    FilePond.create(
        document.querySelector('#passport_face'),
        {
            server: {
                url: window.location.pathname + '/passport_face',
                process: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: function(res){
                        console.log(res);
                    },
                    onerror: function(res){
                        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                    }
                }
            }
        }
    );
    //Загрузка задней части паспорта
    FilePond.create(
        document.querySelector('#passport_visa'),
        {
            server: {
                url: window.location.pathname + '/passport_registry',
                process: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: function(res){
                        console.log(res);
                    },
                    onerror: function(res){
                        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                    }
                }
            }
        }
    );
    // //Загрузка задней части паспорта
    // FilePond.create(
    //     document.querySelector('#selfie'),
    //     {
    //         name: 'selfie',
    //         server: {
    //             url: `/selfie/${car_id}`,
    //             process: {
    //                 headers: {
    //                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //                 },
    //                 onload: function(res){
    //                     console.log(res);
    //                 },
    //                 onerror: function(res){
    //                     alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
    //                 }
    //             }
    //         }
    //     }
    // );
    //Загрузка СТС 1
    FilePond.create(
        document.querySelector('#sts1'),
        {
            server: {
                url: window.location.pathname + '/sts?id=sts_image_part_one',
                process: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: function(res){
                        console.log(res);

                    },
                    onerror: function(res){
                        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                    }
                }
            }
        }
    );
    //Загрузка СТС 1
    FilePond.create(
        document.querySelector('#sts2'),
        {
            server: {
                url: window.location.pathname + '/sts?id=sts_image_part_two',
                process: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: function(res){
                        $('.card__photo__auto').removeClass('d-none').addClass('d-flex');
                    },
                    onerror: function(res){
                        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                    }
                }
            }
        }
    );
    //Загрузка ПТС 1
    FilePond.create(
        document.querySelector('#pts1'),
        {
            server: {
                url: window.location.pathname + '/sts?id=pts_image_part_one',
                process: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: function(res){
                        console.log(res);

                    },
                    onerror: function(res){
                        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                    }
                }
            }
        }
    );
    //Загрузка ПТС 2
    FilePond.create(
        document.querySelector('#pts2'),
        {
            server: {
                url: window.location.pathname + '/sts?id=pts_image_part_two',
                process: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: function(res){
                        console.log(res);
                        $('.card__photo__pts').removeClass('d-none').addClass('d-flex');
                        
                    },
                    onerror: function(res){
                        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                    }
                }
            }
        }
    );
    //Загрузка фото для авто
    FilePond.create(
        document.querySelector('#photo'),
        {
            server: {
                url: window.location.pathname + '/image',
                process: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: function(res){
                        $('.card__osago_id').removeClass('d-none').addClass('d-flex');
                    },
                    onerror: function(res){
                        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                    }
                }
            }
        }
    );
    //Загрузка аватара для пользоваетля
    FilePond.create(
        document.querySelector('#avatar'),
        {
            labelIdle: 'Загрузите сюда ваш аватар',
            server: {
                url: window.location.pathname + '/uploadAvatar',
                process: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: function(res){
                        console.log(res);
                        $('.card__photo').fadeIn();
                    },
                    onerror: function(res){
                        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                    }
                }
            }
        }
    );
    if ($('#booking_id').val() !== '') {
        let booking_id = $('#booking_id').val()
        //Загрузка фото поездки
        FilePond.create(
            document.querySelector('#trips_car_interior'),
            {
                labelIdle: '+',
                server: {
                    url: '/profile/trips/addCarInterior/' + booking_id,
                    process: {
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        onload: function(res){
                            console.log(res);
                            $('.card__photo').fadeIn();
                        },
                        onerror: function(res){
                            alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                        }
                    }
                }
            }
        );
        //Загрузка фото остатка ГСМ
        FilePond.create(
            document.querySelector('#trips_fuel_residue'),
            {
                labelIdle: '+',
                server: {
                    url: '/profile/trips/addCarFuelResidue/' + booking_id,
                    process: {
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        onload: function(res){
                            console.log(res);
                            $('.card__photo').fadeIn();
                        },
                        onerror: function(res){
                            alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                        }
                    }
                }
            }
        );
        //Загрузка фото пробега
        FilePond.create(
            document.querySelector('#trips_run'),
            {
                labelIdle: '+',
                server: {
                    url: '/profile/trips/addCarMileagePhoto/' + booking_id,
                    process: {
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        onload: function(res){
                            console.log(res);
                            $('#mileageInput').css("display", "flex");
                            $('.card__photo').fadeIn();
                        },
                        onerror: function(res){
                            alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                        }
                    }
                }
            }
        );
    }
    if ($('#booking_id').val() !== '') {
        let booking_id = $('#booking_id').val()
        //Загрузка фото поездки
        FilePond.create(
            document.querySelector('#myCarCard_car_interior'),
            {
                labelIdle: '+',
                server: {
                    url: '/profile/my-car/addCarInterior/' + booking_id,
                    process: {
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        onload: function(res){
                            console.log(res);
                            $('.card__photo').fadeIn();
                        },
                        onerror: function(res){
                            alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                        }
                    }
                }
            }
        );
        //Загрузка фото остатка ГСМ
        FilePond.create(
            document.querySelector('#myCarCard_fuel_residue'),
            {
                labelIdle: '+',
                server: {
                    url: '/profile/my-car/addCarFuelResidue/' + booking_id,
                    process: {
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        onload: function(res){
                            console.log(res);
                            $('.card__photo').fadeIn();
                        },
                        onerror: function(res){
                            alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                        }
                    }
                }
            }
        );
        //Загрузка фото пробега
        FilePond.create(
            document.querySelector('#myCarCard_run'),
            {
                labelIdle: '+',
                server: {
                    url: '/profile/my-car/addCarMileagePhoto/' + booking_id,
                    process: {
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        onload: function(res){
                            console.log(res);
                            $('#mileageInputMyCar').css("display", "flex");
                            $('.card__photo').fadeIn();
                        },
                        onerror: function(res){
                            alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                        }
                    }
                }
            }
        );
    }
    //Загрузка ОСАГО
    FilePond.create(
        document.querySelector('#osago'),
        {
            labelIdle: 'Загрузите сюда ваш полис ОСАГО',
            server: {
                url: window.location.pathname + '/osago_image',
                process: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: function(res){
                        console.log(res);
                        $('.card__kasko').removeClass('d-none').addClass('d-flex');
                    },
                    onerror: function(res){
                        console.log(res);
                        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                    }
                }
            }
        }
    );
    //Загрузка КАСКО
    FilePond.create(
        document.querySelector('#kasko'),
        {
            labelIdle: 'Загрузите сюда ваш полис КАСКО',
            server: {
                url: window.location.pathname + '/kasko_image',
                process: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: function(res){
                        $('.card__featured').removeClass('d-none').addClass('d-flex');
                    },
                    onerror: function(res){
                        alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                    }
                }
            }
        }
    );


    // if ($('#editPhoto').length) {
    //     //Загрузка авто для фото на странцие редактирования
    //     FilePond.create(
    //         document.querySelector('#editPhoto'),
    //         {
    //             server: {
    //                 url: `/car/${car_id}/image`,
    //                 process: {
    //                     headers: {
    //                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //                     },
    //                     onload: function(res){

    //                     },
    //                     onerror: function(res){
    //                         alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
    //                     }
    //                 }
    //             }
    //         }
    //     );

    // }

    var changeAddPhoto = [0];
    var changeHeadImage = 0;


    function getModels(models)
    {
        changeBrand = models.value;

        $.ajax({
            url: "/car/models?brandid=" + changeBrand,
            success: function(msg){
                console.log(msg)
                $.each(msg, function (key, value){
                    $("#models").append("<option value='" + value.id + "'>" + value.name + "</option>");
                });
            }
        });
    }

    function setModels(models)
    {
        changeModel = models.value;
    }

    $(document).ready(function() {
        $("#bookingDetals").hide();

        $("#globalsearch").click(function() {
            if (changeBrand == 0 && changeModel == 0) {
                alert("Вы ничего не выбрали");
            } else if (changeBrand != 0 && changeModel == 0) {
                document.location.href = "carslist/0?brandid=" + changeBrand;
            } else {
                document.location.href = "carslist/0?brandid=" + changeBrand + "&modelid=" + changeModel;
            }
        });

        $("#search").click(function() {
            if (changeBrand == 0 && changeModel == 0) {
                alert("Вы ничего не выбрали");
            } else if (changeBrand != 0 && changeModel == 0) {
                document.location.href = "?brandid=" + changeBrand;
            } else {
                document.location.href = "?brandid=" + changeBrand + "&modelid=" + changeModel;
            }
        });

        $("#bookingfromDate").change(function() {
            $("#bookingButton").attr("href", $("#bookingButton").attr("href") + "from=" + $("#bookingfromDate").val() + "&");

            if ($("#bookingtoDate").val()) {
                var from = new Date($("#bookingfromDate").val());
                var to = new Date($("#bookingtoDate").val());

                var diff = to - from;
                var milliseconds = diff;
                var seconds = milliseconds / 1000;
                var minutes = seconds / 60;
                var hours = minutes / 60;
                var days = hours / 24;

                $("#fullCostRend").text($("#cost_day").text() * days);
                $("#rentDays").text(days);
                if (days < 4) {
                    $("#deliveryCost").text($("#cost_day").text() / 2);
                } else {
                    $("#deliveryCost").text("Бесплатно");
                }

                $("#bookingDetals").show();

            }
        });

        $("#bookingtoDate").change(function() {
            $("#bookingButton").attr("href", $("#bookingButton").attr("href") + "to=" + $("#bookingtoDate").val() + "&");

            if ($("#bookingfromDate").val()) {
                var from = new Date($("#bookingfromDate").val());
                var to = new Date($("#bookingtoDate").val());

                var diff = to - from;
                var milliseconds = diff;
                var seconds = milliseconds / 1000;
                var minutes = seconds / 60;
                var hours = minutes / 60;
                var days = hours / 24;

                $("#fullCostRend").text($("#cost_day").text() * days);
                $("#rentDays").text(days);
                if (days < 4) {
                    $("#deliveryCost").text($("#cost_day").text() / 2);
                } else {
                    $("#deliveryCost").text("Бесплатно");
                }

                $("#bookingDetals").show();

            }
        });

        $("#goToRentCar").submit(function(e){
            e.preventDefault();
            let form = $(this);
            $.ajax({
                type: form.attr('method'),
                url: form.attr('action'),
                data: form.serialize()
            }).done(function(res) {
                alert(res.message)
                document.location.reload(true);
            }).fail(function(err) {
                alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
            });
        });

        $("#fromrentcar").submit(function(e){
            e.preventDefault();
            let form = $(this);
            $.ajax({
                type: form.attr('method'),
                url: form.attr('action'),
                data: form.serialize()
            }).done(function(res) {
                alert(res.message)
                document.location.reload(true);
            }).fail(function(err) {
                alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
            });
        });

        function readStsPhotoURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#imageStsPhoto').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        function readAutoPhotoURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#imageAutoPhoto').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        function readAutoPhoto(input, id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#imageHolder' + id).children('label').remove();
                    $('#carImage' + id).attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);

            }
        }

        $("#StsPhoto").change(function(){
            readStsPhotoURL(this);
        });

        $("#AutoPhoto").change(function(){
            readAutoPhotoURL(this);
        });

        $(".inputfile").change(function () {
            readAutoPhoto(this);
        });

        $("#file0").change(function () {
            changeAddPhoto.push(0);
            readAutoPhoto(this, 0);
        });

        $("#file1").change(function () {
            changeAddPhoto.push(1);
            readAutoPhoto(this, 1);
        });

        $("#file2").change(function () {
            changeAddPhoto.push(2);
            readAutoPhoto(this, 2);
        });

        $("#file3").change(function () {
            changeAddPhoto.push(3);
            readAutoPhoto(this, 3);
        });

        $("#file4").change(function () {
            changeAddPhoto.push(4);
            readAutoPhoto(this, 4);
        });

        $("#file5").change(function () {
            changeAddPhoto.push(5);
            readAutoPhoto(this, 5);
        });

        $("#file6").change(function () {
            changeAddPhoto.push(6);
            readAutoPhoto(this, 6);
        });

        $("#file7").change(function () {
            changeAddPhoto.push(7);
            readAutoPhoto(this, 7);
        });

        $("#file8").change(function () {
            changeAddPhoto.push(8);
            readAutoPhoto(this, 8);
        });

        $("#file9").change(function () {
            changeAddPhoto.push(9);
            readAutoPhoto(this, 9);
        });

        $('#savePhotoCar').click(function() {
            $.each(changeAddPhoto, function(key, value) {

                if (value > 0) {
                    console.log($("#file" + value).val());
                    $("#mask" + value).show();

                    var fd = new FormData();
                    var files = $('#file' + value)[0].files[0];
                    fd.append('photo', files);
                    fd.append('car_id', car_id);

                    $.ajax({
                        url: '/profile/saveCarImages/' + car_id,
                        type: 'post',
                        data: fd,
                        contentType: false,
                        processData: false,
                        success: function(response){
                            if(response != 0){
                                console.log(response);
                                $("#mask" + value).hide();
                            } else {
                                alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
                            }
                        },
                    });
                }
            });
        });

        $(".myCarPhoto").click(function () {
            changeHeadImage = $(this).attr("data-image");
            $("#saveHeadImageId").find('input').val(changeHeadImage);
            $(".myCarPhoto").css("box-shadow", "");
            $(this).css("box-shadow", "0 0 20px 0px black");
        });

        
        $(".saveHeadImageId").submit(function(e){
            e.preventDefault();
            let form = $(this);
            form.find('.loading').fadeIn(200);
            $.ajax({
                type: form.attr('method'),
                url: form.attr('action'),
                data: form.serialize()
            }).done(function(res) {
                document.location.reload(true);
            }).fail(function(err) {
                alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
            });
        })


        //Список годов для поля "Год выпуска" с 2009 по текущий год
        var yearArr;
        (function getListYear(){
            var d = new Date( "2009");
            first = d.getFullYear();

            var s = new Date();
            second = s.getFullYear();
            yearArr = Array();

            for(i = first; i <= second; i++) yearArr.push(i);
        }())




		/**
		 * При в бивании значения в поле "Цена за день"  задаем значение для поля
		 * "Укажите минимальную сумму для километра перепробега" и
		 * "Укажите максимальную сумму для километра перепробега"
 		 */
		$('form#saveTypeCost').on('keypress change', '[name=cost_day]', function() {
			// Поле: Укажите минимальную сумму для километра перепробега
			const $minOvermileFeeInput = $('#mileage_inclusive [name=min_overmile_fee]');
			// Поле: Укажите максимальную сумму для километра перепробега
			const $maxOvermileFeeInput = $('#mileage_inclusive [name=max_overmile_fee]');

			const minPercent = 0.2;
			const maxPercent = 0.6;
			const costDayValue = this.value;

			if (costDayValue) {
				$minOvermileFeeInput.attr('placeholder', Math.round10(costDayValue / 100 * minPercent, -1));
				$maxOvermileFeeInput.attr('placeholder', Math.round10(costDayValue / 100 * maxPercent, -1));
			}
		});

		/**
		 * Корректировка округления десятичных дробей.
		 *
		 * @param {String}  type  Тип корректировки.
		 * @param {Number}  value Число.
		 * @param {Integer} exp   Показатель степени (десятичный логарифм основания корректировки).
		 * @returns {Number} Скорректированное значение.
		 */
		function decimalAdjust(type, value, exp) {
			// Если степень не определена, либо равна нулю...
			if (typeof exp === 'undefined' || +exp === 0) {
				return Math[type](value);
			}
			value = +value;
			exp = +exp;
			// Если значение не является числом, либо степень не является целым числом...
			if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
				return NaN;
			}
			// Сдвиг разрядов
			value = value.toString().split('e');
			value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
			// Обратный сдвиг
			value = value.toString().split('e');
			return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
		}

		// Десятичное округление к ближайшему
		if (!Math.round10) {
			Math.round10 = function(value, exp) {
				return decimalAdjust('round', value, exp);
			};
		}



    });
    
    

</script>
    @yield('bottom_scripts')
</body>

</html>
