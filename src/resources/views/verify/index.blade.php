@extends('layouts.app')
@section('content')
<div class="container mb-5 checkout verify" id="checkout">
  <div class="row">
    <div class="col-12" id="checkout-lists">
			
				@if ($paymentBooking['result'])
					<div class="alert alert-success mt-4">Оплата прошла успешно</div>
				@else
					<div class="alert alert-danger mt-4">К сожалению не удалось произвести оплату, попробуй еще раз.</div>
				@endif
				
				@if ($paymentBooking['result'])
					@if (!$user['validate'])
							
						@if ($userPersonal->driver_lisense_first && $userPersonal->driver_license_second && $userPersonal->passport_face && $userPersonal->passport_face && $userPersonal->selfie)
							<div class="alert alert-success mt-4">Твои данные получены, ожидай подтверждения поездки.</div>		
						@else
							<div class="alert alert-info mt-4">Чтобы завершить бронирование автомобиля, тебе необходимо заполнить свои данные.</div>
						@endif



						@if ($user['email'] == $user['phone'] || $user['name'] == 'Мое имя')

						@if ($user['email'] == $user['phone'] && $user['name'] == 'Мое имя')
							<h1 class="about-car__title mt-3">Укажи свой Email и как тебя зовут</h1><br>		
						@elseif ($user['email'] == $user['phone'])
							<h1 class="about-car__title mt-3">Укажи свой Email</h1><br>		
						@elseif ($user['name'] == 'Мое имя')
							<h1 class="about-car__title mt-3">Укажи как тебя зовут</h1><br>		
						@endif

				<div class="row">
					<div class="col-md-6">
						@if ($user['email'] == $user['phone'])
						<form id="email-registration" method="POST" action="{{route('verify.email')}}" class="mt-5">
								@csrf
								<div class="login-form__pass">
									<input type="email" name="email" class="login-form__input" id="email"
										placeholder="Введи Email" required>
								</div>
								<p class="text-danger" style="display: none">Этот Email уже зарегистрирован</p>
								<button type="submit" class="btn login-form__button">Подтвердить</button>
							</form>
						@endif

					</div>
					<div class="col-md-6">
							@if ($user['name'] == 'Мое имя')
						<form id="name-registration" method="POST" action="{{route('verify.name')}}" class="mt-5">
							@csrf
							<div class="login-form__pass">
								<input type="text" name="name" class="login-form__input" id="name"
									placeholder="Введи Имя" required>
							</div>
							<button type="submit" class="btn login-form__button">Подтвердить</button>
						</form>
						@endif
					</div>
					{{--  --}}

				</div>
						@endif
				

						@if (!($userPersonal->driver_lisense_first && $userPersonal->driver_license_second && $userPersonal->passport_face && $userPersonal->passport_face && $userPersonal->selfie))
							<h2 class="about-car__title mt-3">
								Заполните свои данные<br>
								- фотографии паспорта<br>
								- фотографии водительского удостоверения
							</h2>
							<ul class="checkout-lists first-step">
								<li class="checkout-list">
										<div class="title-list" id="upload-pasport">
												<span class="marker">&#8226;</span>
												<span class="checkout-title">Загрузка паспорта</span>
										</div>
										@if ($userPersonal->passport_face && $userPersonal->passport_visa)
											<p class="mb-4">Твои данные на проверке</p>
										@else
											<div class="container-upload-pasport checkout__block {{ ($user->confirmed) ? 'd-block' : '' }}">
												<div class="container-form">
													<form enctype="multipart/form-data" method="POST" id="passport" action="{{ route('verify.passport') }}" style="width: 100%;">
														@csrf
														<li class="card__photo card-list__item d-flex justify-content-between align-items-center flex-wrap"
																style="border: none">
																<h2 class="card-list__title card-list__title_small">
																		Загрузите фото паспорта с фотографией
																</h2>

																<div class="card-list__Upload">
																		<div class="input-file-container">
																				<input class="input-file input-file-pasport-image"
																						onchange="inputFileUpload('pasport-image')" id="pasport-image"
																						name="passport_face" type="file">
																				<label tabindex="0" for="pasport-image"
																						class="input-file-trigger input-file-trigger-pasport-image">Выберите
																						файл</label>
																				<p class="file-return file-return-pasport-image"></p>
																		</div>
																</div>


														</li>
														<li class="card__photo card-list__item d-flex justify-content-between align-items-center flex-wrap"
																style="border: none">
																<h2 class="card-list__title card-list__title_small">
																		Загрузите фото паспорта с пропиской
																</h2>
																<div class="card-list__Upload">
																		<div class="input-file-container">
																				<input class="input-file input-file-pasport-register" name="passport_visa"
																						onchange="inputFileUpload('pasport-register')" id="pasport-register"
																						type="file">
																				<label tabindex="0" for="pasport-register"
																						class="input-file-trigger input-file-trigger-pasport-register">Выберите
																						файл</label>
																				<p class="file-return file-return-pasport-register"></p>
																		</div>
																</div>
														</li>
														{{-- <li class="card__photo card-list__item d-flex justify-content-between align-items-center flex-wrap" style="border: none">
																<h2 class="card-list__title w-100">
																				Данные паспорта
																		</h2>
																		<div class="card-list_input">
																				<input id="#" type="text" name="passport_seria" class="login-form__input seria" placeholder="Серия" required="">
																				<input id="#" type="text" name="passport_nomer" class="login-form__input nomer" placeholder="Номер" required="">
																				<input id="#" type="text" name="passport_issued_by" class="login-form__input" placeholder="Кем выдан" required="">
																				<input id="#" type="date" name="passport_issued" class="login-form__input" placeholder="Дата выдачи" required="">
																		</div>
														</li> --}}
														<button type="submit" class="checkout-btn btn">{{ __('Сохранить') }}</button>
													</form>
												</div>
											</div>
										@endif
								</li>
								<li class="checkout-list">
										<div class="title-list" id="driver-license">
											<span class="marker">&#8226;</span>
											<span class="checkout-title">Загрузка водительского удостоверения</span>
										</div>
										@if ($userPersonal->driver_lisense_first && $userPersonal->driver_license_second)
											<p class="mb-4">Твои данные на проверке</p>
										@else
											<div class="container-driver-license checkout__block {{ ($userPersonal->passport_face && $userPersonal->passport_visa) ? 'd-block' : '' }} ">
												<div class="container-form">
													<form enctype="multipart/form-data" method="POST" id="driving" action="{{route('verify.driving')}}" style="width: 100%;">
														@csrf
														<li class="card__photo card-list__item d-flex justify-content-between align-items-center flex-wrap"
																style="border: none">
																<h2 class="card-list__title card-list__title_small">
																		Загрузите фото водительских прав с лицевой стороны
																</h2>
																<div class="card-list__Upload">
																		<div class="input-file-container">
																				<input class="input-file input-file-driving-license-from-front" id="driving-license-from-front"
																						onchange="inputFileUpload('driving-license-from-front')" type="file" name="driving_front">
																				<label tabindex="0" for="driving-license-from-front"
																						class="input-file-trigger input-file-trigger-driving-license-from-front">Выберите файл</label>
																				<p class="file-return file-return-driving-license-from-front"></p>
																		</div>
																</div>
														</li>
														<li class="card__photo card-list__item d-flex justify-content-between align-items-center flex-wrap"
																style="border: none">
																<h2 class="card-list__title card-list__title_small">
																		Загрузите фото водительских прав с оборотной стороны
																</h2>
																<div class="card-list__Upload">
																		<div class="input-file-container">
																				<input class="input-file input-file-driving-license-from-back" id="driving-license-from-back"
																						onchange="inputFileUpload('driving-license-from-back')" type="file" name="driving_back">
																				<label tabindex="0" for="driving-license-from-back"
																						class="input-file-trigger input-file-trigger-driving-license-from-back">Выберите файл</label>
																				<p class="file-return file-return-driving-license-from-back"></p>
																		</div>
																</div>
																{{-- <h2 class="card-list__title mt-4  w-100">
																																								Данные водительских прав
																																						</h2> --}}
																{{-- <div class="card-list_input w-100">
																																								<input name="driver_licence_num" id="#" type="text" class="login-form__input w-100 d-flex mw-100" placeholder="Серия номер водительских прав
																																								" required="">
																																						</div> --}}
														</li>
														<button type="submit" class="checkout-btn btn">{{ __('Сохранить') }}</button>
													</form>
												</div>
											</div>
										@endif
								</li>
								<li class="checkout-list">
									<div class="title-list" id="one-more-step">
										<span class="marker">&#8226;</span>
										<span class="checkout-title">Загрузите селфи с паспортом</span>
									</div>
									@if ($userPersonal->selfie)
										<p class="mb-4">Твои данные на проверке</p>
									@else
										<div class="container-one-more-step checkout__block {{ ($userPersonal->passport_face && $userPersonal->passport_visa && $userPersonal->driver_lisense_first && $userPersonal->driver_license_second) ? 'd-block' : '' }}">
											<div class="container-form">
												<form enctype="multipart/form-data" id="selfie-photo" method="POST" action="{{route('verify.selfie')}}" style="width: 100%;">
													@csrf
													<li class="card__photo card-list__item d-flex justify-content-between align-items-center" style="border: none">
															<h2 class="card-list__title card-list__title_small">
																	Загрузите свое фото
															</h2>
															<div class="card-list__Upload">
																	<div class="input-file-container">
																			<input class="input-file input-file-serfie" id="serfie" onchange="inputFileUpload('serfie')" type="file"
																					name="selfie">
																			<label tabindex="0" for="serfie" class="input-file-trigger input-file-trigger-serfie">Выберите файл</label>
																			<p class="file-return file-return-serfie"></p>
																	</div>
															</div>
													</li>
													<button type="submit" class="checkout-btn btn">{{ __('Сохранить') }}</button>
												</form>
											</div>
										</div>
									@endif
								</li>
							</ul>
						@endif
					@endif
						<a href="{{route('profile.trips.card', $paymentBooking['booking_id'])}}" class="checkout-btn btn mt-4">{{ __('Перейти к поездке') }}</a>
				@endif
				{{-- <form action="" class="flex justify-content-center mt-4">
						<button type="submit" class="checkout-btn btn">{{ __('Оплатить еще раз') }}</button>
				</form> --}}
		</div>
	</div>
</div>


<script>


	function inputFileUpload(nameInput) {

			var fileInput  = document.querySelector(".input-file-" + nameInput),  
					button     = document.querySelector(".input-file-trigger-" + nameInput),
					the_return = document.querySelector(".file-return-" + nameInput);
					
			the_return.innerHTML = fileInput.value.split('fakepath\\')[1]; // Разделить по 'fakepath\', взять вторую часть

	}

$(document).ready(function() {

	document.querySelector("html").classList.add('js');

	$('body').on('submit', '#email-registration', function (e) {
    e.preventDefault();
    const form = $(this);
    const loading = $('.loading');
    const email = $('#email');

     console.log(email.val());
    // console.log(email.val().match("^[a-zA-Z0-9_-]*@[a-zA-Z0-9_-]*[.][a-zA-Z]*"));


    if (email.val()) {
      // if (email.val().match("^[a-zA-Z0-9_-]*@[a-zA-Z0-9_-]*[.][a-zA-Z]*")) {
        loading.addClass('active');
        $.ajax({
          type: form.attr('method'),
          url: form.attr('action'),
          data: form.serialize()
        }).done(function (res) {
          setTimeout(function () {
            loading.removeClass('active')
          }, 1000);



          if (res == 0) {
            form.find('input').addClass('red-border')
            form.find('p').fadeIn();


          } else {
            form.find('input').removeClass('red-border')
            form.find('p').fadeOut();
            $('#checkout-lists').load(` #checkout-lists > *`).fadeIn();
          }
        }).fail(function (err) {
          alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
          setTimeout(function () {
            loading.removeClass('active')
          }, 1000)
        });
       }
  });

  $('body').on('submit', '#name-registration', function (e) {
    e.preventDefault();
    const form = $(this);
    const loading = $('.loading');
    const name = $('#name');

     console.log(name.val());
    // console.log(email.val().match("^[a-zA-Z0-9_-]*@[a-zA-Z0-9_-]*[.][a-zA-Z]*"));


    if (name.val()) {
      // if (email.val().match("^[a-zA-Z0-9_-]*@[a-zA-Z0-9_-]*[.][a-zA-Z]*")) {
        loading.addClass('active');
        $.ajax({
          type: form.attr('method'),
          url: form.attr('action'),
          data: form.serialize()
        }).done(function (res) {
          setTimeout(function () {
            loading.removeClass('active')
          }, 1000);



          if (res == 0) {
            form.find('input').addClass('red-border')
            form.find('p').fadeIn();


          } else {
            form.find('input').removeClass('red-border')
            form.find('p').fadeOut();
            $('#checkout-lists').load(` #checkout-lists > *`).fadeIn();
          }
        }).fail(function (err) {
          alert('Что-то пошло не так. Попробуйте еще раз. Мы уже знаем об этой ошибке и работаем над ее устранением');
          setTimeout(function () {
            loading.removeClass('active')
          }, 1000)
        });
       }
  });

	let isCode = false;

	$('body').on('click', '.checkout-title', function(){
			$(this).closest('.checkout-list').find('.checkout__block').toggleClass('d-block');
	});
});
</script>
@endsection