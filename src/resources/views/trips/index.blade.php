@extends('layouts.app') 
@section('content')
<section class="my-car trips">
	<div class="container">
		<div class="row">
			<div class="col">
				<h1 class="my-car-title">
					{{trans('trips.index.1')}}
				</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<ul class="nav" id="myTab" role="tablist">
					<li class="nav-item">
						<a class="tab_personal__link active" id="active-tab" data-toggle="tab" href="#active" role="tab" aria-controls="active" aria-selected="true">{{trans('trips.index.2')}}</a>
					</li>
					<li class="nav-item">
						<a class="tab_personal__link" id="booking-tab" data-toggle="tab" href="#booking" role="tab" aria-controls="booking" aria-selected="false">{{trans('trips.index.3')}}</a>
					</li>
					<li class="nav-item">
						<a class="tab_personal__link" id="history-tab" data-toggle="tab" href="#history" role="tab" aria-controls="history" aria-selected="false">{{trans('trips.index.4')}}</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="tab-content">
					<div class="tab-pane fade all show active" id="active" role="tabpanel" aria-labelledby="active-tab">
						<div class="row">
							@foreach ($activeCars as $item)
								@if(isset($item) && isset($item->cars) && isset($item->img_url))
									<div class="col-12 col-md-6 col-lg-4">
										<div class="car-wrapper">
											<img src="https://hb.bizmrg.com/soautomedia/{{$item->img_url}}" alt="" class="car-wrapper__img">
											<a href="{{ route('profile.trips.card', $item->id) }}" class="car-wrapper__link d-flex flex-column justify-content-center align-items-center"></a>
											<div class="car-wrapper__box">
												@if(isset($item->cars->brand['name']) && isset($item->cars->model['name']))
													<h1 class="car-wrapper__title">{{$item->cars->brand['name']}} {{$item->cars->model['name']}}</h1>
												@endif
												<div class="car-wrapper__item d-flex">
													<div class="car-wrapper__wrap-left p-0 w-100">
														<div class="car-wrapper__inner d-flex">
															<p class="car-wrapper__text_f" style="width: 30%;">{{trans('trips.index.5')}}</p>
															@isset($item->datefrom)
																@isset($item->dateto)
																	<p class="car-wrapper__text_s" style="width: 70%;font-size: 13px;text-align: right;">
																		{{trans('trips.index.6')}}{{ $item->datefrom }} <br>{{trans('trips.index.7')}}{{ $item->dateto }}</p>
																@endisset
															@endisset
														</div>
														<div class="car-wrapper__inner d-flex align-items-center">
															<p class="car-wrapper__text_f">{{trans('trips.index.8')}}</p>
															@isset($item->statusName)
																<p class="car-wrapper__text">{{$item->statusName}}</p>
															@endisset
														</div>
														<div class="car-wrapper__inner d-flex align-items-center">
															<p class="car-wrapper__text_f">{{trans('trips.index.9')}}</p>
															@isset($item->statusName)
																<p class="car-wrapper__text">{{ $item->totalCost}} {{trans('trips.index.10')}}</p>
															@endisset
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								@endif
							@endforeach
						</div>
					</div>
					<div class="tab-pane fade show" id="booking" role="tabpanel" aria-labelledby="booking-tab">
						<div class="row">
							@foreach ($bookedCars as $item)
								@if(isset($item) && isset($item->cars) && isset($item->img_url))
									<div class="col-12 col-md-6 col-lg-4">
										<div class="car-wrapper">
											<img src="https://hb.bizmrg.com/soautomedia/{{$item->img_url}}" alt="" class="car-wrapper__img">
											<a href="{{ route('profile.trips.card', $item->id) }}" class="car-wrapper__link d-flex flex-column justify-content-center align-items-center"></a>
											<div class="car-wrapper__box">
												@if(isset($item->cars->brand['name']) && isset($item->cars->model['name']))
													<h1 class="car-wrapper__title">{{$item->cars->brand['name']}} {{$item->cars->model['name']}}</h1>
												@endif
												<div class="car-wrapper__item d-flex">
													<div class="car-wrapper__wrap-left p-0 w-100">
														<div class="car-wrapper__inner d-flex">
															<p class="car-wrapper__text_f" style="width: 30%;">{{trans('trips.index.11')}}</p>
															@isset($item->datefrom)
																@isset($item->dateto)
																<p class="car-wrapper__text_s" style="width: 70%;font-size: 13px;text-align: right;">
																	{{trans('trips.index.12')}}{{ $item->datefrom }} <br>{{trans('trips.index.13')}}{{ $item->dateto }}</p>
																@endisset
															@endisset
														</div>
														<div class="car-wrapper__inner d-flex">
															<p class="car-wrapper__text_f">{{trans('trips.index.14')}}</p>
															@isset($item->statusName)
																<p class="car-wrapper__text">{{$item->statusName}}</p>
															@endisset
														</div>
														<div class="car-wrapper__inner d-flex">
															<p class="car-wrapper__text_f">{{trans('trips.index.15)}}</p>
															<p class="car-wrapper__text_s">{{ $item->totalCost}} {{trans('trips.index.16')}}</p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								@endif
							@endforeach
						</div>
					</div>
					<div class="tab-pane fade show" id="history" role="tabpanel" aria-labelledby="history-tab">
						<div class="row">
							@foreach ($historyCars as $item)
								@if(isset($item) && isset($item->cars) && isset($item->img_url))
									<div class="col-12 col-md-6 col-lg-4">
										<div class="car-wrapper">
											<img src="https://hb.bizmrg.com/soautomedia/{{$item->img_url}}" alt="" class="car-wrapper__img">
											<a href="{{ route('profile.trips.card', $item->id) }}" class="car-wrapper__link d-flex flex-column justify-content-center align-items-center"></a>
											<div class="car-wrapper__box">
												@if(isset($item->cars->brand['name']) && isset($item->cars->model['name']))
													<h1 class="car-wrapper__title">{{$item->cars->brand['name']}} {{$item->cars->model['name']}}</h1>
												@endif
												<div class="car-wrapper__item d-flex">
													<div class="car-wrapper__wrap-left p-0 w-100">
														<div class="car-wrapper__inner d-flex">
															<p class="car-wrapper__text_f" style="width: 30%;">{{trans('trips.index.17')}}</p>
															@isset($item->datefrom)
																@isset($item->dateto)
																<p class="car-wrapper__text_s" style="width: 70%;font-size: 13px;text-align: right;">
																	{{trans('trips.index.18')}}{{ $item->datefrom }} <br>{{trans('trips.index.19')}}{{ $item->dateto }}</p>
																@endisset
															@endisset
														</div>
														<div class="car-wrapper__inner d-flex">
															<p class="car-wrapper__text_f">{{trans('trips.index.20')}}</p>
															@isset($item->statusName)
																<p class="car-wrapper__text">{{$item->statusName}}</p>
															@endisset
														</div>
														<div class="car-wrapper__inner d-flex">
															<p class="car-wrapper__text_f">{{trans('trips.index.21')}}</p>
															<p class="car-wrapper__text_s">{{ $item->totalCost }} {{trans('trips.index.22')}}</p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								@endif
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@if (session('result') !== null)
	@if (session('result') === '1')
		<script>
			ym(53583991, 'reachGoal', 'success-pay');
		</script>
		<div class="modal fade" id="modal-trip" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">{{trans('trips.index.23')}} </h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<p>{{trans('trips.index.24')}}</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-close creating-cars-btn mt-0" data-dismiss="modal">{{trans('trips.index.25')}}</button>
				</div>
				</div>
			</div>
		</div>
	@elseif(session('result') == '0')
		<div class="modal fade" id="modal-trip" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">{{trans('trips.index.26')}}</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<p>{{trans('trips.index.27')}}</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-close creating-cars-btn mt-0" data-dismiss="modal">{{trans('trips.index.28')}}</button>
				</div>
				</div>
			</div>
		</div>
	@endif
@endif

@endsection

