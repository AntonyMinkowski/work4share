@extends('layouts.app') 
@section('content')


<section class="rent-car trips__card">
	<div class="container">
		<div class="row">
			<div class="col">
				<h1 class="about-car__title">{{$car->brand->name}} {{$car->models_name}}</h1>
			</div>
			{{-- Добавим id в документ что бы js мог его получить --}}
			<input type="text" id="booking_id" value="{{$booked->id}}" hidden style="display: none">
		</div>
		<div class="row trips__card__intro">
			<div class="col-lg-7 col-12">
				<div class="img-card1 d-flex" style="overflow: hidden;">
					<div class="carousel slide" data-ride="carousel" id="carouselExampleIndicators">
						<div class="carousel-inner">
							<?php $i = 0; ?>
							@foreach ($images as $image)
								<div class="carousel-item @if ($i == 0) active @endif">
									<div class="img-slider" style="background: url(https://hb.bizmrg.com/soautomedia/{{$image->img_url}}) center center no-repeat;"></div>
								</div>

							<?php $i++; ?>
							@endforeach
						</div><a class="carousel-control-prev" data-slide="prev" href="#carouselExampleIndicators" role="button"><span aria-hidden="true" class="carousel-control-prev-icon"></span> <span class="sr-only">Previous</span></a> <a class="carousel-control-next" data-slide="next" href="#carouselExampleIndicators" role="button"><span aria-hidden="true" class="carousel-control-next-icon"></span> <span class="sr-only">Next</span></a>
					</div>
				</div>
			</div>
			<div class="col-lg-5 col-12">
				<div class="trips__card__info">
					<h2>
						{{trans('card.1')}}
					</h2>
					<div class="trips__card__info__content">
						<div class="trips__card__info__text">
							<p>{{trans('card.2')}}</p>
							@isset($statusName)
								<span>{{$statusName}}</span>
							@endisset
						</div>
						<div class="trips__card__info__text">
							<p>{{trans('card.3')}}</p>
							<span>{{ $booked->datefrom }} - {{ $booked->dateto }}</span>
						</div>
						<div class="trips__card__info__text">
							<p>{{trans('card.4')}}</p>
							<span>{{ $booked->count_day }} {{trans('card.5')}}</span>
						</div>
						<div class="trips__card__info__text">
							<p>{{trans('card.5')}}</p>
							<span>{{ $booked->mileage }} {{trans('card.6')}}</span>
						</div>
						<div class="trips__card__info__text">
							<p>{{trans('card.7')}}</p>
							@if (isset($booked->delivery->deliveryCars->rentPlace->name))
							<span>{{ $booked->delivery->deliveryCars->rentPlace->name }}</span>
							@else
							<span>{{ $booked->cars->location }}</span>
							@endif
						</div>
						<div class="trips__card__info__text">
							<p>{{trans('card.8')}}</p>
							<span>{{ $booked->delivery['cost'] ?? 0 }} ₽</span>
						</div>
						<div class="trips__card__info__text">
							<p>{{trans('card.9')}}</p>
							<span>{{ $booked->deposit }} ₽</span>
						</div>
						<div class="trips__card__info__text">
							<p>{{trans('card.10')}}</p>
							<span>{{ $booked->kasko }} ₽</span>
						</div>
						<div class="trips__card__info__text">
							<p>{{trans('card.11')}}</p>
							<span>{{ $booked->payment['amount_booking'] }} ₽</span>
						</div>
						<div class="trips__card__info__text">
							<p>{{trans('card.12')}}</p>
							<span>{{ $booked->payment['total_amount_booking'] }} ₽</span>
						</div>
					</div>
					<div class="alert alert-info">
						<strong>{{trans('card.13')}}!</strong>{{trans('card.14')}} 
					</div>
					@if ($booked->status === 7 && $buttonGet && $user->validate)
						<p class="mt-3">{{trans('card.15')}} <a target="_blank" href="{{route('pdf', [
							'name' => $user->name,
							'brand' => $car->brand->name,
							'model' => $car->model->name,
							'vin' => $car->vin,
							'color' => $car->color->name,
							'gos' => $car->gos_nomer
							])}}">{{trans('card.16')}}</a></p>
					@endif
					<div class="trips__card__info__buttons mt-3" style="display: flex;justify-content: space-around;">
						@isset($booked->status)
							@if ($booked->payment->result)
								@if ($booked->status != 5 && $booked->status != 21 && $booked->status != 23)
									<button type="button" class="btn rent-btn" data-toggle="modal" data-target="#revocation" id="rescission">
									{{trans('card.17')}}</button>
								@endif
								
								@if ($booked->status === 7 && $buttonGet && $user->validate)
									{{-- <a href="/chat" class="btn creating-cars-btn">{{trans('card.18')}}</a> --}}
									<button class="btn creating-cars-btn" onclick="showFilling()">{{trans('card.19')}}</button>
								@endif
								@if ($booked->status === 11)
									<button type="button" class="btn creating-cars-btn" data-toggle="modal" data-target="#pass-car">{{trans('card.20')}}</button>
								@endif
								@if ($booked->status === 15)
									<button type="button" class="btn creating-cars-btn" data-toggle="modal" data-target="#cancel-transfer" style="font-size: 17px">
									{{trans('card.21')}}</button>
								@endif
							@else
								@if ($booked->status != 5 && $booked->status != 21 && $booked->status != 23 && $booked->status != 1)
									<button type="button" class="btn rent-btn" data-toggle="modal" data-target="#revocation" id="rescission">{{trans('card.22')}}</button>
								@endif
								@if ($booked->status == 1)
									<form class="w-100 d-flex justify-content-center" method="POST" action="
									{{ route('profile.trips.payment', $booked->id) }}">
										@csrf
										<button type="submit" class="btn rent-btn">{{trans('card.23')}}</button>
									</form>
								@endif
								
							@endif
						@endisset
					</div>
				</div>
			</div>
		</div>
		<div id="container-filling" style="display: none;">
			<div class="row trips__card__salon">
				<div class="col-12">
					<form method="POST" action="" style="justify-content: flex-start;">
						@csrf
						<h2>{{trans('card.24')}}</h2>

						@foreach ($bookedPhoto as $image)
						<div>
							<div class="trips__card__salon__photo trips__card__add">
								<div>
									<img src="https://hb.bizmrg.com/soautomedia/{{$image->name}}" alt="">
								</div>
							</div>
						</div>
						@endforeach

						<input type="file" id="trips_car_interior" name="trips_car_interior" multiple>
					</form>
					<hr>
				</div>
			</div>
			<div class="row trips__card__another">
				<div class="col-12">
					<div class="container_trips__card__another">
						<div class="fuel trips__card__add">
							<h2>{{trans('card.25')}}</h2>
						</div>
						<input type="file" id="trips_fuel_residue" name="trips_fuel_residue">
						<div class="mileage trips__card__add">
							<h2>{{trans('card.26')}}</h2>
						</div>
						<input type="file" id="trips_run" name="trips_run">
					</div>

					<form enctype="multipart/form-data" style="display: none;" id="mileageInput" method="POST" action="{{ route('trips.addCarMileageInput', $booked->id) }}">
						@csrf
						<div class="input">
							<input type="text" id="mileageTrips" name="mileage" placeholder="Введите пробег">
						</div>
						<button class="btn rent-btn">{{trans('card.27')}}</button>
					</form>
					<hr>
				</div>
			</div>
		</div>
		@if ($booked->status === 15)
			@if ($bookedPhotoLandlord !== [] && $booked->booking_fiel->fiel_photo_after && $booked->booking_mileage->mileage_photo_after && $booked->booking_mileage->mileage_after)
				<div class="car-wrapper__inner d-flex" style="flex-direction:column;height: 100%;">
					<div class="container-check-photos">
						<h1 class="about-car__title">{{trans('card.28')}}</h1>
						<div>
							<h1 class="about-car__title">{{trans('card.29')}}</h1>
							@foreach ($bookedPhotoLandlord as $image)
								<img src="https://hb.bizmrg.com/soautomedia/{{$image->name}}" width="60%" height="100%" alt="">
							@endforeach
						</div>
						<div>
							<h1 class="about-car__title">{{trans('card.30')}} ГСМ</h1>
							<img src="https://hb.bizmrg.com/soautomedia/{{$booked->booking_fiel->fiel_photo_after}}" width="60%" height="100%" alt="">
						</div>
						<div>
							<h1 class="about-car__title">{{trans('card.31')}}</h1>
							<img src="https://hb.bizmrg.com/soautomedia/{{$booked->booking_mileage->mileage_photo_after}}" width="60%" height="100%" alt="">
						</div>
						<div>
							<h1 class="about-car__title">{{trans('card.32')}} </h1>
								<h1 class="about-car__title">{{$booked->booking_mileage->mileage_after}}</h1>
						</div>
						<div class="container-button">
							<a href="/profile/trips/accept/{{$booked->id}}" class="btn creating-cars-btn">{{trans('card.33')}}</a>
							<button class="btn creating-cars-btn">{{trans('card.34')}}</button>
							<button class="btn rent-btn">{{trans('card.35')}}</button>
						</div>
					</div>
				</div>
			@endif				
		@endif				
		@if ($booked->status === 9)				
			<div class="row trips__card__feature">
				<div class="col-12">
					<form action="">
						<h2>{{trans('card.36')}}</h2>
						<div class="trips__card__feature__block">
							<div class="trips__card__feature__text">
								<p>{{trans('card.37')}}</p>
								<span>120 руб</span>
							</div>
							<div class="trips__card__feature__text">
								<p>{{trans('card.38')}}</p>
								<span>20 руб</span>
							</div>
							<div class="trips__card__feature__text">
								<p>{{trans('card.39')}}</p>
								<span>120 руб</span>
							</div>
						</div>
						<a href="/profile/trips/payment/{{$booked->id}}" class="btn creating-cars-btn">{{trans('card.40')}}</a>
					</form>
				</div>
			</div>
			<hr>
		@endif
	</div>
	<div class="modal fade" id="revocation" tabindex="-1" role="dialog" aria-labelledby="revocationLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form enctype="multipart/form-data" method="POST" action="{{ route('trips.rescission', $booked->id) }}">
					@csrf
					<div class="modal-header">
						<h5 class="modal-title" id="revocationLabel">{{trans('card.41')}}</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						</button>
					</div>
					<div class="modal-body">
						<textarea name="rescission_description" id="" rows="10" style="width: 100%;"></textarea>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-close-revocation creating-cars-btn mt-0">{{trans('card.42')}}</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal fade" id="pass-car" tabindex="-1" role="dialog" aria-labelledby="pass-carLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="pass-carLabel">{{trans('card.43')}}</h5>
					<button type="button" class="close btn-close-pass-car" data-dismiss="pass-car" aria-label="Close">
					</button>
				</div>
				<div class="modal-footer" style="display:flex;justify-content:space-between">
					<button type="button" class="btn btn-close-pass-car rent-btn">{{trans('card.44')}}</button>
					<a href="/profile/trips/pass-car/{{$booked->id}}" class="btn creating-cars-btn mt-0">{{trans('card.45')}}</a>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="cancel-transfer" tabindex="-1" role="dialog" aria-labelledby="cancel-transferLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="cancel-transferLabel">{{trans('card.46')}}</h5>
					<button type="button" class="close btn-close-cancel-transfer" data-dismiss="cancel-transfer" aria-label="Close">
					</button>
				</div>
				<div class="modal-footer" style="display:flex;justify-content:space-between">
					<button type="button" class="btn btn-close-cancel-transfer rent-btn">{{trans('card.47')}}</button>
					<a href="/profile/trips/cancel-transfer/{{$booked->id}}" class="btn creating-cars-btn mt-0">{{trans('card.48')}}</a>
				</div>
			</div>
		</div>
	</div>
	<script>
	function showFilling() {
		$('#container-filling').addClass('show-filling') 
	}

	$(function () {
		// $('#revocation').modal('show')
		$('.btn-close-revocation').click(function(){
			$('#revocation').modal('toggle');
		});
	});

	$(function () {
		// $('#pass-car').modal('show')
		$('.btn-close-pass-car').click(function(){
			$('#pass-car').modal('toggle');
		});
	});

	$(function () {
		// $('#cancel-transfer').modal('show')
		$('.btn-close-cancel-transfer').click(function(){
			$('#cancel-transfer').modal('toggle');
		});
	});


	</script>
</section>
@endsection