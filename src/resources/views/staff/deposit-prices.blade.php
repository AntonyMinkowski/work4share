@extends('layouts.app')
@push('scripts')
    <script src="{{ asset('js/staff.js') }}" defer></script>
@endpush
@section('content')



    <div class="container-fluid">
        @include('staff.parts.navbar',['route' => $route])
        <div class="row">
            <div class="col-sm-12">
                <div class="container">
                    <div class="row" style="border-bottom: 1px solid;padding-bottom: 10px;">
                        <div class="col-sm-3 text-center"><b>Рыночная стоимость машины ( От: )</b></div>
                        <div class="col-sm-3 text-center"><b>Депозит</b></div>
                        <div class="col-sm-3 text-center"><b>Сохранить</b></div>
                        <div class="col-sm-3 text-center"><b>Удалить</b></div>
                    </div>
                    @foreach ($depositPrices as $item)
                        <form class="row my-2" action="{{ url('staff/update-deposit-price') }}" method="post">
                            @csrf
                            <input type="hidden" value="{{ $item->id }}" name="deposit_price_id">
                            <div class="col-sm-3">
                                <input type="number" name="car_price" class="form-control car-price"
                                       value="{{ $item->car_price }}">
                            </div>
                            <div class="col-sm-3 text-center">
                                <input type="number" name="deposit_price" class="form-control deposit-price"
                                       value="{{ $item->deposit_price }}">
                            </div>

                            <div class="col-sm-3 d-flex justify-content-center">
                                <button type="submit" class="btn">Обновить</button>
                            </div>
                            <div class="col-sm-3 d-flex justify-content-center">
                                <a href="{{ url('/staff/delete-deposit-price/'.$item->id) }}"
                                   class="btn btn-danger">Удалить</a>
                            </div>

                        </form>
                    @endforeach
                    <form class="row my-2" method="post" action="{{ url('staff/update-deposit-price') }}">
                        @csrf
                        <div class="col-sm-3">
                            <input type="number" name="car_price" class="form-control car-price">
                        </div>
                        <div class="col-sm-3 text-center">
                            <input type="number" name="deposit_price" class="form-control deposit-price">
                        </div>

                        <div class="col-sm-3 d-flex justify-content-center">
                            <button type="submit" class="btn">Добавить</button>
                        </div>

                    </form>
                </div>


            </div>
        </div>
    </div>


@endsection