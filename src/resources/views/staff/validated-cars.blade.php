@extends('layouts.app')
@push('scripts')
    <script src="{{ asset('js/staff.js') }}" defer></script>
@endpush
@section('content')


    <div class="container-fluid">
        @include('staff.parts.navbar',['route' => $route])
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col"># Пользователя</th>
                        <th scope="col"># Машины</th>
                        <th scope="col">Имя владельца</th>
                        <th scope="col">Эл.почта</th>
                        <th scope="col">Номер телефона</th>
                        <th scope="col">Автомобиль</th>
                        <th scope="col">ОСАГО</th>
                        <th scope="col">КАСКО</th>
                        <th scope="col">Дата добавления</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($cars as $item)
                        <tr id="car_{{ $item->car_id }}">
                            <th scope="row">{{ $item->user_id }}</th>
                            <th scope="row">{{ $item->car_id }}</th>
                            <td>{{ $item->user_name }}</td>
                            <td>{{ $item->email }}</td>
                            <td>{{ $item->phone }}</td>
                            <td>{{ $item->brand }} {{ $item->model }}</td>
                            <td>
                                <a href="{{ $item->osago_image }}">Открыть</a>
                                <form method="POST" action="{{route('car.editOsagoKaskoDate', $item->car_id)}}" class="editOsagoKaskoDate">
                                    @csrf
                                    <input type="date" name="osago_date" value="{{$item->osago_date}}">
                                    <button class="btn-primary btn-sm" type="submit">сохранить</button>
                                </form>
                                
                            </td>
                            <td>
                                <a href="{{ $item->kasko_image }}">Открыть</a>
                                <form method="POST" action="{{route('car.editOsagoKaskoDate', $item->car_id)}}" class="editOsagoKaskoDate">
                                    @csrf
                                    <input type="date" name="kasko_date" value="{{$item->kasko_date}}">
                                    <button class="btn-primary btn-sm" type="submit">сохранить</button>
                                </form>
                            </td>
                            <td>{{ $item->created_at }}</td>
                            <td>
                                <a target="_blank" href="{{ url('/profile/mycar/'.$item->car_id) }}" class="btn">Редактировать</a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection