@extends('layouts.app')

@section('content')

<div class="container">
    @include('staff.parts.navbar',['route' => $route])
</div>

@endsection