<form method="post" action="{{ url('staff/checkauto') }}" class="row">
    @csrf
    <div class="col-lg-3 col-md-6 col-12">
        <div id="brand">
            <h2 class="form-cars__title">
                Марка автомобиля
            </h2>
            <div class="form-cars__wrap">
                <select
                        id="brandid"
                        type="checkbox"
                        class="form-cars__select"
                        name="brand">
                    <option value="">Ничего не выбрано</option>
                    @isset ($brandsCars)
                        @foreach ($brandsCars as $item)
                            <option
                                    class="form-cars__option" value="{{ $item['id'] }}"
                                    @isset ($selected_brand)
                                    @if( $selected_brand == $item['id']) selected @endif
                                    @endisset
                            >
                                {{ $item['name'] }}
                            </option>
                        @endforeach
                    @endisset
                </select>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-12">
        <div id="model">
            <h2 class="form-cars__title">
                Модель автомобиля
            </h2>
            <div class="form-cars__wrap">
                <select
                        id="model-select"
                        type="checkbox"
                        class="form-cars__select"
                        name="model" @if(!$models) style="display: none" @endif>
                    <option value="">Ничего не выбрано</option>
                    @if($models)
                        @foreach ($models as $item)
                            <option
                                    class="form-cars__option" value="{{ $item['id'] }}"
                                    @isset ($selected_model)
                                    @if( $selected_model == $item['id']) selected @endif
                                    @endisset
                            >
                                {{ $item['name'] }}
                            </option>
                        @endforeach
                    @endif

                </select>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-12">
        <div id="model">
            <h2 class="form-cars__title">
                Дата добавления
            </h2>
            <div class="form-cars__wrap d-flex">
                <label for="from-date">От</label>
                <input type="date" value="{{ $fromDate ?? '' }}" id="from-date" name="from_date">
                <label for="to-date">До</label>
                <input type="date" value="{{ $toDate ?? '' }}" id="to-date" name="to_date">
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-12">
        <button type="submit" class="btn">
            Найти
        </button>
    </div>
</form>
