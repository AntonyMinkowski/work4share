@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Изменение профиля</div>
                <div class="card-body">
                    @csrf
                    @cannot ('validate-user')
                        <ul class="nav nav-tabs" id="tabpanel_verified" role="tablist">
                            <li class="nav-item">
                              <a class="nav-link
                              @if(!isset($personal) && !$personal['passport_face'] && !$personal['passport_visa'])
                              active
                              @endif" id="home-tab" data-toggle="tab" href="#add_pasport_user" role="tab" aria-controls="home" aria-selected="true">Шаг 1</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link
                              @if(!isset($personal) && ($personal['passport_face'] || $personal['passport_visa']) && !$personal['driver_lisense_first'] && !$personal['driver_license_second'])
                              active
                              @endif" id="profile-tab" data-toggle="tab" href="#driving_licence" role="tab" aria-controls="profile" aria-selected="false">Шаг 2</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Шаг 3</a>
                            </li>
                        </ul>

                        <div class="tab-content" id="tabpanel_verifiedContent">
                          <div class="tab-pane fade
                          @if(!isset($personal) && !$personal['passport_face'] && !$personal['passport_visa'])
                          show active
                          @endif" id="add_pasport_user" role="tabpanel" aria-labelledby="home-tab">
                              <p>Добавьте фото паспорта со страницами фотографии и прописки</p>
                              <form
                                  class="md-form"
                                  enctype="multipart/form-data"
                                  id="user_pasport"
                                  role="form"
                                  method="POST"
                                  >
                                <div style="position:relative;">
                                <input name="passport_face" class="input-file" type="file"/>
                                <br />
                                <input name="passport_visa" class="input-file" type="file"/>
                                  </div>
                                  <div class="row">
                                    <span class="output_pasport"></span>
                                </div>
                                  <div class="form-group row mb-0">
                                      <div class="col-md-6 offset-md-4">
                                          <button type="button submit" class="btn btn-primary user_pasport">
                                              отправить
                                          </button>
                                      </div>
                                  </div>
                              </form>
                          </div>
                          <div class="tab-pane fade
                          @if(!isset($personal) && ($personal['passport_face'] || $personal['passport_visa']) && !$personal['driver_lisense_first'] && !$personal['driver_license_second'])
                          show active
                          @endif" id="driving_licence" role="tabpanel" aria-labelledby="profile-tab">
                              <p>Добавьте фото водительского удостоверения с 2х сторон</p>
                              <form
                                  class="md-form"
                                  enctype="multipart/form-data"
                                  id="add_driving_licence"
                                  role="form"
                                  method="POST"
                                  >
                                <div style="position:relative;">
                                <input name="driver_lisense_first" class="input-file" type="file"/>
                                <br />
                                <input name="driver_license_second" class="input-file" type="file"/>
                                  </div>
                                  <div class="row">
                                    <span class="output_pasport"></span>
                                </div>
                                  <div class="form-group row mb-0">
                                      <div class="col-md-6 offset-md-4">
                                          <button type="button submit" class="btn btn-primary user_pasport">
                                              отправить
                                          </button>
                                      </div>
                                  </div>
                              </form>
                          </div>
                          <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                              Данные отправлены на верификацию
                          </div>
                        </div>
                    @endcannot
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
