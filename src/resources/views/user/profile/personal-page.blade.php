@extends('layouts.app') 
@section('content')

<body>
  <div class="container profile">
    <div class="row" style="margin-top: 50px">
      <div class="col-sm-12">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="account-tab" data-toggle="tab" href="#account" role="tab" aria-controls="account" aria-selected="true">Аккаунт</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="history-tab" data-toggle="tab" href="#history" role="tab" aria-controls="history" aria-selected="false">История платежей</a>
          </li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12 tab-content" id="myTabContent">
        <div class="tab-pane fade all show active mb-5" id="account" role="tabpanel" aria-labelledby="account-tab">
          <section class="info">
            <form method="POST" action="profile/saveInfo">
              @csrf
              <div class="row" style="padding-top: 40px;">
                <div class="col-12 col-sm-3">
                  <div class="form-cars__wrap">
                    <h2 class="form-cars__title">
                      Email
                    </h2>
                    <p class="login-form__input">{{ $user->email }}</p>
                  </div>
                </div>
                <div class="col-12 col-sm-3">
                  <div class="form-cars__wrap">
                    <h2 class="form-cars__title">
                      Телефон
                    </h2>
                    <input type="text" class="login-form__input" placeholder="Телефон" name="phone" id="phone" value="{{ $user->phone }}" required>
                  </div>
                </div>
                <div class="col-12 col-sm-3">
                  <div class="form-cars__wrap">
                    <h2 class="form-cars__title">
                        Страна
                    </h2>
                    <input type="text" class="login-form__input" placeholder="" name="country" id="country" value="{{ $user->country }}">  
                  </div>
                </div>
                <div class="col-12 col-sm-3">
                  <div class="form-cars__wrap">
                    <h2 class="form-cars__title">
                        Город
                    </h2>
                    <input type="text" class="login-form__input" placeholder="Город" name="city" id="city" value="{{ $user->city }}">
                  </div>
                </div>
              </div>
              <div class="row" style="padding-top: 40px;">
                <div class="col-12 col-sm-3">
                  <div class="form-cars__wrap">
                    <h2 class="form-cars__title">
                        Вконтакте
                    </h2>
                    <input type="text" class="login-form__input" placeholder="Вконтакте" name="vk_link" id="vk_link" value="{{ $user->vk_link }}">
                  </div>
                </div>
                <div class="col-12 col-sm-3">
                  <div class="form-cars__wrap">
                    <h2 class="form-cars__title">
                        Instagram
                    </h2>
                    <input type="text" class="login-form__input" placeholder="Instagram" name="inst_link" id="inst_link" value="{{ $user->inst_link }}">
                  </div>
                </div>
                <div class="col-12 col-sm-3">
                  <div class="form-cars__wrap">
                    <h2 class="form-cars__title">
                        Facebook
                    </h2>
                    <input type="text" class="login-form__input" placeholder="Facebook" name="fb_link" id="fb_link" value="{{ $user->fb_link }}">
                  </div>
                </div>
              </div>
              <button type="submit" class="btn login-form__button" style="margin-top: 40px;">{{ __('Сохранить') }}</button>
            </form>
          </section>
          <section class="mt-5">
            <div class="container">
              <h1 class="login-title">Смена пароля</h1>
              @if (session()->get( 'response' ))
              <p>{{ session()->get( 'response' ) }}</p>
              @endif
              <form method="POST" action="profile/updateAuthUserPassword" aria-label="{{ ('Login') }}" class="login-form d-flex justify-content-between align-items-center">
                @csrf
                <div class="login-form__pass">
                  <p class="login-form__text">{{ ('Текущий пароль') }}</p>
                  <input id="#" type="password" class="login-form__input{{ $errors->has('password') ? ' is-invalid' : '' }}" name="current"
                   minlength="6" placeholder="Текущий пароль" required>
                </div>
                <div class="login-form__pass">
                  <p class="login-form__text">{{ ('Новый пароль') }}</p>
                  <input id="#" type="password" class="login-form__input{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
                   minlength="6" placeholder="Новый пароль" required>
                </div>
                <div class="login-form__pass">
                  <p class="login-form__text">{{ ('Подвердите новый пароль') }}</p>
                  <input id="#" type="password" class="login-form__input{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password_confirmation"
                   minlength="6" placeholder="Подвердите пароль" required>
                </div>
                <button style="height: 60px; margin-bottom: 53px" type="submit" class="btn login-form__button">{{ __('Изменить') }}</button>
              </form>
            </div>

          </section>
          <div class="row" style="margin-top: 20px">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header">
                  Выберите предпочитаемый тип КПП
                </div>
                <div class="card-body">
                  <form method="POST" action="profile/updatePreferredTypeKPP">
                    <div class="row align-items-center">
                      <div class="col-sm-8">
                        <div class="group-ridio">
                          @csrf
                          <input name="preferred_type_KPP" class="form-check-label" 
                          {{ $user->preferred_type_KPP == 'auto' ? 'checked' : '' }} type="radio" value="auto" id="auto">
                          <label for="auto">Автоматическая коробка передач</label>
                          <input name="preferred_type_KPP" class="form-check-label" 
                          {{ $user->preferred_type_KPP == 'mech' ? 'checked' : '' }} type="radio" value="mech" id="mech">
                          <label for="mech">Механическая коробка передач</label>
                        </div>
                      </div>
                    
                      <div class="col-sm-4">
                        <button type="submit" class="btn login-form__button">Сохранить</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div class="row" style="margin-top: 20px">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header">Выберите канал получения уведомлений</div>
                <div class="card-body">
                  <form method="POST" action="profile/saveNotifications">
                    <div class="row align-items-center">
                    <div class="col-sm-8">
                      <div class="checkbox">
                      @csrf
                      <input id="email" type="checkbox" name="email" {{ $user->email_notif ? 'checked' : '' }} class="check">
                        <label for="email">email</label>
                      <input id="sms" type="checkbox" name="sms" {{ $user->sms_notif ? 'checked' : '' }} class="check">
                        <label for="sms">sms</label>
                      <input id="app" type="checkbox" name="app" {{ $user->app_notif ? 'checked' : '' }} class="check">
                        <label for="app">app</label>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <button type="submit" class="btn login-form__button">Сохранить</button>
                    </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div class="row" style="margin-top: 20px">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header">
                  Стаж
                </div>
                <div class="card-body">
                  <form method="POST" action="profile/updateExperience">
                    <div class="row align-items-center">
                      <div class="col-sm-8">
                        @csrf
                        <div class="login-form__pass">
                          <p class="login-form__text">Стаж вождения</p>
                          <input type="number" class="login-form__input" min="0" name="driving_experience"
                                 value="{{ $user->driving_experience }}" required>
                        </div>
                      </div>

                      <div class="col-sm-4">
                        <button type="submit" class="btn login-form__button">Сохранить</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div class="row" style="margin-top: 20px">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header">
                  День рождения
                </div>
                <div class="card-body">
                  <form method="POST" action="profile/updateBirthday">
                    <div class="row align-items-center">
                      <div class="col-sm-8">
                        @csrf
                        <div class="login-form__pass">
                          <p class="login-form__text">День рождения</p>
                          <input type="date" class="login-form__input" name="birthday"
                                 value="{{ $user->birthday }}" required>
                        </div>
                      </div>

                      <div class="col-sm-4">
                        <button type="submit" class="btn login-form__button">Сохранить</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div class="row" style="margin-top: 20px">
            <div class="col-12">
              <ul class="card-list">
                <li class="card__photo card-list__item d-flex justify-content-between align-items-center">
                  <h2 class="card-list__title card-list__title_small">
                      Загрузите аватар
                  </h2>
                  <div class="card-list__Upload">
                    <input type="file" id="avatar" name="avatar" value="">
                      @if ($user->avatar) 
                        <img id="blah" src="{{ $user->avatar }}" width="300" />
                      @endif
                  </div>
                </li>
              </ul>
            </div>
          </div>
          <div class="row" style="margin-top: 20px">
            <div class="col-sm-12">
            <div class="card">
              <div class="card-header">
              Статус заявки на допуск к авто
              </div>
              <div class="card-body">
              <p>Ваш статус: </p>
              </div>
            </div>
            </div>
          </div>
          <div class="row" style="margin-top: 20px">
            <div class="col-sm-12">
            <div class="card">
              <div class="card-header">
                @if ($user->isActive == null) 
                  <a href="profile/isActive" style="color: red;">Скрыть аккаунт</a>
                @else 
                  <a href="profile/isActive" style="color: green;">Сделать видимым аккаунт</a>
                @endif
              </div>
            </div>
            </div>
          </div>
        </div>
        <div class="tab-pane fade show" id="history" role="tabpanel" aria-labelledby="history-tab">
          <h2>История платежей</h2>
          <div class="table">
            <div class="tr">
              <div class="table_tr">Дата платежа</div>
              <div class="table_tr">Сумма платежа</div>
              <div class="table_tr">Результат</div>
            </div>
            @foreach ($paymentBooking as $item)
            <div class="tr">
              <div class="th" scope="row">
                {{$item->updated_at}}
              </div>
              <div class="td">
                {{$item->total_amount_booking}}
              </div>
              <div class="td">
                @if ($item->result)
                  Успешно
                @else
                  Ошибка
                @endif
              </div>
            </div>
            @endforeach
            
          </div>
        </div>
		  </div>
		</div>
</div>


</body>
@endsection