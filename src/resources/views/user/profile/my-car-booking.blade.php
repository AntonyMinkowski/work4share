@extends('layouts.app') 
@section('content')

<section class="rent-car trips__card">
	<div class="container">
		<div class="row">
			<div class="col">
				<h1 class="about-car__title">{{$car->brand->name}} {{$car->models_name}}</h1>
			</div>
			{{-- Добавим id в документ что бы js мог его получить --}}
			<input type="text" id="booking_id" value="{{$booked->id}}" hidden style="display: none">
		</div>
		<div class="row trips__card__intro">
			<div class="col-lg-7 col-12">
				<div class="img-card1 d-flex" style="overflow: hidden;">
					<div class="carousel slide" data-ride="carousel" id="carouselExampleIndicators">
						<div class="carousel-inner">
							<?php $i = 0; ?>
							@foreach ($images as $image)
								<div class="carousel-booked @if ($i == 0) active @endif">
									<div class="img-slider" style="background: url(https://hb.bizmrg.com/soautomedia/{{$image->img_url}}) center center no-repeat;"></div>
								</div>
							<?php $i++; ?>
							@endforeach
						</div><a class="carousel-control-prev" data-slide="prev" href="#carouselExampleIndicators" role="button"><span aria-hidden="true" class="carousel-control-prev-icon"></span> <span class="sr-only">Previous</span></a> <a class="carousel-control-next" data-slide="next" href="#carouselExampleIndicators" role="button"><span aria-hidden="true" class="carousel-control-next-icon"></span> <span class="sr-only">Next</span></a>
					</div>
				</div>
			</div>
			<div class="col-lg-5 col-12">
				<div class="trips__card__info">
					<h2>
						Детали бронирования
					</h2>
					<div class="trips__card__info__content">
						<div class="trips__card__info__text">
							<p>Статус</p>
							@isset($statusName)
								<span>{{$statusName}}</span>
							@endisset
						</div>
						<div class="trips__card__info__text">
							<p>Даты</p>
							<span>{{ $booked->datefrom }} - {{ $booked->dateto }}</span>
						</div>

                        @isset ($info_order)
                            @isset ($car)
                                <div class="trips__card__info__text">
                                    <p>Марка</p>
                                    <span>{{ $car->brand->name }}</span>
                                </div>
                                <div class="trips__card__info__text">
                                    <p>Модель</p>
                                    <span>{{ $car->models_name  }}</span>
                                </div>
                                <div class="trips__card__info__text">
                                    <p>Кол-во дней</p>
                                    <span>{{$info_order['number_days']}}</span>
                                </div>
                                <div class="trips__card__info__text">
                                    <p>Включенный пробег</p>
                                    <span>{{$info_order['included_distance']}} км</span>
                                </div>
                                @if (isset($rentPlace->name) && $rentPlace !== [])
                                    <div class="trips__card__info__text">
                                        <p>Место где состоится встреча:</p>
                                        <span>{{$rentPlace->name}}</span>
                                    </div>
                                @endif
                                <div class="trips__card__info__text">
                                    <p>Доставка</p>
                                    @if (isset($info_order['delivery_cost']) && $info_order['delivery_cost'] !== [])
                                        <span >{{$info_order['delivery_cost']}} ₽</span>
                                    @else
                                        <span>0</span>
                                    @endif
                                </div>
                                <div class="trips__card__info__text">
                                    <p>Депозит</p>
                                    <span>{{$info_order['deposit']}}  ₽</span>
                                </div>
                                <div class="trips__card__info__text">
                                    <p>Сумма бронирования</p>
                                    <span>{{$info_order['sum']}}  ₽</span>
                                </div>
                            @endisset
                        @endisset
					</div>
				</div>
			</div>
        </div>
        <div class="row">
            <div class="col">
                <div class="car-wrapper__inner d-flex" style="height: auto;">               
                @if ($booked->booking_photoLandlord && $booked->booking_fiel && $booked->booking_mileage && $booked->booking_mileage->mileage_before)
                    @if ($booked->status !== 20 && $booked->status !== 9 && $booked->status !== 15 && $booked->status !== 5 && $booked->status !== 11)
                        <div class="car-wrapper__inner d-flex" style="flex-direction:column;height: 100%;">
                            <div class="container-check-photos">
                                <h1 class="about-car__title">Арендатор сфотографировал авто и готов его забрать, Вам нужно подтвердить данные</h1>
                                <div>
                                    <h1 class="about-car__title">Фото салона автомобиля</h1>
                                    @foreach ($booked->booking_photoLandlord as $image)
                                        <img src="https://hb.bizmrg.com/soautomedia/{{$image->name}}" width="60%" height="100%" alt="">
                                    @endforeach
                                </div>
                                <div>
                                    <h1 class="about-car__title">Фото остатка ГСМ</h1>
                                    <img src="https://hb.bizmrg.com/soautomedia/{{$booked->booking_fiel->fiel_photo_before}}" width="60%" height="100%" alt="">
                                </div>
                                <div>
                                    <h1 class="about-car__title">Фото фото пробега</h1>
                                    <img src="https://hb.bizmrg.com/soautomedia/{{$booked->booking_mileage->mileage_photo_before}}" width="60%" height="100%" alt="">
                                </div>
                                <div>
                                    <h1 class="about-car__title">Пробег: </h1>
                                    <h1 class="about-car__title">{{$booked->booking_mileage->mileage_before}} км</h1>
                                </div>
                            </div>
                            <a href="/profile/confirm-payment/{{$booked->id}}" class="btn creating-cars-btn" style="height: auto;">Подтвердить</a>
                        </div>
                    @elseif($booked->status === 20)
                        <p class="car-wrapper__text_f" style="width: 100%;">
                        Арендатор оплатил поездку
                        </p>
                    @elseif($booked->status === 9)
                    <p class="car-wrapper__text_f" style="width: 100%; text-align:center;">
                        Вы подтвердили оплату, ждите пока арендатор переведет деньги
                    </p>
                    @elseif ($booked->status === 5)
                        <p class="car-wrapper__text_f" style="width: 100%; text-align:center;">
                        Арендатор отменил бронь
                        </p>
                    @elseif ($booked->status === 11)
                        <p class="car-wrapper__text_f" style="width: 100%; text-align:center;">
                        Арендатор оплатил поездку. Аренда автивна
                        </p>
                    @elseif($booked->status === 15)
                    <div id="container-filling">
                        <div class="row trips__card__salon">
                            <div class="col-12">
                                <form method="POST" action="" style="justify-content: flex-start;">
                                    @csrf
                                    <h2>Загрузите фото салона автомобиля</h2>
            
                                    @foreach ($bookedPhotoTenant as $image)
                                    <div>
                                        <div class="trips__card__salon__photo trips__card__add">
                                            <div>
                                                <img src="https://hb.bizmrg.com/soautomedia/{{$image->name}}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
            
                                    <input type="file" id="myCarCard_car_interior" name="myCarCard_car_interior" multiple>
                                </form>
                                <hr>
                            </div>
                        </div>
                        <div class="row trips__card__another">
                            <div class="col-12">
                                <div class="container_trips__card__another">
                                    <div class="fuel trips__card__add">
                                        <h2>Загрузите фото остатка ГСМ</h2>
                                    </div>
                                    <input type="file" id="myCarCard_fuel_residue" name="myCarCard_fuel_residue">
                                    <div class="mileage trips__card__add">
                                        <h2>Загрузите фото пробега</h2>
                                    </div>
                                    <input type="file" id="myCarCard_run" name="myCarCard_run">
                                </div>
            
                                <form enctype="multipart/form-data" style="display: none;" id="mileageInputMyCar" method="POST" action="{{ route('my-car.addCarMileageInput', $booked->id) }}">
                                    @csrf
                                    <div class="input">
                                        <input type="text" id="mileage" name="mileage" placeholder="Введите пробег">
                                    </div>
                                    <button class="btn rent-btn">Сохранить</button>
                                </form>
                                <hr>
                            </div>
                        </div>
                    </div>
                    @endif
                    @else
                    @if ($booked->status !== 7 && $booked->status !== 23 && $booked->status !== 5 && $booked->status !== 9 && $booked->status !== 22)
                        <p class="car-wrapper__text_f">
                        <button type="button" class="btn rent-btn" data-toggle="modal" data-target="#rescission" id="btn-rescission" onclick="changeFormAction({{$booked->id}})">Отменить</button>
                        </p>
                        <p class="car-wrapper__text_s">
                        <a href="/profile/confirm/{{$booked->id}}" class="btn creating-cars-btn" style="padding: 11px 33px 9px; margin: 0px;">Подтвердить</a>
                        </p>
                    @elseif ($booked->status === 7)
                        <p class="car-wrapper__text_f" style="width: 100%; text-align:center;">
                        Вы уже подтвердили сдачу, ждите пока арендатор предоставит фото
                        </p>
                    @elseif ($booked->status === 5)
                        <p class="car-wrapper__text_f" style="width: 100%; text-align:center;">
                        Арендатор отменил бронь
                        </p>
                    @elseif($booked->status === 23)
                        <p class="car-wrapper__text_f" style="width: 100%;">
                        Вы отменили сдачу
                        </p>
                    @elseif($booked->status === 22)
                        <p class="car-wrapper__text_f" style="width: 100%;">
                        Данные арендатора еще не подтверждены
                        </p>
                    @endif
                @endif
            </div>
        </div>
    </div>
    {{-- Modal window --}}
    <div class="modal fade" id="rescission" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form enctype="multipart/form-data" id="form-rescission" method="POST" action="#">
            @csrf
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Назовите причину отказа</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <textarea name="rescission_description" id="" rows="10" style="width: 100%;"></textarea>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-close-rescission creating-cars-btn mt-0">Сохранить</button>
            </div>
            </form>
        </div>
        </div>
    </div>
</section>
<script>
 function changeFormAction(val){
    var myform = $('#form-rescission').attr('action', '/profile/rescission/' + val);
  }

	$(function () {
		$('#btn-rescission').click(function(){
      $('#rescission').modal('show')
    });

		$('.btn-close-rescission').click(function(){
			$('#rescission').modal('toggle');
		});
    });
</script>
@endsection
