@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="verify__block d-flex justify-content-around align-items-center mb-4">
                <h2 class="rent__title">
                        {{ __('Подтверждение Email адреса') }}
                </h2>
                @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Письмо было отправлено на твой Email.') }}
                        </div>
                    @endif
                <p>{{ __('На твой Email пришло письмо со ссылкой. Пожалуйста проверь почту и перейди по ссылке из письма.') }}
                        {{ __('Письмо не пришло?') }} <br><a href="{{ route('verification.resend') }}">{{ __('Нажми чтобы отправить снова.') }}</a>.</p>
            </div>
               
        </div>
    </div>
</div>
@endsection
