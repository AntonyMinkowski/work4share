@extends('layouts.app')

@section('content')

<section class="join">
	<div class="container">
		<h1 class="join-title">Регистрация</h1>
		<form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}" class="login-form register-form d-flex">
			@csrf
			<div class="login-form__name-email">
			<div class="login-form__name">
				<p class="login-form__text">{{ __('Введите имя') }}</p>
				<input type="text" class="login-form__input{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="Введите имя" required value="{{ old('name') }}">

				@if ($errors->has('name'))
					<p class="login-form__text login-form__text_red">{{ $errors->first('name') }}</p>
				@endif

			</div>
			<div class="login-form__adress">
				<p class="login-form__text">{{ __('Введите адрес эл. почты') }}</p>
				<input id="#" type="email" class="login-form__input{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Введите E-mail" required value="{{ old('email') }}">

				@if ($errors->has('email'))
					<p class="login-form__text login-form__text_red">{{ $errors->first('email') }}</p>
				@endif

			</div>
			</div>
			<div class="login-form__password">
				<div class="login-form__pass">
					<p class="login-form__text">{{ __('Пароль') }}</p>
					<input id="password" type="password" class="login-form__input login-form__input_pass{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" minlength="6" placeholder="Введите пароль" value="" required>
					<div id="show-pass" onclick="showPass('password')"></div>
					@if ($errors->has('password'))
						<p class="login-form__text login-form__text_red">{{ $errors->first('password') }}</p>
					@endif
				</div>
				<div class="login-form__repeat-pass">
					<div id="show-pass-confirm" onclick="showPass('password-confirm')"></div>
					<p class="login-form__text">{{ __('Повторите пароль') }}</p>
					<input id="password-confirm" type="password" class="login-form__input" name="password_confirmation" minlength="6" placeholder="Введите пароль" value="" required>
				</div>
			</div>
			<button type="submit" class="btn login-form__join-button">{{ __('Зарегистрироваться') }}</button>
		</form>
	</div>
</section>
<script type="text/javascript">


	
</script>
{{-- <div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">{{ __('Register') }}</div>

				<div class="card-body">
					<form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
						@csrf

						<div class="form-group row">
							<label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

							<div class="col-md-6">
								<input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

								@if ($errors->has('name'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group row">
							<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

							<div class="col-md-6">
								<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

								@if ($errors->has('email'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group row">
							<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

							<div class="col-md-6">
								<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

								@if ($errors->has('password'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('password') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group row">
							<label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

							<div class="col-md-6">
								<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
							</div>
						</div>

						<div class="form-group row mb-0">
							<div class="col-md-6 offset-md-4">
								<button type="submit" class="btn btn-primary">
									{{ __('Register') }}
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div> --}}
@endsection
