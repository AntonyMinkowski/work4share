@extends('layouts.app') 
@section('content')

{{print_r($selectionCars[0])}}

<div class="container">
  <div class="row">
    <div class="col-12">
      <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          @foreach ($images as $item)
          <div class="carousel-item active">
            <img class="d-block w-100" src="https://hb.bizmrg.com/soautomedia/{{$item->img_url}}" alt="First slide">
          </div>
          @endforeach
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Назад</span>
  </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Вперед</span>
  </a>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-header">
          Об автомобиле
        </div>
        <div class="card-body">
          <h5 class="card-title">{{$selectionCars[0]->brand}} {{$selectionCars[0]->model}}</h5>
          <p class="card-text">
            <div class="row">
              <div class="col-6">
                <p>КПП: {{$selectionCars[0]->gear}}</p>
                <p>Кузов: {{$selectionCars[0]->car_type}}</p>
                <p>Год: {{$selectionCars[0]->year_of_issue}}</p>
              </div>
              <div class="col-6">
                <p>Пассажиров: {{$selectionCars[0]->seat}}</p>
                <p>Дверей: {{$selectionCars[0]->doors}}</p>
                <p style="color: red; font-size: 15px">Цена за сутки:
                <span style="color: red; font-size: 15px" id="cost_day">{{$selectionCars[0]->cost_day}}</span>
              рублей</p>
              </div>
            </div>
          </p>
        </div>
      </div>
    </div>
  </div>
  <div class="row" style="margin-top: 5%">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-header">
          Бронирование
        </div>
        <div class="card-body">
          <p class="card-text">
            От: <input type="date" id="bookingfromDate" min="{{$selectionCars[0]->from}}" max="{{$selectionCars[0]->to}}" name="from"> До: <input type="date" id="bookingtoDate" min="{{$selectionCars[0]->from}}"
              max="{{$selectionCars[0]->to}}" name="to"> 
              {{-- От: <input type="date" id="bookingfromDate" min="{{$selectionCars[0]->from}}" max="{{date("Y-m-d", strtotime("-1 day", strtotime($selectionCars[0]->to)))}}" name="from"> До: <input type="date" id="bookingtoDate" min="{{date("Y-m-d", strtotime("-1 day", strtotime($selectionCars[0]->from)))}}"
              max="{{$selectionCars[0]->to}}" name="to">  --}}
              <div class="row" id="bookingDetals">
                <div class="col-sm-4">
                  <p>Стоимость аренды: <span id="fullCostRend"></span> рублей за <span id="rentDays"></span> дней</p>
                </div>
                <div class="col-sm-4">
                  <p>Доставка: <span id="deliveryCost"></span> рублей</p>
                </div>
                <div class="col-sm-4"></div>
              </div>
              @if (Route::has('login')) 
              @auth
                
                @if ($selectionCars[0]->available)
                <a href="/bookingcar/?carId={{$selectionCars[0]->id}}&" id="bookingButton" class="btn btn-primary">Забронировать</a>
                @else
                <a href="#" class="btn btn-primary" disabled>Забронировать</a>
                @endif
              @else
              @if ($selectionCars[0]->available)
              <a href="/login" onclick="alert('Вы не зарегистрированы')" class="btn btn-primary">Забронировать</a> @else
              <a href="/login" onclick="alert('Вы не зарегистрированы')" class="btn btn-primary" disabled>Забронировать</a>@endif
                 
            @endauth 
            @endif
          </p>
        </div>
      </div>
    </div>
  </div>

</div>

<script type="application/javascript">
var car_id = {{ $selectionCars[0]->id }};
</script>
@endsection